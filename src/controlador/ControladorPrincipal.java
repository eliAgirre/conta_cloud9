package controlador;

import javax.swing.JOptionPane;

import utilidades.Constantes;
import utilidades.Directorios;
import vista.Principal;

/**
 * Controla y muestra la ventana principal.
 * 
 */
public class ControladorPrincipal {
	
	//Atributos de la clase
	private static Principal ventanaPrincipal;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorPrincipal() {

		boolean dirRaiz      = false; 
        boolean listasDir    = false; 
        boolean registrosDir = false; 
        boolean logsDir      = false; 
        
        if( Directorios.esRaizJar() ) {
        	if( !Directorios.existeDirRaiz(Constantes.NOMBRE_DIR_RAIZ)) 
            	dirRaiz      = Directorios.dirRaizCreado();
            else
            	dirRaiz = true;
        }
        
        if( !Directorios.existeDir(Constantes.NOMBRE_DIR_LISTAS)) 
        	listasDir    = Directorios.creadoListasDir();
        else
        	listasDir = true;
        
        if( !Directorios.existeDir(Constantes.NOMBRE_DIR_REGISTROS)) 
        	registrosDir = Directorios.creadoRegistrosDir();
        else
        	registrosDir = true;
        
        if( !Directorios.existeDir(Constantes.NOMBRE_DIR_LOGS)) 
        	logsDir      = Directorios.creadoLogsDir();
        else
        	logsDir = true;

        
        if( Directorios.esRaizJar() ) {
        	if( dirRaiz && listasDir && registrosDir && logsDir){ 
                
    			//Instancia la clase Principal (vista) 
    			ventanaPrincipal=new Principal(); 
    			// Coloca la ventana en el centro de la pantalla 
    			ventanaPrincipal.setLocationRelativeTo(null); 
    			// Hace visible la ventana 
    			ventanaPrincipal.setVisible(true); 
            } 
            else 
            	JOptionPane.showMessageDialog(null, Constantes.AVISO_DIRECTORIO_NO_CREADO, Constantes.AVISO_CREACION_DIRECTORIOS, JOptionPane.WARNING_MESSAGE);
        }
        else {
        	
        	if( listasDir && registrosDir && logsDir){ 
                
    			//Instancia la clase Principal (vista) 
    			ventanaPrincipal=new Principal(); 
    			// Coloca la ventana en el centro de la pantalla 
    			ventanaPrincipal.setLocationRelativeTo(null); 
    			// Hace visible la ventana 
    			ventanaPrincipal.setVisible(true); 
            } 
            else 
            	JOptionPane.showMessageDialog(null, Constantes.AVISO_DIRECTORIO_NO_CREADO, Constantes.AVISO_CREACION_DIRECTORIOS, JOptionPane.WARNING_MESSAGE); 
        }

	} //Cierre del constructor
	
} // Cierre de la clase