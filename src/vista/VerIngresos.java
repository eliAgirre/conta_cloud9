package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 

import javax.swing.ImageIcon; 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JPanel; 
import javax.swing.JScrollPane; 
import javax.swing.JTable; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla; 

public class VerIngresos extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
        
    // Atributos de la clase 
    private JPanel contentPane; 
    private JButton btnMenu; 
            
    // Atributos relacionados con la tabla 
    private JScrollPane scrollPane; 
    private DefaultTableModel modelo; 
    private String [][] arrayTabla; //array bidimensional 
    private JTable tabla; 
    private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_INGRESOS];
    private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_INGRESOS);
    
    public VerIngresos(){ 
            
        // Caracteristicas de la ventana 
        setResizable(false); // no reestablece el size 
        setBounds(100, 100, 411, 437); // size 
        setTitle(Constantes.VISTA_VER_INGRESOS); // titulo 
        setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
        
        // Llama al metodo componentes 
        componentes(); 
        
        modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
            
    } // Constructor 
    
    public void componentes(){ 
            
        // Layout 
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
        
        // Panel para visualizar y hacer scroll
        scrollPane = Utilidades.obtenerScroll(21, 75, 374, 221, scrollPane, false);
        contentPane.add(scrollPane);
        
        // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
        modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_INGRESOS); 
        
        // Se le pasa a JTable el modelo de tabla 
        tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);
        
        // Labels + textFields + comboBox + buttons 
        contentPane.add(Utilidades.obtenerVersionLabel(0, 383, 95, 14)); // Version label 
        
        contentPane.add(Utilidades.obtenerAvisoInfoLabel(21, 27, 268, 20, Constantes.LIT_INFO_VER_INGRESOS+Constantes.PUNTOS)); // Aviso de visualizacion 
        
        btnMenu = Utilidades.obtenerBoton(151, 326, 126, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu); 
            
    } // Cierre componentes 
    
    @SuppressWarnings("unused") 
    public void actionPerformed(ActionEvent evento) { 
        // Si hace clic en Menu se guardan los datos y vuelve a la principal 
        if(evento.getSource()==btnMenu){ 

			ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
			setVisible(false);
        } 
            
    } // Cierre del metodo actionPerformed 

} // Cierre clase 