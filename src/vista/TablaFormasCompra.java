package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal; 
import utilidades.Constantes; 
import utilidades.FicherosListas; 
import utilidades.ListaConstantesValidacion; 
import utilidades.Utilidades; 
import utilidades.UtilidadesTabla; 

public class TablaFormasCompra extends JFrame implements ActionListener { 
        
        private static final long serialVersionUID = -8624753721794395368L; 
        
        // Atributos de la clase 
        private JPanel contentPane; 
        private String id; 
        private String formaCompra; 
        private JButton btnMenu; 
        
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla; 
        private static String[] datosFile=new String[2]; 
        private ArrayList<String> registros = FicherosListas.leerRegistros(Constantes.FICHERO_FORMA_COMPRA); 
                
        // Atributos del resultados user 
        
        public TablaFormasCompra(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 340, 475); // size 
            setTitle(Constantes.VISTA_COMPRAS_A_EDITAR); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Carga los componentes de la vista 
            componentes(); 
                
        } // Constructor 
        
        private void componentes(){ 
                
	        // Layout 
	        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
	        setContentPane(contentPane); 
	                
	        // Panel para visualizar y hacer scroll 
	        scrollPane = Utilidades.obtenerScroll(40, 55, 250, 303, scrollPane, false);
	        contentPane.add(scrollPane); 
	                
	        // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
	        modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_FORMAS_COMPRA); 
	        
	        // Se le pasa a JTable el modelo de tabla 
	        tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane); 
	        modelo = UtilidadesTabla.mostrarTodasListasTabla(registros, datosFile, modelo); 
	        
	        // Se agrega un listener a la tabla 
	        tabla.addMouseListener(new MouseAdapter() { 
	            // Cuando el raton seleccione un dato de una fila muestra un aviso 
	        	public void mouseClicked(MouseEvent evento) { 
	                    
	                String nl = System.getProperty(Constantes.PROPERTY_SALTO_LINEA); 
	                int fila = tabla.rowAtPoint(evento.getPoint()); 
	                int columna = 0; 
	                if ((fila > -1) && (columna > -1)) 
	                id=(String) modelo.getValueAt(fila,columna); 
	                formaCompra=(String) modelo.getValueAt(fila,1); 
	                JOptionPane.showMessageDialog(null, "Forma de compra a editar: "+nl+nl+"   Tipo compra: "+formaCompra+nl+Constantes.ESPACIO); 
	                EditarFormaCompra editarFormaCompra = new EditarFormaCompra(id,formaCompra); 
	                // Coloca la ventana en el centro de la pantalla 
	                editarFormaCompra.setLocationRelativeTo(null); 
	                // Hace visible la ventana 
	                editarFormaCompra.setVisible(true); 
	                // Desaparece esta ventana 
	                setVisible(false); 
	                          
	            } // Cierre del mouseClicked 
	        });         
	        scrollPane.setViewportView(tabla);// Se agrega la tabla al panel scroll 
	        
	        // Version label 
	        contentPane.add(Utilidades.obtenerVersionLabel(0, 414, 95, 14)); 
	        
	        contentPane.add(Utilidades.obtenerAvisoInfoLabel(24, 27, 300, 14, Constantes.LIT_INFO_EDITAR_FORMA_COMPRA)); // Aviso de seleccion 
	                
	        btnMenu = Utilidades.obtenerBoton(100, 368, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
	        btnMenu.addActionListener((ActionListener)this); 
	        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
	        contentPane.add(btnMenu); 
                
        } // Cierre componentes 

        @SuppressWarnings("unused") 
        public void actionPerformed(ActionEvent evento) {         

            // Se guardan los datos y vuelve a la principal 
            if(evento.getSource()==btnMenu){ 
            	
				ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
				this.setVisible(false);
            }                 
        } // Cierre actionPerformed 
        
} // Cierre clase

