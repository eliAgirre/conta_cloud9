package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.ValidacionesComun;

public class EditarPresupuesto extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtID;
	private JTextField txtImporte;
	private JTextField dateChooser;
	private JRadioButton rbSemanal;
	private JRadioButton rbMensual;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnEditar;
	private JButton btnVolver;
	private JButton btnBorrar;
	private String idPresu;
	
	// Atributos del resultados user
	private String resultFecha;
	private String resultTipo;
	private Double resultImporte;
		
	public EditarPresupuesto(String idPresupuesto, String fecha, String presu, String tipo){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 499, 366); // size
		setTitle(Constantes.VISTA_EDITAR_PRESUPUESTO); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		idPresu = idPresupuesto;
		
		// Carga los componentes de la vista
		componentes(presu, tipo);
		
	} // Constructor
	
	private void componentes(String presu, String tipo){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Labels + textFields + comboBox + buttons
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 312, 95, 14));
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 254, 14, Constantes.LIT_INFO_MODIF)); // Aviso de modificacion
		
		contentPane.add(Utilidades.obtenerLabel(31, 89, 48, 14, Constantes.LABEL_ID+Constantes.ESPACIO+Constantes.PUNTOS)); // Label id
		
		txtID = Utilidades.obtenerTextField(80, 89, 126, 20, 10, txtID, idPresu, false);
		txtID.addActionListener((ActionListener)this);
		contentPane.add(txtID);
		
		contentPane.add(Utilidades.obtenerLabel(31, 164, 46, 14, Constantes.LABEL_FECHA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha
		
		dateChooser = Utilidades.obtenerTextField(80, 164, 164, 20, 10, dateChooser, "dateChooser", true); // fecha presu
		dateChooser.addPropertyChangeListener(Constantes.INPUT_FECHA, (PropertyChangeListener) this);
		contentPane.add(dateChooser);
		
		contentPane.add(Utilidades.obtenerLabel(255, 92, 95, 14, Constantes.LABEL_TIPO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label tipo
		
		rbSemanal = Utilidades.obtenerRadio(340, 76, 109, 23, Constantes.RADIO_SEMANAL, true, buttonGroup, rbSemanal);
		rbSemanal.addActionListener((ActionListener)this);
		contentPane.add(rbSemanal);
		
		rbMensual = Utilidades.obtenerRadio(340, 112, 109, 23, Constantes.RADIO_MENSUAL, true, buttonGroup, rbMensual);
		rbMensual.addActionListener((ActionListener)this);
		contentPane.add(rbMensual);
		
		contentPane.add(Utilidades.obtenerLabel(255, 164, 95, 14, Constantes.LABEL_PRESUPUESTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label presupuesto
		
		txtImporte = Utilidades.obtenerTextField(340, 161, 126, 20, 10, txtImporte, presu, true);
		txtImporte.addActionListener((ActionListener)this);
		contentPane.add(txtImporte);
		
		btnEditar = Utilidades.obtenerBoton(216, 250, 108, 41, Constantes.BTN_EDITAR, btnEditar); // boton editar
		btnEditar.addActionListener((ActionListener)this);
		btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT2+Constantes.EXTENSION_PNG)));
		contentPane.add(btnEditar);
		
		btnVolver = Utilidades.obtenerBoton(96, 250, 108, 41, Constantes.BTN_VOLVER, btnVolver); // boton volver
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_BACK+Constantes.EXTENSION_PNG)));
		contentPane.add(btnVolver);
		
		btnBorrar = Utilidades.obtenerBoton(336, 250, 111, 41, Constantes.BTN_BORRAR, btnBorrar); // boton borrar
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_DELETE+Constantes.EXTENSION_PNG)));
		contentPane.add(btnBorrar);
		
	} // Cierre componentes
	

	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {

		if(evento.getSource()==rbSemanal){

			resultTipo=rbSemanal.getText();
		}

		if(evento.getSource()==rbMensual){

			resultTipo=rbMensual.getText();
		}

		if(evento.getSource()==btnVolver){
			
			TablaPresupuestos ventanaTablaPresupuestos=new TablaPresupuestos();
			ventanaTablaPresupuestos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaPresupuestos.setVisible(true);
	  		// Desparece esta ventana
	  		setVisible(false);
		}

		if(evento.getSource()==btnEditar){
			
        	boolean validar = false;
        	
            if(UtilesValida.esNulo(resultTipo)) {
            	resultTipo=rbSemanal.getText(); 
            }
        	
        	validar = ValidacionesComun.validarCamposPresu(resultFecha, resultTipo, txtImporte.getText(), getTxtImporte()); 
        	
        	if(validar) {
        		
        		resultImporte = Double.valueOf(txtImporte.getText());
        		
				// Se obtiene la ultima linea del el fichero log
				String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_PRESUPUESTOS);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) {
						
						FicherosLog.escribirPresuLog(ListaConstantesValidacion.TIPO_COMUNICACION[1], Integer.valueOf(idPresu), resultFecha, resultTipo, String.valueOf(resultImporte));
						
						ArrayList<String> listaPresupuestos = FicherosRegistros.editarListaPresus(String.valueOf(idPresu), resultFecha, resultTipo, String.valueOf(resultImporte));
						
						if(!UtilesValida.esNula(listaPresupuestos)) {
							
							if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_PRESUPUESTOS)) {
								
								if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_PRESUPUESTOS)) {
									
									FicherosRegistros.insertarRegistros(listaPresupuestos, Constantes.FICHERO_PRESUPUESTOS);
									
									JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_MODI);

							  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

							  		setVisible(false); // Desparece esta ventana
								}
								else
									JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
							}
							else
								JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO);
						}
						else 
							JOptionPane.showMessageDialog(null, Constantes.ERROR_MODI);
					}
				}
        		
        	}
			
		} // editar
		
        if(evento.getSource()==btnBorrar){ 
            
            // Se obtiene la ultima linea del el fichero log 
            String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_PRESUPUESTOS); 
            
            if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                    
                if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) { 
                	
                	boolean validar = false;
                	
                    if(UtilesValida.esNulo(resultTipo)) {
                    	resultTipo=rbSemanal.getText(); 
                    }
                	
                	validar = ValidacionesComun.validarCamposPresu(resultFecha, resultTipo, txtImporte.getText(), getTxtImporte());
                        
                    if(validar) {
                    	
    	        		resultImporte = Double.valueOf(txtImporte.getText());
                    
    	        		FicherosLog.escribirPresuLog(ListaConstantesValidacion.TIPO_COMUNICACION[2], Integer.valueOf(idPresu), resultFecha, resultTipo, String.valueOf(resultImporte)); 
                        
                        ArrayList<String> listaPresus = FicherosRegistros.borrarRegistroPresu(String.valueOf(idPresu)); 
                        
                        if(!UtilesValida.esNula(listaPresus)) { 
                                
                            if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_PRESUPUESTOS)) { 
                                    
                                if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_PRESUPUESTOS)) { 
                                        
                                    FicherosRegistros.insertarRegistros(listaPresus, Constantes.FICHERO_PRESUPUESTOS); 
                                    
                                    JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_BAJA); 

                                    ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                                    setVisible(false); // Desparece esta vemtana 
                                } 
                                else
                                    JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
                            } 
                            else 
                            	JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO);
                        } 
                        else
                            JOptionPane.showMessageDialog(null, Constantes.ERROR_BAJA);
                    }      
                } 
            } 
        } // borrar
	} // Cierre del metodo actionPerformed
	
	// Getters
	public JTextField getTxtImporte() {
		return txtImporte;
	}
	
} // Cierre clase