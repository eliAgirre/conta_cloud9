package vista;

import java.awt.HeadlessException;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;

import controlador.ControladorPrincipal;
import gestor.Compras;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class AnadirCompra extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private int idCompra;
	private JButton btnMenu;
	private JButton btnGuardar;
	private JButton btnOtroMas;
	private static JTextField txtProducto;
	private static JTextField txtImporte;
	private static JComboBox<String> cbTiendas;
	private static JComboBox<String> cbFormaCompra;
	private static JComboBox<String> cbFormaPago;
	private static JTextField txtFechaCompra;
	
	// Resultados a usar
	private String resultFechaCompra;
	private String resultTienda;
	private String resultProducto;
	private double resultImporte;
	private String resultFormaCompra;
	private String resultFormaPago;
	
	// Listas
	private Compras listaCompras = new Compras();
	
	public AnadirCompra(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 699, 327); // size
		setTitle(Constantes.VISTA_ANADIR_COMPRA); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Carga los componentes de la ventana
		componentes();
		
        // LISTA DE TIENDAS 
        try { 
            if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_TIENDAS), cbTiendas) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (HeadlessException e) { 
                JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (FileNotFoundException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
        } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
        } 

        // LISTA FORMA COMPRA                 
        try{ 
            if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_COMPRA), cbFormaCompra) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_COMPRA+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (HeadlessException e) { 
                JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_COMPRA+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);; 
        } catch (FileNotFoundException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
        } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
        } 
        
        // LISTA FORMA DE PAGO 
        try{ 
            if(UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_PAGO), cbFormaPago) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_PAGO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (HeadlessException e) { 
                JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_PAGO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (FileNotFoundException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
        } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
        } 


	} // Cierre constructor
	
	private void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Labels + textFields + comboBox + buttons
		contentPane.add(Utilidades.obtenerVersionLabel(10, 264, 95, 14)); // Version label
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_INFORMACION)); // Aviso
		
		contentPane.add(Utilidades.obtenerLabel(48, 81, 101, 14, Constantes.LABEL_FECHA_COMPRA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha compra
		
		txtFechaCompra = Utilidades.obtenerTextField(159, 81, 164, 20, 10, txtFechaCompra, null, true); // fecha compra
		txtFechaCompra.addActionListener((ActionListener)this);
		contentPane.add(txtFechaCompra);
		
		contentPane.add(Utilidades.obtenerLabel(48, 125, 92, 14, Constantes.LABEL_TIENDA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label tienda
			
		cbTiendas = Utilidades.obtenerCombo(159, 122, 164, 20, cbTiendas); // combo tiendas
		cbTiendas.addActionListener((ActionListener)this);
		contentPane.add(cbTiendas);
		
		contentPane.add(Utilidades.obtenerLabel(48, 175, 95, 14, Constantes.LABEL_PRODUCTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label producto
		
		txtProducto = Utilidades.obtenerTextField(159, 171, 164, 20, 10, txtProducto, null, true);  // txtProducto field
		txtProducto.addActionListener((ActionListener)this);
		contentPane.add(txtProducto);
		
		contentPane.add(Utilidades.obtenerLabel(365, 175, 95, 14, Constantes.LABEL_IMPORTE+Constantes.ESPACIO+Constantes.PUNTOS)); // Label importe
		
		txtImporte = Utilidades.obtenerTextField(463, 171, 164, 20, 10, txtImporte, null, true);  // txtImporte field
		txtImporte.addActionListener((ActionListener)this);
		contentPane.add(txtImporte);
		
		contentPane.add(Utilidades.obtenerLabel(364, 81, 126, 14, Constantes.LABEL_FORMA_COMPRA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label forma compra
		
		cbFormaCompra = Utilidades.obtenerCombo(463, 81, 164, 20, cbFormaCompra); // combo forma compra
		cbFormaCompra.addActionListener((ActionListener)this);
		contentPane.add(cbFormaCompra);
		
		contentPane.add(Utilidades.obtenerLabel(364, 125, 95, 14, Constantes.LABEL_FORMA_PAGO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label forma pago
		
		cbFormaPago = Utilidades.obtenerCombo(463, 122, 164, 20, cbFormaPago); // combo forma pago
		cbFormaPago.addActionListener((ActionListener)this);
		contentPane.add(cbFormaPago);
		
		btnMenu = Utilidades.obtenerBoton(167, 222, 126, 41, Constantes.BTN_MENU, btnMenu); // boton menu
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG)));
		contentPane.add(btnMenu); 
		
		btnGuardar = Utilidades.obtenerBoton(320, 222, 115, 41, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG)));
		contentPane.add(btnGuardar);
		
		btnOtroMas = Utilidades.obtenerBoton(465, 222, 115, 41, Constantes.BTN_OTRO, btnOtroMas);  // boton otro
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnOtroMas);
		
	} // Cierre componentes
	
	// Limpia los datos de los componentes
	private void limpiar(){
		
		cbTiendas.setSelectedItem(Constantes.VACIO);
		txtProducto.setText(Constantes.VACIO);
		txtImporte.setText(Constantes.VACIO);
		txtFechaCompra.setText(Constantes.VACIO);
		cbFormaCompra.setSelectedItem(Constantes.VACIO);
		cbFormaPago.setSelectedItem(Constantes.VACIO);
		
	} // Cierre limpiar
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {

		if(evento.getSource()==cbTiendas){
			
			if(cbTiendas.isValid())
				resultTienda=cbTiendas.getSelectedItem().toString();
		}

		if(evento.getSource()==cbFormaCompra){
			
			if(cbFormaCompra.isValid())
				resultFormaCompra=cbFormaCompra.getSelectedItem().toString();
		}
		
		if(evento.getSource()==cbFormaPago){
			
			if(cbFormaPago.isValid())
				resultFormaPago=cbFormaPago.getSelectedItem().toString();
		}
		
		// Si hace clic en Otro, se van agregando registros al array hasta que da al Menu
		if(evento.getSource()==btnOtroMas){
			
			boolean validar=false;			
		
			validar = utilidades.ValidacionesComun.validarCamposCompra(txtFechaCompra.getText(), resultTienda, txtProducto.getText(), txtImporte.getText(), resultFormaCompra, resultFormaPago, txtProducto, txtImporte);

			if(validar) {
				
				resultFechaCompra 	= txtFechaCompra.getText();
				resultImporte 		= Double.valueOf(txtImporte.getText());
				resultProducto 		= txtProducto.getText();
				
				if(!FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_COMPRAS))
					FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_COMPRAS);
				
				if(!FicherosLog.ficheroLogCreado(Constantes.FICHERO_COMPRAS))
					FicherosLog.ficheroLogCreado(Constantes.FICHERO_COMPRAS);
				
				// Se obtiene la ultima linea del el fichero log
				//String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_COMPRAS)
				String ultimaLinea = utilidades.FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_COMPRAS);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia
						
						utilidades.FicherosLog.cabeceraComprasLog(); // se inserta la cabecera
						
						idCompra++;
						
						Utilidades.agregarCompraALista(idCompra, resultFechaCompra, resultTienda, resultImporte, resultProducto, resultFormaCompra, resultFormaPago, listaCompras);
						
						FicherosRegistros.insertarRegistroCompra(String.valueOf(idCompra), resultFechaCompra, resultTienda, resultProducto, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago);
						
						// Se limpian los componentes
						limpiar();
					}
					else {
						
						if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){ // Si el fichero no existe
							
							boolean ficheroLogCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_COMPRAS);
							boolean ficheroCreado = utilidades.FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_COMPRAS);
							
							if(ficheroCreado && ficheroLogCreado) { // Si el fichero se ha creado
								
								utilidades.FicherosLog.cabeceraComprasLog(); // Se inserta en la cabecera
								
								idCompra++;
								
								Utilidades.agregarCompraALista(idCompra, resultFechaCompra, resultTienda, resultImporte, resultProducto, resultFormaCompra, resultFormaPago, listaCompras);
								
								FicherosRegistros.insertarRegistroCompra(String.valueOf(idCompra), resultFechaCompra, resultTienda, resultProducto, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago);
								
								// Se limpian los componentes
								limpiar();
							}
						}
						else {
							
							int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
							String ultimoID = ultimaLinea.substring(0,pos); // ultima linea ID desde registros
							//String ultimoID = ultimaLinea.substring(32, 35); // Se obtiene la ultima ID desde el log
							idCompra = Integer.valueOf(ultimoID.trim());
							idCompra++;
							
							Utilidades.agregarCompraALista(idCompra, resultFechaCompra, resultTienda, resultImporte, resultProducto, resultFormaCompra, resultFormaPago, listaCompras);
							
							// Se limpian los componentes
							limpiar();							
						}
					} // Si la ultima linea no es vacia
				} // Si el fichero es nula
			}
		}

		if(evento.getSource()==btnMenu){ // va a la ventana principal

			ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			setVisible(false);
		}
		
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnGuardar){
			
			if(listaCompras.length()==0){ // Si en la lista no hay compras, se guardara una sola compra
				
				boolean validar=false;			

				validar = utilidades.ValidacionesComun.validarCamposCompra(txtFechaCompra.getText(), resultTienda, txtProducto.getText(), txtImporte.getText(), resultFormaCompra, resultFormaPago, txtProducto, txtImporte);	
				
				if(validar){	
					
					resultFechaCompra 	= txtFechaCompra.getText();
					resultImporte 		= Double.valueOf(txtImporte.getText());
					resultProducto 		= txtProducto.getText();

					// Se obtiene la ultima linea del el fichero log
					//String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_COMPRAS);
					String ultimaLinea = utilidades.FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_COMPRAS);
					
					if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
						
						if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia, se inserta la cabecera y un registro.
							
							Utilidades.escribirCabeceraRegistroCompras(idCompra, resultFechaCompra, resultTienda, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago, resultProducto);
							
							JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

					  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

					  		setVisible(false); // Desparece la ventana
						}
						else {
							
							if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){
								
								boolean ficheroLogCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_COMPRAS);
								boolean ficheroCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_COMPRAS);
								
								if(ficheroCreado && ficheroLogCreado) {
									
									Utilidades.escribirCabeceraRegistroCompras(idCompra, resultFechaCompra, resultTienda, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago, resultProducto);

									JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

							  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

							  		setVisible(false); // Desparece esta ventana
								}
								
							}
							else {
								
								boolean esCabecera = utilidades.Utilidades.compararUltimaLineaConCabeceraCompras(ultimaLinea);
								
								boolean esRaya = utilidades.Utilidades.compararUltimaLineaConRaya(ultimaLinea);
								
								if(!esCabecera && !esRaya){
									int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
									String ultimoID = ultimaLinea.substring(0,pos);
									//String ultimoID = ultimaLinea.substring(32, 35);
									idCompra = Integer.valueOf(ultimoID.trim());
									idCompra++;
								}
								
								utilidades.FicherosLog.escribirComprasLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idCompra, resultFechaCompra, resultTienda, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago, resultProducto);
								
								FicherosRegistros.insertarRegistroCompra(String.valueOf(idCompra), resultFechaCompra, resultTienda, resultProducto, String.valueOf(resultImporte), resultFormaCompra, resultFormaPago);
								
								JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

						  		setVisible(false); // Desparece esta ventana
							}
						}
					}
					
				}
				
			}
			else if(listaCompras.length()!=0){ // Si en la lista hay compras, se guardaran varias compras en el log
				
				boolean validar=false;			

				validar = utilidades.ValidacionesComun.validarCamposCompra(txtFechaCompra.getText(), resultTienda, txtProducto.getText(), txtImporte.getText(), resultFormaCompra, resultFormaPago, txtProducto, txtImporte);	
				
				if(validar){
					
					resultFechaCompra 	= txtFechaCompra.getText();
					resultImporte 		= Double.valueOf(txtImporte.getText());
					resultProducto 		= txtProducto.getText();
						
					idCompra = listaCompras.getUltimoID(listaCompras.length()-1);
					
					idCompra++;
					
					Utilidades.agregarCompraALista(idCompra, resultFechaCompra, resultTienda, resultImporte, resultProducto, resultFormaCompra, resultFormaPago, listaCompras);
						
				}
				
				for(int i=0; i<listaCompras.length(); i++) {
					
					utilidades.FicherosLog.escribirComprasLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], listaCompras.getCompra(i).getIdCompra(), 
							listaCompras.getCompra(i).getFechaCompra(), listaCompras.getCompra(i).getTienda(), String.valueOf(listaCompras.getCompra(i).getImporte()), 
							listaCompras.getCompra(i).getFormaCompra(), listaCompras.getCompra(i).getFormaPago(), listaCompras.getCompra(i).getProducto());
					
					FicherosRegistros.insertarRegistroCompra(String.valueOf(listaCompras.getCompra(i).getIdCompra()), listaCompras.getCompra(i).getFechaCompra(),
							listaCompras.getCompra(i).getTienda(), listaCompras.getCompra(i).getProducto(), String.valueOf(listaCompras.getCompra(i).getImporte()), 
							listaCompras.getCompra(i).getFormaCompra(), listaCompras.getCompra(i).getFormaPago());
					
				}
				
				JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

		  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
		  		
		  		setVisible(false);
				
			} // Lista de la compra 
		} // Evento guardar
		
	} // Cierre actionPerformed
	
	// getters
	public static JTextField getTxtProducto() {
		return txtProducto;
	}

	public static JTextField getTxtCoste() {
		return txtImporte;
	}
	
} // Cierre de clase