package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla; 
 
public class VerGastosFijos extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
        
	// Atributos de la clase 
	private JPanel contentPane; 
	private JComboBox<String> cbTipoGasto; 
	private JButton btnVer; 
	private JButton btnMenu; 
	private JButton btnTodos;
	private JLabel lblTotal;
	
	// Atributos relacionados con la tabla 
	private JScrollPane scrollPane; 
	private DefaultTableModel modelo; 
	private String [][] arrayTabla; //array bidimensional 
	private JTable tabla; 
	private static String[] datosFile=new String[Constantes.NUM_COLUMNAS_GASTOS_FIJOS];
	private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_GASTOS_FIJOS);
	
	// Atributos del resultados user 
	private String resultTipoGasto; 
                
	public VerGastosFijos(){
	        
	    // Caracteristicas de la ventana 
	    setResizable(false); // no reestablece el size 
	    setBounds(100, 100, 789, 609); // size 
	    setTitle(Constantes.VISTA_VER_GASTOS_FIJOS); // titulo 
	    setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo
	    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
	    
	    // Carga los componentes de la ventana 
	    componentes();
	    
		// LISTA DE GASTOS FIJOS	
		/*if(utilidades.UtilesValida.esNulo( Utilidades.getCombo(Utilidades.getLista(Constantes.MODELO_GASTO_FIJO), cbTipoGasto) ))
			JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);*/
	        
	} // Constructor
        
    public void componentes(){
    	
        double total = Constantes.DEFAULT_DOBLE_CERO;
            
        // Layout           
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
        
        // Panel para visualizar y hacer scroll
        scrollPane = Utilidades.obtenerScroll(24, 153, 732, 303, scrollPane, false);
        contentPane.add(scrollPane);
        
        // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
        modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_GASTOS_FIJOS);
        
        // Se le pasa a JTable el modelo de tabla 
        tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);
        modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
                    
        // Labels + textFields + comboBox + buttons 
        contentPane.add(Utilidades.obtenerVersionLabel(0, 554, 95, 14)); // Version label 
        
        contentPane.add(Utilidades.obtenerAvisoInfoLabel(32, 30, 268, 20, Constantes.LIT_INFO_VER_GASTOS+Constantes.PUNTOS)); // Aviso de visualizacion
        
        cbTipoGasto = Utilidades.obtenerCombo(247, 78, 141, 20, cbTipoGasto); 
        cbTipoGasto.addActionListener((ActionListener)this); 
        contentPane.add(cbTipoGasto); 
        
        contentPane.add(Utilidades.obtenerLabel(630, 481, 50, 14, Constantes.LABEL_TOTAL+Constantes.PUNTOS+Constantes.ESPACIO)); // label total
        
        lblTotal = Utilidades.obtenerLabel(678, 481, 78, 14, String.valueOf(calcularTotal(total, null))); // cantidad total            
        contentPane.add(lblTotal);
        
        btnVer = Utilidades.obtenerBoton(496, 68, 110, 41, Constantes.BTN_VER, btnVer); // boton ver 
        btnVer.addActionListener((ActionListener)this); 
        btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnVer); 
        
        btnTodos = Utilidades.obtenerBoton(647, 68, 109, 41, Constantes.BTN_TODOS, btnTodos); // boton todos 
        btnTodos.addActionListener((ActionListener)this); 
        btnTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ALL+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnTodos); 
        
        btnMenu = Utilidades.obtenerBoton(344, 528, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu); 
            
    } // Cierre componentes 
        
    @SuppressWarnings("unused")
    public void actionPerformed(ActionEvent evento) { 
        
        if(evento.getSource()==cbTipoGasto){ // si selecciona el tip de gasto se guarda 
                
            if(cbTipoGasto.isValid()){ 
            	resultTipoGasto = cbTipoGasto.getSelectedItem().toString(); 
            } 
        } 
        
        if(evento.getSource()==btnVer){
        	
        	double total = Constantes.DEFAULT_DOBLE_CERO;

            UtilidadesTabla.limpiarTabla(modelo); 
            
            if(!UtilesValida.esNulo(resultTipoGasto)){ //si el combo es seleccionado
                                                    
                if(!UtilesValida.esNulo(registros)){
                	
                	lblTotal.setText(String.valueOf(calcularTotal(total, resultTipoGasto)));
                        
                    for(int i=0; i<registros.size(); i++){ 
                            
                        datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                        
                        if(datosFile[5].equals(resultTipoGasto)){ 
                                
                        	modelo.addRow(datosFile); 
                        } 
                    } 
                        
                } 
                    
            } 
            else if(UtilesValida.esNulo(resultTipoGasto)){ // si no se ha seleccionado el combo
            	
            	total = Constantes.DEFAULT_DOBLE_CERO;
                
                UtilidadesTabla.limpiarTabla(modelo);
            	
                if(!UtilesValida.esNulo(registros)){
                	
                	lblTotal.setText(String.valueOf(calcularTotal(total, null)));
                        
                	modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                        
                } 
            } 

        } 

        if(evento.getSource()==btnTodos){

        	double total = Constantes.DEFAULT_DOBLE_CERO;
            
            UtilidadesTabla.limpiarTabla(modelo);
            
            if(!UtilesValida.esNulo(registros)){
            	
            	lblTotal.setText(String.valueOf(calcularTotal(total, null)));
                    
            	modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
                    
            } 
                
        } 
        // Si hace clic en menu se guardan los datos y vuelve a la principal 
        if(evento.getSource()==btnMenu){                 

			ControladorPrincipal controladorPrincipal = new ControladorPrincipal(); 
			setVisible(false); 
        } 
            
    } // Cierre actionPerformed
        
    private double calcularTotal(double total, String tipoGasto) {
    	
    	String[] dato = null;
    	
        if(!UtilesValida.esNula(registros)) {
        	for(int i=0; i<registros.size(); i++) {
        		dato = registros.get(i).split(Constantes.PUNTO_COMA);
        		
        		if(!UtilesValida.esNulo(tipoGasto)) {
        			
        			if(dato[5].equals(tipoGasto)){
	            		if(UtilesValida.esDoble(dato[7]))
	            			total = total + Double.parseDouble(dato[7]);
            		}
        			else if(!dato[5].equals(tipoGasto)){
        				total = Constantes.DEFAULT_DOBLE_CERO;
        			}
        			
        		}
        		else if(UtilesValida.esNulo(tipoGasto)) {
        			
        			if(UtilesValida.esDoble(dato[7]))
            			total = total + Double.parseDouble(dato[7]);
        		}
            }
        }
    	
    	return total;
    } // calcular total
        
} // Cierre clase 