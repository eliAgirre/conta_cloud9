package vista;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla;

public class TablaCompras extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JComboBox<String> cbTiendas;
	private JTextField dateChooser;
	private JButton btnBuscar;
	private JButton btnMenu;
	private JButton btnTodos;
	
	// Atributos relacionados con la tabla
	private JScrollPane scrollPane;
	private DefaultTableModel modelo;
	private String [][] arrayTabla; //array bidimensional
	private JTable tabla;
	private static String[] datosFile=new String[Constantes.NUM_COLUMNAS_COMPRAS];
	private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_COMPRAS);
	
	// Atributos para la siguiente ventana
	private String id;
	private String fecha;
	private String tienda;
	private String importe;
	private String formaPago;
	private String formaCompra;
	private String producto;
	
	// Atributos del resultados user
	private String resultTienda;	
	private String resultFecha;
	
	public TablaCompras(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 673, 621); // size
		setTitle(Constantes.VISTA_COMPRAS_A_EDITAR); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();
		
        try { 
            if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_TIENDAS), cbTiendas) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (HeadlessException e) { 
                JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (FileNotFoundException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
        } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
        }
		
		modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Panel para visualizar y hacer scroll
        scrollPane = Utilidades.obtenerScroll(24, 144, 628, 369, scrollPane, false);
        contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS);
		
		// Se le pasa a JTable el modelo de tabla		
		tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);
		utilidades.UtilidadesTabla.anchoColumnasTablaCompras(tabla);

		// Se agrega un listener a la tabla
		tabla.addMouseListener(new MouseAdapter() {
			// Cuando el raton seleccione un dato de una fila muestra un aviso
			public void mouseClicked(MouseEvent evento) {
		    	
				String nl = System.getProperty(Constantes.PROPERTY_SALTO_LINEA);
		        int fila = tabla.rowAtPoint(evento.getPoint());
		        int columna = 0;
		        if ((fila > -1) && (columna > -1))
	        	id = String.valueOf(modelo.getValueAt(fila,columna));
		        fecha = String.valueOf( modelo.getValueAt(fila,1));
		        tienda = String.valueOf( modelo.getValueAt(fila,2));
		        producto = String.valueOf( modelo.getValueAt(fila,3));
		        importe = String.valueOf( modelo.getValueAt(fila,4));
		        formaCompra = String.valueOf(modelo.getValueAt(fila,5));
		        formaPago = String.valueOf( modelo.getValueAt(fila,6));
		        if(UtilesValida.esFechaCastellano(fecha)) {
			        JOptionPane.showMessageDialog(null, Constantes.MSG_REGISTRO_A_EDITAR+Constantes.PUNTOS+Constantes.ESPACIO+nl+nl+"   Dia: "+fecha+nl+"   Tienda: "+tienda+nl+"   Producto: "+producto+nl+"   Coste: "+importe+nl+Constantes.ESPACIO);
			        EditarCompra editarCompra = new EditarCompra(id, fecha, tienda, producto, importe, formaCompra, formaPago);
					// Coloca la ventana en el centro de la pantalla
			        editarCompra.setLocationRelativeTo(null);
					// Hace visible la ventana
			        editarCompra.setVisible(true);
					setVisible(false);
		        }
		        else
		        	JOptionPane.showMessageDialog(null, "La fecha de la compra no es correcta.");
				 
		    } // Cierre del mouseClicked
		});	
		scrollPane.setViewportView(tabla);
		
		// Labels + textFields + comboBox + buttons
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 567, 95, 14));
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(24, 36, 254, 14, Constantes.LIT_INFO_EDITAR_COMPRAS)); // Aviso de compra a editar
	
		contentPane.add(Utilidades.obtenerLabel(286, 61, 46, 14, Constantes.COMPRAS_COLUMNA_FECHA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha compra
		
		dateChooser = Utilidades.obtenerTextField(353, 49, 126, 20, 10, dateChooser, null, true); // fecha compra
		contentPane.add(dateChooser);
		
		contentPane.add(Utilidades.obtenerLabel(286, 98, 64, 14, Constantes.LABEL_TIENDA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label tienda
		
		cbTiendas = Utilidades.obtenerCombo(353, 98, 126, 20, cbTiendas);
		cbTiendas.addActionListener((ActionListener)this);
		contentPane.add(cbTiendas);
		
		btnBuscar = Utilidades.obtenerBoton(508, 82, 126, 41, Constantes.BTN_BUSCAR, btnBuscar); // boton buscar 
		btnBuscar.addActionListener((ActionListener)this); 
		btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SEARCH+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnBuscar);
        
        btnTodos = Utilidades.obtenerBoton(351, 540, 109, 41, Constantes.BTN_TODOS, btnTodos); // boton todos 
        btnTodos.addActionListener((ActionListener)this); 
        btnTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ALL+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnTodos);

        btnMenu = Utilidades.obtenerBoton(214, 540, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	

		if(evento.getSource()==cbTiendas){
			
			if(cbTiendas.isValid())
				resultTienda=cbTiendas.getSelectedItem().toString();
		}

		if(evento.getSource()==btnBuscar){
			// Limpia la tabla
			utilidades.UtilidadesTabla.limpiarTabla(modelo);
			
			if(!utilidades.UtilesValida.esNulo(cbTiendas.getSelectedItem()) && !utilidades.UtilesValida.esNulo(resultTienda)){// Si la seleccion de la tienda no es nula
				
				if(utilidades.UtilesValida.esNulo(resultFecha)){
					
					if(!UtilesValida.esNulo(registros)) {
						
						for(int i=0;i<registros.size();i++) {
							
							datosFile = registros.get(i).split(Constantes.PUNTO_COMA);
							
							if(datosFile[2].equals(cbTiendas.getSelectedItem()) || datosFile[2].equals(resultTienda)) {
								// Se agrega cada fila al modelo de tabla
								modelo.addRow(datosFile);
							}
						}
					}
					
					
				} else if(!utilidades.UtilesValida.esNulo(resultFecha)){
					
					if(!UtilesValida.esNulo(registros)) {
						
						for(int i=0;i<registros.size();i++) {
							
							datosFile = registros.get(i).split(Constantes.PUNTO_COMA);
							
							if(datosFile[1].equals(resultFecha) && (datosFile[2].equals(cbTiendas.getSelectedItem()) || datosFile[2].equals(resultTienda)) ) {
								// Se agrega cada fila al modelo de tabla
								modelo.addRow(datosFile);
							}
						}
					}
					
				}
			}
			else{
				
				if(utilidades.UtilesValida.esNulo(resultFecha)){
					
					modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
				}
				else{ // Si la fecha no es nula

					if(!UtilesValida.esNulo(registros)) {
						
						for(int i=0;i<registros.size();i++) {
							
							datosFile = registros.get(i).split(Constantes.PUNTO_COMA);
							
							if(datosFile[1].equals(resultFecha)) {
								// Se agrega cada fila al modelo de tabla
								modelo.addRow(datosFile);
							}
						}
					}
				}
			}
		} // buscar

		if(evento.getSource()==btnTodos){
			
			utilidades.UtilidadesTabla.limpiarTabla(modelo);
			limpiarComponentes();
			
			modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
		}

		if(evento.getSource()==btnMenu){ 
			
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		setVisible(false);
		}
	} // Cierre del metodo actionPerformed
	
	private void limpiarComponentes(){	

		cbTiendas.setSelectedItem(Constantes.VACIO);
		resultFecha = Constantes.VACIO;
		dateChooser.setText(Constantes.VACIO);
		
	} // Cierre limpiar
	
} // Cierre clase