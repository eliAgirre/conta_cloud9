package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class EditarGastosFijos extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtID;
	private JTextField dateDoc;
	private static JTextField txtNumFactura;
	private JComboBox<String> cbTipoGasto;
	private JTextField dateInicio;
	private JTextField dateFin;
	private static JTextField txtDuracion;
	private static JTextField txtImporte;
	private JButton btnVolver;
	private JButton btnEditar;
	private JButton btnBorrar;

	// Atributos del resultados user	
	private int idGastoFijo;
	private String resultFechaDoc;
	private String resultNumDoc;
	private String resultTipoGasto;
	private String resultFechaInicio;
	private String resultFechaFin;
	private String resultDuracion;
	private double resultImporte;
		
	
	public EditarGastosFijos(String id, String fechaDoc, String numDoc, String gastoFijo, String fechaInicio, String fechaFin, String duracion, String importe){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 638, 370); // size
		setTitle(Constantes.VISTA_EDITAR_GASTO_FIJO); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Carga los componentes de la ventana
		componentes(numDoc, gastoFijo, duracion, importe);
		
		if(UtilesValida.esNumero(id))
			idGastoFijo = Integer.valueOf(id);
		
		// LISTA DE GASTOS FIJOS
		/*if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_GASTO_FIJO), cbTipoGasto) ))
			JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);
		else
			resultTipoGasto = Utilidades.getItem( Utilidades.getLista(Constantes.MODELO_GASTO_FIJO), cbTipoGasto, gastoFijo).toString(); // muestra el item previamente elegido
		
		if(UtilesValida.esFecha(fechaDoc)) {
			dateDoc.setDate(Utilidades.parseaFecha(fechaDoc));
			resultFechaDoc = fechaDoc;
		}
		
		if(UtilesValida.esFecha(fechaInicio)) {
			dateInicio.setDate(Utilidades.parseaFecha(fechaInicio));
			resultFechaInicio = fechaInicio;
		}
		
		if(UtilesValida.esFecha(fechaFin)) {
			dateFin.setDate(Utilidades.parseaFecha(fechaFin));
			resultFechaFin = fechaFin;
		}	*/
				
	} // Constructor
	
	private void componentes(String numDoc, String gastoFijo, String duracion, String importe){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);

		// Labels + textFields + comboBox + buttons
        // Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 316, 95, 14));
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_INFO_MODIF)); // Aviso de modificacion
		
		contentPane.add(Utilidades.obtenerLabel(320, 36, 48, 14, Constantes.LABEL_ID+Constantes.ESPACIO+Constantes.PUNTOS)); // Label id 
		
		txtID = Utilidades.obtenerTextField(421, 33, 126, 20, 10, txtID, String.valueOf(idGastoFijo), false); // txtID
		txtID.addActionListener((ActionListener)this);
		contentPane.add(txtID);
		
		contentPane.add(Utilidades.obtenerLabel(48, 81, 95, 14, Constantes.LABEL_FECHA_DOC+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha doc
		
		dateDoc = Utilidades.obtenerTextField(159, 78, 126, 20, 10, dateDoc, "dateDoc", true); // fecha doc
		dateDoc.addActionListener((ActionListener)this);
		contentPane.add(dateDoc);
		
		contentPane.add(Utilidades.obtenerLabel(48, 122, 105, 14, Constantes.LABEL_NUM_DOC+Constantes.ESPACIO+Constantes.PUNTOS)); // Label num doc
		
		txtNumFactura = Utilidades.obtenerTextField(159, 119, 126, 20, 10, txtNumFactura, numDoc, true); // txtNumFactura
		txtNumFactura.addActionListener((ActionListener)this);
		contentPane.add(txtNumFactura);
		
		contentPane.add(Utilidades.obtenerLabel(43, 164, 75, 14, Constantes.LABEL_TIPO_GASTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label tipp gasto
		
		cbTipoGasto = Utilidades.obtenerCombo(159, 161, 126, 20, cbTipoGasto); // combo tipo gasto
		cbTipoGasto.addActionListener((ActionListener)this);
		contentPane.add(cbTipoGasto);
		
		contentPane.add(Utilidades.obtenerLabel(320, 81, 105, 14, Constantes.LABEL_FECHA_INICIO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha inicio
		
		dateInicio = Utilidades.obtenerTextField(421, 75, 126, 20, 10, dateInicio, "dateInicio", true); // fecha inicio
		dateInicio.addActionListener((ActionListener)this);
		contentPane.add(dateInicio);
		
		contentPane.add(Utilidades.obtenerLabel(320, 122, 105, 14, Constantes.LABEL_FECHA_FIN+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha fin
		
		dateFin = Utilidades.obtenerTextField(421, 119, 126, 20, 10, dateFin, "dateFin", true); // fecha fin
		dateFin.addActionListener((ActionListener)this);
		contentPane.add(dateFin);
		
		contentPane.add(Utilidades.obtenerLabel(320, 167, 75, 14, Constantes.LABEL_DURACION+Constantes.ESPACIO+Constantes.PUNTOS)); // Label duracion
		
		txtDuracion = Utilidades.obtenerTextField(421, 164, 126, 20, 10, txtDuracion, duracion, false); // txtDuracion
		txtDuracion.addActionListener((ActionListener)this);
		contentPane.add(txtDuracion);
		
		contentPane.add(Utilidades.obtenerLabel(320, 210, 75, 14, Constantes.LABEL_IMPORTE+Constantes.ESPACIO+Constantes.PUNTOS)); // Label importe
		
		txtImporte = Utilidades.obtenerTextField(421, 207, 126, 20, 10, txtImporte, importe, true); // txtImporte
		txtImporte.addActionListener((ActionListener)this);
		contentPane.add(txtImporte);
		
		btnVolver = Utilidades.obtenerBoton(126, 274, 108, 41, Constantes.BTN_VOLVER, btnVolver); // boton volver
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_BACK+Constantes.EXTENSION_PNG)));
		contentPane.add(btnVolver); 
		
		btnEditar = Utilidades.obtenerBoton(261, 274, 108, 41, Constantes.BTN_EDITAR, btnEditar); // boton editar
		btnEditar.addActionListener((ActionListener)this);
		btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT2+Constantes.EXTENSION_PNG)));
		contentPane.add(btnEditar); 
		
		btnBorrar = Utilidades.obtenerBoton(402, 274, 111, 41, Constantes.BTN_BORRAR, btnBorrar); // boton borrar
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_DELETE+Constantes.EXTENSION_PNG)));
		contentPane.add(btnBorrar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource()==cbTipoGasto){
			
			if(cbTipoGasto.isValid())
				resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
		} // combo

		if(evento.getSource()==btnVolver){

			TablaGastosFijos ventanaTablaIngresos=new TablaGastosFijos();
			ventanaTablaIngresos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaIngresos.setVisible(true);
	  		// Desparece esta ventana
	  		setVisible(false);
		} // volver

		if(evento.getSource()==btnEditar){
			
			if(UtilesValida.esNulo(resultTipoGasto))
				resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
			
			boolean validar = false;	
			
			validar = utilidades.ValidacionesComun.validarCamposGastosFijos(resultFechaDoc, txtNumFactura.getText(), resultFechaInicio, resultFechaFin, resultTipoGasto, txtDuracion.getText(), txtImporte.getText(), txtNumFactura, txtImporte);
			
			if(validar){
				
				resultNumDoc = txtNumFactura.getText();
				resultDuracion = txtDuracion.getText();
				resultImporte = Double.valueOf(txtImporte.getText());

				// Se obtiene la ultima linea del el fichero log
				String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_GASTOS_FIJOS);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) {
						
						FicherosLog.escribirGastoFijoLog(ListaConstantesValidacion.TIPO_COMUNICACION[1], idGastoFijo, resultFechaDoc, resultNumDoc, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte));
						
						ArrayList<String> listaGastosFijos = FicherosRegistros.editarListaGastosFijos(String.valueOf(idGastoFijo), resultFechaDoc, resultNumDoc, resultFechaInicio, resultFechaFin, resultTipoGasto, resultDuracion, String.valueOf(resultImporte));
						
						if(!UtilesValida.esNula(listaGastosFijos)) {
							
							if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_GASTOS_FIJOS)) {
								
								if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_GASTOS_FIJOS)) {
									
									FicherosRegistros.insertarRegistros(listaGastosFijos, Constantes.FICHERO_GASTOS_FIJOS);
									
									JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_MODI);

							  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

							  		setVisible(false); // Desparece esta ventana
								}
								else 
									JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
							}
							else 
								JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO);
						}
						else 
							JOptionPane.showMessageDialog(null, Constantes.ERROR_MODI);
					}
				}
			}
			
		} // editar
		
        if(evento.getSource()==btnBorrar){ 
            
            // Se obtiene la ultima linea del el fichero log 
            String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_GASTOS_FIJOS); 
            
            if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                    
                if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) { 
                	
                	boolean validar = false;
                	
                	validar = utilidades.ValidacionesComun.validarCamposGastosFijos(resultFechaDoc, txtNumFactura.getText(), resultFechaInicio, resultFechaFin, resultTipoGasto, txtDuracion.getText(), txtImporte.getText(), txtNumFactura, txtImporte);
                        
                    if(validar) {
                    	
                    	resultNumDoc = txtNumFactura.getText();
        				resultImporte = Double.valueOf(txtImporte.getText());
                    
        				FicherosLog.escribirGastoFijoLog(ListaConstantesValidacion.TIPO_COMUNICACION[2], idGastoFijo, resultFechaDoc, resultNumDoc, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte)); 
                        
                        ArrayList<String> listaGastosFijos = FicherosRegistros.borrarRegistroGastoFijo(String.valueOf(idGastoFijo)); 
                        
                        if(!UtilesValida.esNula(listaGastosFijos)) { 
                                
                            if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_GASTOS_FIJOS)) { 
                                    
                                if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_GASTOS_FIJOS)) { 
                                        
                                    FicherosRegistros.insertarRegistros(listaGastosFijos, Constantes.FICHERO_GASTOS_FIJOS); 
                                    
                                    JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_BAJA); 

                                    ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                                    setVisible(false); // Desparece esta vemtana 
                                } 
                                else { 
                                    JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO); 
                                } 
                                    
                            } 
                            else { 
                                    JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO); 
                            } 
                        } 
                        else { 
                                
                            JOptionPane.showMessageDialog(null, Constantes.ERROR_BAJA);       
                        }	
                    }      
                } 
            } 
        } // borrar 
		
	} // Cierre del metodo actionPerformed
	
	// getters
	public static JTextField getTxtImporte() {
		return txtImporte;
	}

	public static JTextField getTxtNumFactura() {
		return txtNumFactura;
	}

	public static JTextField getTxtDuracion() {
		return txtDuracion;
	}
	
} // Cierre clase