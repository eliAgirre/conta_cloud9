package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla;

public class BuscarProducto extends JFrame implements ActionListener, KeyListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtProducto;
	private JButton btnBuscar;
	private JButton btnMenu;
	
	// Atributos relacionados con la tabla
	private JScrollPane scrollPane;
	private DefaultTableModel modelo;
	private String [][] arrayTabla; //array bidimensional
	private JTable tabla;
	private static String[] datosFile=new String[Constantes.NUM_COLUMNAS_COMPRAS];
	private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_COMPRAS);
	
	// Atributos del resultados user
	private String resultProducto;
	
	public BuscarProducto(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 673, 621); // size
		setTitle(Constantes.VISTA_BUSCAR_PRODUCTO); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_SEARCH+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();

		modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);	
		
		// Panel para visualizar y hacer scroll
        scrollPane = Utilidades.obtenerScroll(24, 144, 628, 369, scrollPane, false);
        contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);
		
		// Labels + textFields + comboBox + buttons
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 550, 95, 14));
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(24, 36, 254, 14, Constantes.LIT_INFO_BUSCAR_PRODUCTO)); // Aviso de buscar producto
		
		txtProducto = Utilidades.obtenerTextField(41, 78, 312, 20, 10, txtProducto, null, true);  // producto field 
		txtProducto.addKeyListener((KeyListener)this);
        contentPane.add(txtProducto);
		
		btnBuscar = Utilidades.obtenerBoton(380, 69, 112, 41, Constantes.BTN_BUSCAR, btnBuscar); // boton buscar
		btnBuscar.addActionListener((ActionListener)this);
		btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SEARCH+Constantes.EXTENSION_PNG)));
		contentPane.add(btnBuscar); 
		
        btnMenu = Utilidades.obtenerBoton(287, 540, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@Override
	public void keyPressed(KeyEvent evento) {
		// Cuando presiona la tecla "Enter"
		if(evento.getKeyCode()==KeyEvent.VK_ENTER){
			
			resultProducto=txtProducto.getText(); // Se guarda el texto del producto
			
			UtilidadesTabla.limpiarTabla(modelo);
			
			if(!utilidades.UtilesValida.esNulo(txtProducto.getText()) && !utilidades.UtilesValida.esNulo(resultProducto)){// Si la seleccion del proudcto no es nula
				
				if(!UtilesValida.esNulo(registros)) {
					
					for(int i=0;i<registros.size();i++) {
						
						datosFile = registros.get(i).split(Constantes.PUNTO_COMA);
						
						if(datosFile[3].toLowerCase().equals(txtProducto.getText().toLowerCase()) || datosFile[3].toLowerCase().equals(resultProducto.toLowerCase())) {
							// Se agrega cada fila al modelo de tabla
							modelo.addRow(datosFile);
						}
						
					}
					
				}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent evento) {}

	@Override
	public void keyTyped(KeyEvent evento) {}
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {

		if(evento.getSource()==btnBuscar){
			
			resultProducto=txtProducto.getText(); // Se guarda el texto del producto
			
			UtilidadesTabla.limpiarTabla(modelo);
			
			if(!utilidades.UtilesValida.esNulo(txtProducto.getText()) && !utilidades.UtilesValida.esNulo(resultProducto)){// Si la seleccion del proudcto no es nula
				
				if(!UtilesValida.esNulo(registros)) {
					
					for(int i=0;i<registros.size();i++) {
						
						datosFile = registros.get(i).split(Constantes.PUNTO_COMA);
						
						if(datosFile[3].toLowerCase().equals(txtProducto.getText().toLowerCase()) || datosFile[3].toLowerCase().equals(resultProducto.toLowerCase())) {
							// Se agrega cada fila al modelo de tabla
							modelo.addRow(datosFile);
						}
						
					}
					
				}
			}
		}

		if(evento.getSource()==btnMenu){ 

	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		setVisible(false);
	  		
		} // menu
		
	} // Cierre actionPerformed
	
} // Cierre clase