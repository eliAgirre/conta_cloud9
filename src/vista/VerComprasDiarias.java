package vista; 

import java.awt.HeadlessException; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.io.FileNotFoundException; 
import java.io.IOException; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.border.EmptyBorder; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal; 
import utilidades.Constantes; 
import utilidades.FicherosRegistros; 
import utilidades.ListaConstantesValidacion; 
import utilidades.UtilesValida; 
import utilidades.Utilidades; 
import utilidades.UtilidadesTabla; 

public class VerComprasDiarias extends JFrame implements ActionListener{ 

        private static final long serialVersionUID = -8624753721794395368L; 
                
        // Atributos de la clase 
        private JPanel contentPane; 
        private JComboBox<String> cbTienda; 
        private JComboBox<String> cbFormaCompra; 
        private JComboBox<String> cbFormaPago; 
        private JButton btnConsultar; 
        private JButton btnDiario; 
        private JButton btnMenu; 
        private double total = Constantes.DEFAULT_DOBLE_CERO; 
        private JLabel lblTotal; 
        
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla; 
        private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_COMPRAS]; 
        ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_COMPRAS); 
        
        // Atributos del resultados user 
        private String resultTienda; 
        private String resultFormaCompra; 
        private String resultFormaPago; 
        
        public VerComprasDiarias(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 678, 609); // size 
            setTitle(Constantes.VISTA_VER_COMPRAS_DIARIAS); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Carga los componentes de la ventana 
            componentes(); 
            
            // LISTA DE TIENDAS 
            try { 
                if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_TIENDAS), cbTienda) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            } catch (HeadlessException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_TIENDAS+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            } catch (FileNotFoundException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
            } 
            
            // LISTA DE FORMAS COMPRA 
            try { 
                if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_COMPRA), cbFormaCompra) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_COMPRA+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            } catch (HeadlessException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_COMPRA+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            } catch (FileNotFoundException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
            } 
            
            // LISTA DE FORMAS PAGO 
            try { 
                if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_FORMA_PAGO), cbFormaPago) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_PAGO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            } catch (HeadlessException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_FORMA_PAGO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
            } catch (FileNotFoundException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
            } 
            
            modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                
        } // cierra constructor 
        
        private void componentes(){ 
                
            // Layout 
            contentPane = new JPanel(); 
            contentPane.setBorder(new EmptyBorder(5, 5, 5, 5)); 
            setContentPane(contentPane); 
            contentPane.setLayout(null); 
            
            // Panel para visualizar y hacer scroll 
            scrollPane = new JScrollPane(); 
            scrollPane.setViewportBorder(null); 
            scrollPane.setEnabled(false); 
            scrollPane.setBounds(24, 153, 617, 303); 
            contentPane.add(scrollPane); 
                    
            // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
            modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_COMPRAS); 
                
            // Se le pasa a JTable el modelo de tabla                 
            tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane); 
            UtilidadesTabla.anchoColumnasTablaCompras(tabla); 
            
            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 554, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 323, 14, Constantes.LIT_INFO_VER+Constantes.PUNTOS)); // Aviso de seleccion 

            contentPane.add(Utilidades.obtenerLabel(24, 99, 88, 14, Constantes.LABEL_TIENDA+Constantes.PUNTOS+Constantes.ESPACIO)); // label tienda 
            
            cbTienda = Utilidades.obtenerCombo(112, 96, 126, 20, cbTienda); // combo tiendas 
            cbTienda.addActionListener((ActionListener)this); 
            contentPane.add(cbTienda); 
            
            contentPane.add(Utilidades.obtenerLabel(257, 96, 88, 14, Constantes.LABEL_FORMA_PAGO+Constantes.PUNTOS+Constantes.ESPACIO)); // label forma de pago 
            
            cbFormaPago = Utilidades.obtenerCombo(349, 93, 126, 20, cbFormaPago); // combo forma de pago 
            cbFormaPago.addActionListener((ActionListener)this); 
            contentPane.add(cbFormaPago); 
            
            contentPane.add(Utilidades.obtenerLabel(257, 60, 95, 14, Constantes.LABEL_FORMA_COMPRA+Constantes.PUNTOS+Constantes.ESPACIO)); // label forma de compra 
            
            cbFormaCompra = Utilidades.obtenerCombo(350, 57, 126, 20, cbFormaCompra); // combo forma de compra 
            cbFormaCompra.addActionListener((ActionListener)this); 
            contentPane.add(cbFormaCompra); 
            
            contentPane.add(Utilidades.obtenerLabel(422, 481, 50, 14, Constantes.LABEL_TOTAL+Constantes.PUNTOS+Constantes.ESPACIO)); // label total 
            
            //contentPane.add(Utilidades.obtenerLabel(470, 481, 78, 14, String.valueOf(total))); // label importe 
            lblTotal = Utilidades.obtenerLabel(470, 481, 78, 14, String.valueOf(calcularTotal(total, null, null, null))); // cantidad total             
            contentPane.add(lblTotal); 
            
            btnConsultar = Utilidades.obtenerBoton(500, 54, 140, 59, Constantes.BTN_CONSULTAR, btnConsultar); // boton consultar 
            btnConsultar.addActionListener((ActionListener)this); 
            btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnConsultar); 
            
            btnDiario = Utilidades.obtenerBoton(365, 528, 95, 41, Constantes.BTN_DIARIO, btnDiario); // boton diario 
            btnDiario.addActionListener((ActionListener)this); 
            btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_DIARY+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnDiario); 
            
            btnMenu = Utilidades.obtenerBoton(220, 528, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnMenu); 
                
        } // Cierre componentes 
        
        private void limpiar(){ 
                
            cbTienda.setSelectedIndex(0); 
            cbFormaCompra.setSelectedIndex(0); 
            cbFormaPago.setSelectedIndex(0); 
                
        } // Cierre limpiar 

        @SuppressWarnings("unused") 
        @Override 
        public void actionPerformed(ActionEvent evento) { 
                
           if(evento.getSource()==cbTienda){ 
                    
                if(cbTienda.isValid()==true) 
                    resultTienda = cbTienda.getSelectedItem().toString(); 
            } 
            
            if(evento.getSource()==cbFormaCompra){ 
                    
                if(cbFormaCompra.isValid()==true) 
                    resultFormaCompra = cbFormaCompra.getSelectedItem().toString(); 
            } 
            
            if(evento.getSource()==cbFormaPago){ 
                    
                if(cbFormaPago.isValid()==true) 
                    resultFormaPago = cbFormaPago.getSelectedItem().toString(); 
            } 
            
            if(evento.getSource()==btnConsultar){ 
                    
                // Limpia la tabla 
                UtilidadesTabla.limpiarTabla(modelo); 
                
                boolean tiendaSeleccionada      = false; 
                boolean formaCompraSeleccionada = false; 
                boolean formaPagoSeleccionada   = false; 
                
                if( !UtilesValida.esNulo(cbTienda.getSelectedItem()) && !UtilesValida.esNulo(resultTienda)) // Si no ha seleccionado la tienda 
                        tiendaSeleccionada = true; 
                
                if( !UtilesValida.esNulo(cbFormaCompra.getSelectedItem()) && !UtilesValida.esNulo(resultFormaCompra)) // Si no ha seleccionado la forma de compra 
                        formaCompraSeleccionada = true; 

                if( !UtilesValida.esNulo(cbFormaPago.getSelectedItem()) && !UtilesValida.esNulo(resultFormaPago))// Si no ha seleccionado la forma de pago 
                        formaPagoSeleccionada = true; 
                
                
                int seleccion = 0; 
                
                if( tiendaSeleccionada && !formaCompraSeleccionada && !formaPagoSeleccionada){ 
                        seleccion = 1; 
                } 
                else if( tiendaSeleccionada && formaCompraSeleccionada && !formaPagoSeleccionada){ 
                        seleccion = 2; 
                } 
                else if( tiendaSeleccionada && formaCompraSeleccionada && formaPagoSeleccionada){ 
                        seleccion = 3; 
                } 
                else if( !tiendaSeleccionada && formaCompraSeleccionada && !formaPagoSeleccionada){ 
                        seleccion = 4; 
                } 
                else if( !tiendaSeleccionada && formaCompraSeleccionada && formaPagoSeleccionada){ 
                        seleccion = 5; 
                } 
                else if( !tiendaSeleccionada && !formaCompraSeleccionada && formaPagoSeleccionada){ 
                        seleccion = 6; 
                } 
                else if( !tiendaSeleccionada && !formaCompraSeleccionada && !formaPagoSeleccionada){ 
                        seleccion = 7; 
                } 
                            
                            
                switch( seleccion ){
                        
                    case 1:                                 
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO)); 
                        if(!UtilesValida.esNulo(registros)) {
                                
                            for(int i=0;i<registros.size();i++) { 
                                
                                datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                                
                                if(datosFile[2].equals(cbTienda.getSelectedItem()) || datosFile[2].equals(resultTienda)) {                                                                     
                                    modelo.addRow(datosFile); 
                                } 
                            } 
                            
                            lblTotal.setText(String.valueOf(calcularTotal(total, resultTienda, null, null))); 
                        } 
                        break; 
                            
                    case 2:                                 
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO)); 
                        if(!UtilesValida.esNulo(registros)) { 
                            
                            for(int i=0;i<registros.size();i++) { 
                                    
                                datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                                
                                if( (datosFile[2].equals(cbTienda.getSelectedItem()) || datosFile[2].equals(resultTienda)) 
                                     && (datosFile[5].equals(cbFormaCompra.getSelectedItem()) || datosFile[5].equals(resultFormaCompra))) { 
                                        
                                    total += Double.parseDouble(datosFile[4]); 
                                    modelo.addRow(datosFile); 
                                } 
                            } 
                            lblTotal.setText(String.valueOf(calcularTotal(total, resultTienda, resultFormaCompra, null))); 
                        } 
                        break; 
                            
                    case 3:                             
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO)); 
                        if(!UtilesValida.esNulo(registros)) { 
                                
                            for(int i=0;i<registros.size();i++) { 
                                
                                datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                                
                                if( (datosFile[2].equals(cbTienda.getSelectedItem()) || datosFile[2].equals(resultTienda)) 
                                     && (datosFile[5].equals(cbFormaCompra.getSelectedItem()) || datosFile[5].equals(resultFormaCompra)) 
                                     && (datosFile[6].equals(cbFormaPago.getSelectedItem()) || datosFile[6].equals(resultFormaPago))) { 
                                        
                                    total += Double.parseDouble(datosFile[4]); 
                                    modelo.addRow(datosFile); 
                                } 
                            } 
                            lblTotal.setText(String.valueOf(calcularTotal(total, resultTienda, resultFormaCompra, resultFormaPago))); 
                        } 
                        break; 
                            
                            
                    case 4:                                 
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO)); 
                        if(!UtilesValida.esNulo(registros)) { 
                                
                            for(int i=0;i<registros.size();i++) { 
                                    
                                datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                                
                                if( datosFile[5].equals(cbFormaCompra.getSelectedItem()) || datosFile[5].equals(resultFormaCompra)) { 
                                    
                                    total += Double.parseDouble(datosFile[4]); 
                                    modelo.addRow(datosFile); 
                                } 
                            } 
                            lblTotal.setText(String.valueOf(calcularTotal(total, null, resultFormaCompra, null))); 
                        } 
                        break; 
                            
                    case 5:                             
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO)); 
                        if(!UtilesValida.esNulo(registros)) {
                                
                            for(int i=0;i<registros.size();i++) { 
                                
                                datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                                
                                if( (datosFile[5].equals(cbFormaCompra.getSelectedItem()) || datosFile[5].equals(resultFormaCompra)) 
                                     && (datosFile[6].equals(cbFormaPago.getSelectedItem()) || datosFile[6].equals(resultFormaPago))) { 
                                        
                                    total += Double.parseDouble(datosFile[4]); 
                                    modelo.addRow(datosFile); 
                                } 
                            } 
                            
                            lblTotal.setText(String.valueOf(calcularTotal(total, null, resultFormaCompra, resultFormaPago))); 
                    } 
                        break; 
                            
                    case 6:                                 
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO));
                        if(!UtilesValida.esNulo(registros)) { 
                                
                            for(int i=0;i<registros.size();i++) { 
                                    
                                datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                                
                                if( datosFile[6].equals(cbFormaPago.getSelectedItem()) || datosFile[6].equals(resultFormaPago) ) { 
                                        
                                    total += Double.parseDouble(datosFile[4]); 
                                    modelo.addRow(datosFile); 
                                } 
                            } 
                            lblTotal.setText(String.valueOf(calcularTotal(total, null, null, resultFormaPago))); 
                        } 
                        break; 
                            
                    case 7:                            
                        limpiar(); 
                        UtilidadesTabla.limpiarTabla(modelo); 
                        lblTotal.setText(String.valueOf(Constantes.DEFAULT_DOBLE_CERO)); 
                        modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                        lblTotal.setText(String.valueOf(calcularTotal(total, null, null, null))); 
                        break; 
                                
                } 
                    
            } 
            
            if(evento.getSource()==btnDiario){ 
                    
                    UtilidadesTabla.limpiarTabla(modelo); 
                    lblTotal.setText(String.valueOf(calcularTotal(total, null, null, null))); 
                    modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                limpiar(); 
            } 
            
            if(evento.getSource()==btnMenu){ 
                    
                  ControladorPrincipal controladorPrincipal = new ControladorPrincipal(); 
                  setVisible(false); 
            } 
                
        } // cierra actionPerformed 
        
        private double calcularTotal(double total, String tienda, String formaCompra, String formaPago) { 
                
                String[] dato = null; 
                
            if(!UtilesValida.esNula(registros)) { 
                    
                    total = 0.0; 
                    
                    for(int i=0; i<registros.size(); i++) { 
                            
                            dato = registros.get(i).split(Constantes.PUNTO_COMA); 
                            
                            if( !UtilesValida.esNulo(tienda) && UtilesValida.esNulo(formaCompra) && UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(dato[2].equals(tienda)){ 
                                        if(UtilesValida.esDoble(dato[4])) 
                                                total = total + Double.parseDouble(dato[4]); 
                                } 
                                    
                            } 
                            else if( !UtilesValida.esNulo(tienda) && !UtilesValida.esNulo(formaCompra) && UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(dato[2].equals(tienda) && dato[5].equals(formaCompra)){ 
                                        if(UtilesValida.esDoble(dato[4])) 
                                                total = total + Double.parseDouble(dato[4]); 
                                } 
                                    
                            } 
                            else if( !UtilesValida.esNulo(tienda) && !UtilesValida.esNulo(formaCompra) && !UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(dato[2].equals(tienda) && dato[5].equals(formaCompra) && dato[6].equals(formaPago)){ 
                                        if(UtilesValida.esDoble(dato[4])) 
                                                total = total + Double.parseDouble(dato[4]); 
                                } 
                                    
                            } 
                            else if( UtilesValida.esNulo(tienda) && !UtilesValida.esNulo(formaCompra) && UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(dato[5].equals(formaCompra)){ 
                                        if(UtilesValida.esDoble(dato[4])) 
                                                total = total + Double.parseDouble(dato[4]); 
                                } 
                                    
                            } 
                            else if( UtilesValida.esNulo(tienda) && !UtilesValida.esNulo(formaCompra) && !UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(dato[5].equals(formaCompra) && dato[6].equals(formaPago)){ 
                                        if(UtilesValida.esDoble(dato[4])) 
                                                total = total + Double.parseDouble(dato[4]); 
                                } 
                                    
                            } 
                            else if( UtilesValida.esNulo(tienda) && UtilesValida.esNulo(formaCompra) && !UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(dato[6].equals(formaPago)){ 
                                        if(UtilesValida.esDoble(dato[4])) 
                                                total = total + Double.parseDouble(dato[4]); 
                                } 
                                    
                            } 
                            else if( UtilesValida.esNulo(tienda) && UtilesValida.esNulo(formaCompra) && UtilesValida.esNulo(formaPago)) { 
                                    
                                    if(UtilesValida.esDoble(dato[4])) 
                                        total = total + Double.parseDouble(dato[4]); 
                            } 
                } 
            } 
                
                return total; 
        } // calcular total 

} // cierre de clase
