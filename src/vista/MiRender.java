package vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import utilidades.Constantes;

/**
 * Es una clase para cambiar el color del texto a una celda dependiendo de su valor.
 * 
*/
public class MiRender extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = -8624753721794395368L;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)  {
		
		JLabel cell=(JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		// Si el objeto que nos pasan es un Float, lo ponemos en el JLabel.
		if (value instanceof Float){
			if(column==4){
				
				float valor=(Float)value;
				
				if(valor>0){ // Positivo
					Color customColor = new Color(11,97,56);
					cell.setForeground(customColor);
					cell.setFont(new Font(Constantes.FUENTE_TAHOMA, Font.BOLD, 11));
				}
				else{
					cell.setForeground(Color.RED);
				}
			}
		}
		// Si el objeto que nos pasan es un Double
		if (value instanceof Double){
			cell.setForeground(Color.BLACK);
		}
		// Si el objeto que nos pasan es un String
		if (value instanceof String){
			cell.setForeground(Color.BLACK);
		}
		return cell;
	}

} // Cierre clase