package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal; 
import utilidades.Constantes; 
import utilidades.FicherosListas; 
import utilidades.ListaConstantesValidacion; 
import utilidades.Utilidades; 
import utilidades.UtilidadesTabla; 

public class VerFormasCompra extends JFrame implements ActionListener { 
        
    private static final long serialVersionUID = -8624753721794395368L; 
    
    // Atributos de la clase 
    private JPanel contentPane; 
    private JButton btnMenu; 
            
    // Atributos relacionados con la tabla 
    private JScrollPane scrollPane; 
    private DefaultTableModel modelo; 
    private String [][] arrayTabla; //array bidimensional 
    private JTable tabla; 
    private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_FORMAS_COMPRA]; 
    private ArrayList<String> registros = FicherosListas.leerRegistros(Constantes.FICHERO_FORMA_COMPRA); 
    
    public VerFormasCompra(){ 
            
        // Caracteristicas de la ventana 
        setResizable(false); // no reestablece el size 
        setBounds(100, 100, 446, 480); // size 
        setTitle(Constantes.VISTA_VER_FORMAS_COMPRA); // titulo 
        setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
        
        // Carga los componentes de la ventana 
        componentes(); 
        
        modelo = UtilidadesTabla.mostrarTodasListasTabla(registros, datosFile, modelo); 
            
    } // Cierre constructor 
    
    public void componentes(){ 
            
        // Layout 
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
        setContentPane(contentPane); 
        
        // Panel para visualizar y hacer scroll 
        scrollPane = Utilidades.obtenerScroll(10, 65, 420, 303, scrollPane, false); 
        contentPane.add(scrollPane); 
        
        // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
        modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_FORMAS_COMPRA); 
        
        // Se le pasa a JTable el modelo de tabla 
        tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane); 
        
        // Labels + textFields + radio buttons + comboBox + buttons 
        contentPane.add(Utilidades.obtenerVersionLabel(0, 410, 95, 14)); // Version label 
        
        contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 35, 300, 14, Constantes.LIT_INFO_VER+Constantes.PUNTOS)); // Aviso de seleccion a visualizar 
        
        btnMenu = Utilidades.obtenerBoton(169, 380, 106, 33, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu); 
            
    } // Cierre componentes 

    @SuppressWarnings("unused") 
    public void actionPerformed(ActionEvent evento) { 
        
        // Se guardan los datos y vuelve a la principal 
        if(evento.getSource()==btnMenu){ 
                
            ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
            setVisible(false); 
        }                 
    } // Cierre actionPerformed 
        
} // Cierre clase 
