package vista;

import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

import utilidades.Constantes;
import utilidades.FicherosListas;
import utilidades.FicherosRegistros;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class Principal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	
		// items del menu
		private JMenuItem itemPresu;
		private JMenuItem itemIngreso;
		private JMenuItem itemTienda;
		private JMenuItem itemFormaCompra;
		private JMenuItem itemTiposGastosFijos;
		private JMenuItem itemFormaPago;
		private JMenuItem itemEditarPresu;
		private JMenuItem itemEditarIngreso;
		private JMenuItem itemEditarGastosFijos;
		private JMenuItem itemEditarTienda;
		private JMenuItem itemEditarFormaCompra;
		private JMenuItem itemEditarFormaPago;
		private JMenuItem itemEditarTipoGastoFijo;
		private JMenuItem itemProducto;
		private JMenuItem itemPresupuestos;
		private JMenuItem itemIngresos;
		private JMenuItem itemVerComprasDiarias;
		private JMenuItem itemConcretos;
		private JMenuItem itemVerGastosFijos;
		private JMenuItem itemVerTiendas;
		private JMenuItem itemVerFormasCompra;
		private JMenuItem itemVerFormasPago;
		private JMenuItem itemVerTiposGastosFijos;
		private JMenuItem itemSemanal;
		private JMenuItem itemComprasLog;
		private JMenuItem itemFijosLog;
		private JMenuItem itemGraficoComida;
		private JMenuItem itemGraficoOnline;
		private JMenuItem itemGraficoGastoFijo;
		private JMenuItem itemCrearMail;
		private JMenuItem itemRecibirMail;
		
		// accesos directos - botones
		private JButton btnCompra;
		private JButton btnFijo;
		private JButton btnEditarCompras;
		//private JButton btnDiario;		
		private JButton btnFechas;
		private JButton btnFijos;
		private JButton btnAhorroMensual;
		
		// Nombre de los ficheros
		public static final String FICHERO_COMPRAS		= Constantes.FICHERO_COMPRAS;
		public static final String FICHERO_GASTOS_FIJOS	= Constantes.FICHERO_GASTOS_FIJOS;
		public static final String FICHERO_PRESUPUESTOS	= Constantes.FICHERO_PRESUPUESTOS;
		public static final String FICHERO_INGRESOS		= Constantes.FICHERO_INGRESOS;
		
		public static final String FICHERO_LISTA_TIENDAS		= Constantes.FICHERO_TIENDAS;
		public static final String FICHERO_LISTA_FORMAS_COMPRA	= Constantes.FICHERO_FORMA_COMPRA;
		public static final String FICHERO_LISTA_FORMAS_PAGO	= Constantes.FICHERO_FORMA_PAGO;
		public static final String FICHERO_LISTA_TIPO_GASTO_FIJO= Constantes.FICHERO_GASTOS_FIJOS_LISTA;
		
		// Comprueba si existen los ficheros
        private static final boolean EXISTE_FICHERO_COMPRAS             = FicherosRegistros.existeFichero(FICHERO_COMPRAS); 
        private static final boolean EXISTE_FICHERO_GASTOS_FIJOS        = FicherosRegistros.existeFichero(FICHERO_GASTOS_FIJOS); 
        private static final boolean EXISTE_FICHERO_PRESUPUESTOS        = FicherosRegistros.existeFichero(FICHERO_PRESUPUESTOS); 
        private static final boolean EXISTE_FICHERO_INGRESOS            = FicherosRegistros.existeFichero(FICHERO_INGRESOS); 
        
        private static final boolean EXISTE_FICHERO_LISTA_TIENDAS        = FicherosListas.existeFichero(FICHERO_LISTA_TIENDAS); 
        private static final boolean EXISTE_FICHERO_LISTA_FORMAS_COMPRA  = FicherosListas.existeFichero(FICHERO_LISTA_FORMAS_COMPRA); 
        private static final boolean EXISTE_FICHERO_LISTA_FORMAS_PAGO    = FicherosListas.existeFichero(FICHERO_LISTA_FORMAS_PAGO); 
        private static final boolean EXISTE_FICHERO_LISTA_TIPO_GASTO_FIJO= FicherosListas.existeFichero(FICHERO_LISTA_TIPO_GASTO_FIJO); 

		
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Principal(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 574, 326); // size
		setTitle(Constantes.VISTA_CONTABILIDAD); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_COINS+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // La equis de la ventana detiene la aplicacion
		
		// Carga los componentes de la ventana
		componentes();
		
	} // constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Principal.
     *  
     */
	public void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 272, 79, 14));
		
		//contenedor de menu bar
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 581, 21);
		contentPane.add(menuBar);
		
		/*************************************************************/
		
		// menu + items del menu
		JMenu mnAnadir = new JMenu(Constantes.ANADIR); 
		menuBar.add(mnAnadir);
		
		itemPresu = new JMenuItem(Constantes.MENU_PRESUPUESTO);
		itemPresu.addActionListener((ActionListener)this);
		mnAnadir.add(itemPresu);
		
		itemIngreso = new JMenuItem(Constantes.MENU_INGRESO);
		itemIngreso.addActionListener((ActionListener)this);
		mnAnadir.add(itemIngreso);
		
		itemTienda = new JMenuItem(Constantes.MENU_TIENDA);
		itemTienda.addActionListener((ActionListener)this);
		mnAnadir.add(itemTienda);
		
		itemFormaCompra = new JMenuItem(Constantes.MENU_FORMA_COMPRA);
		itemFormaCompra.addActionListener((ActionListener)this);
		mnAnadir.add(itemFormaCompra);
		
		itemFormaPago = new JMenuItem(Constantes.MENU_FORMA_PAGO);
		itemFormaPago.addActionListener((ActionListener)this);
		mnAnadir.add(itemFormaPago);
		
		itemTiposGastosFijos = new JMenuItem(Constantes.MENU_TIPOS_GASTOS_FIJOS);
		itemTiposGastosFijos.addActionListener((ActionListener)this);
		mnAnadir.add(itemTiposGastosFijos);
		
		/*************************************************************/
		
		JMenu mnEditar = new JMenu(Constantes.EDITAR);
		menuBar.add(mnEditar);
		
		itemEditarPresu = new JMenuItem(Constantes.MENU_PRESUPUESTOS);
		itemEditarPresu.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarPresu);
		
		itemEditarIngreso = new JMenuItem(Constantes.MENU_INGRESOS);
		itemEditarIngreso.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarIngreso);
		
		itemEditarGastosFijos = new JMenuItem(Constantes.MENU_GASTOS_FIJOS);
		itemEditarGastosFijos.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarGastosFijos);
		
		itemEditarTienda = new JMenuItem(Constantes.MENU_TIENDA);
		itemEditarTienda.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarTienda);
		
		itemEditarFormaCompra = new JMenuItem(Constantes.MENU_FORMA_COMPRA);
		itemEditarFormaCompra.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarFormaCompra);
		
		itemEditarFormaPago = new JMenuItem(Constantes.MENU_FORMA_PAGO);
		itemEditarFormaPago.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarFormaPago);
		
		itemEditarTipoGastoFijo = new JMenuItem(Constantes.MENU_GASTO_FIJOS);
		itemEditarTipoGastoFijo.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarTipoGastoFijo);
		
		/*************************************************************/
		
		JMenu mnBuscar = new JMenu(Constantes.BUSCAR);
		menuBar.add(mnBuscar);
		
		itemProducto = new JMenuItem(Constantes.MENU_PRODUCTO);
		itemProducto.addActionListener((ActionListener)this);
		mnBuscar.add(itemProducto);
		
		/*************************************************************/
		
		JMenu mnVer = new JMenu(Constantes.VER);
		menuBar.add(mnVer);
		
		itemPresupuestos = new JMenuItem(Constantes.MENU_PRESUPUESTOS);
		itemPresupuestos.addActionListener((ActionListener)this);
		mnVer.add(itemPresupuestos);
		
		itemIngresos = new JMenuItem(Constantes.MENU_INGRESOS);
		itemIngresos.addActionListener((ActionListener)this);
		mnVer.add(itemIngresos);
		
		itemVerComprasDiarias = new JMenuItem(Constantes.MENU_COMPRAS_DIARIAS);
		itemVerComprasDiarias.addActionListener((ActionListener)this);
		mnVer.add(itemVerComprasDiarias);
		
		itemConcretos = new JMenuItem(Constantes.MENU_COMPRAS_CONCRETAS);
		itemConcretos.addActionListener((ActionListener)this);
		mnVer.add(itemConcretos);
		
		itemVerGastosFijos = new JMenuItem(Constantes.MENU_GASTOS_FIJOS);
		itemVerGastosFijos.addActionListener((ActionListener)this);
		mnVer.add(itemVerGastosFijos);
		
		itemVerTiendas = new JMenuItem(Constantes.MENU_TIENDAS);
		itemVerTiendas.addActionListener((ActionListener)this);
		mnVer.add(itemVerTiendas);
		
		itemVerFormasCompra = new JMenuItem(Constantes.MENU_FORMAS_COMPRA);
		itemVerFormasCompra.addActionListener((ActionListener)this);
		mnVer.add(itemVerFormasCompra);
		
		itemVerFormasPago = new JMenuItem(Constantes.MENU_FORMAS_PAGO);
		itemVerFormasPago.addActionListener((ActionListener)this);
		mnVer.add(itemVerFormasPago);
		
		itemVerTiposGastosFijos = new JMenuItem(Constantes.MENU_TIPOS_GASTOS_FIJOS);
		itemVerTiposGastosFijos.addActionListener((ActionListener)this);
		mnVer.add(itemVerTiposGastosFijos);
		
		/*************************************************************/
		
		JMenu mnAhorro = new JMenu(Constantes.MENU_AHORRO);
		menuBar.add(mnAhorro);
		
		itemSemanal = new JMenuItem(Constantes.MENU_SEMANAL);
		itemSemanal.addActionListener((ActionListener)this);
		mnAhorro.add(itemSemanal);
		
		/*************************************************************/
		
		JMenu mnFichero = new JMenu(Constantes.MENU_FICHERO);
		menuBar.add(mnFichero);
		
		itemComprasLog = new JMenuItem(Constantes.MENU_COMPRAS_LOG);
		itemComprasLog.addActionListener((ActionListener)this);
		mnFichero.add(itemComprasLog);
		
		itemFijosLog = new JMenuItem(Constantes.MENU_FIJOS_LOG);
		itemFijosLog.addActionListener((ActionListener)this);
		mnFichero.add(itemFijosLog);
				
		/*************************************************************/
		
		JMenu mnGrafico = new JMenu(Constantes.MENU_GRAFICO);
		menuBar.add(mnGrafico);
		
		itemGraficoComida = new JMenu(Constantes.MENU_COMIDA);
		itemGraficoComida.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoComida);
		
		itemGraficoOnline = new JMenu(Constantes.MENU_TARJETA_NET);
		itemGraficoOnline.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoOnline);		
		
		itemGraficoGastoFijo = new JMenuItem(Constantes.MENU_GASTO_FIJOS);
		itemGraficoGastoFijo.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoGastoFijo);
		
		/*************************************************************/
				
		JMenu mnEmail = new JMenu(Constantes.MENU_EMAIL);
		menuBar.add(mnEmail);
		
		itemCrearMail = new JMenu(Constantes.MENU_ENVIAR);
		itemCrearMail.addActionListener((ActionListener)this);
		mnEmail.add(itemCrearMail);
		
		itemRecibirMail = new JMenuItem(Constantes.MENU_RECIBIR);
		itemRecibirMail.addActionListener((ActionListener)this);
		mnEmail.add(itemRecibirMail);
		
		/*************************************************************/
		
		// Accesos directos - botones
		btnCompra = Utilidades.obtenerBoton(10, 75, 152, 76, Constantes.ACCESO_COMPRA, btnCompra);
		btnCompra.addActionListener((ActionListener)this);
		btnCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnCompra);	
		
		btnFijo = Utilidades.obtenerBoton(202, 75, 152, 76, Constantes.ACCESO_FIJO, btnFijo);
		btnFijo.addActionListener((ActionListener)this);
		btnFijo.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnFijo);
		
		btnEditarCompras = Utilidades.obtenerBoton(392, 75, 152, 76, Constantes.ACCESO_COMPRAS, btnEditarCompras);
		btnEditarCompras.addActionListener((ActionListener)this);
		btnEditarCompras.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)));
		contentPane.add(btnEditarCompras);
		
		/*btnDiario = Utilidades.obtenerBoton(10, 174, 152, 76, Constantes.ACCESO_DIARIO, btnDiario);
		btnDiario.addActionListener((ActionListener)this);
		btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)));
		contentPane.add(btnDiario);*/
		
		btnFechas = Utilidades.obtenerBoton(10, 174, 152, 76, Constantes.ACCESO_FECHAS, btnFechas);
		btnFechas.addActionListener((ActionListener)this);
		btnFechas.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)));
		contentPane.add(btnFechas);
		
		btnFijos = Utilidades.obtenerBoton(202, 174, 152, 76, Constantes.ACCESO_FIJOS, btnFijos);
		btnFijos.addActionListener((ActionListener)this);
		btnFijos.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)));
		contentPane.add(btnFijos);
		
		btnAhorroMensual = Utilidades.obtenerBoton(392, 174, 152, 76, Constantes.MENU_AHORRO, btnAhorroMensual);
		btnAhorroMensual.addActionListener((ActionListener)this);
		btnAhorroMensual.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_COINS+Constantes.EXTENSION_PNG)));
		contentPane.add(btnAhorroMensual);
		
	} // Cierre componentes
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	public void actionPerformed(ActionEvent evento) {

        // Listas de registros 
        ArrayList<String> registrosCompras      = new ArrayList<>(); 
        ArrayList<String> registrosGastosFijos  = new ArrayList<>(); 
        ArrayList<String> registrosPresupuestos = new ArrayList<>(); 
        ArrayList<String> registrosIngresos     = new ArrayList<>(); 
        
        if(EXISTE_FICHERO_COMPRAS) 
                registrosCompras = FicherosRegistros.leerRegistros(FICHERO_COMPRAS); 
        
        if(EXISTE_FICHERO_GASTOS_FIJOS) 
                registrosGastosFijos = FicherosRegistros.leerRegistros(FICHERO_GASTOS_FIJOS); 
                
        if(EXISTE_FICHERO_PRESUPUESTOS) 
                registrosPresupuestos = FicherosRegistros.leerRegistros(FICHERO_PRESUPUESTOS); 
        
        if(EXISTE_FICHERO_INGRESOS) 
                registrosIngresos = FicherosRegistros.leerRegistros(FICHERO_INGRESOS); 
                
        // Listas de listas 
        ArrayList<String> listaTiendas          = new ArrayList<>(); 
        ArrayList<String> listaFormasCompra     = new ArrayList<>(); 
        ArrayList<String> listaFormasPago       = new ArrayList<>(); 
        ArrayList<String> listaTipoGastoFijo 	= new ArrayList<>(); 
        
        if(EXISTE_FICHERO_LISTA_TIENDAS) 
                listaTiendas = FicherosListas.leerRegistros(FICHERO_LISTA_TIENDAS); 
                
        if(EXISTE_FICHERO_LISTA_FORMAS_COMPRA) 
                listaFormasCompra = FicherosListas.leerRegistros(FICHERO_LISTA_FORMAS_COMPRA); 
        
        if(EXISTE_FICHERO_LISTA_FORMAS_PAGO) 
                listaFormasPago = FicherosListas.leerRegistros(FICHERO_LISTA_FORMAS_PAGO); 
        
        if(EXISTE_FICHERO_LISTA_TIPO_GASTO_FIJO) 
                listaTipoGastoFijo = FicherosListas.leerRegistros(FICHERO_LISTA_TIPO_GASTO_FIJO); 
        
        // Comprobar si las listas de registro estan vacias 
        boolean hayCompras      = registrosCompras.isEmpty(); 
        boolean hayGastosFijos  = registrosGastosFijos.isEmpty(); 
        boolean hayPresupuestos = registrosPresupuestos.isEmpty(); 
        boolean hayIngresos     = registrosIngresos.isEmpty(); 
        
        // Comprobar si las listas estan vacias 
        boolean hayTiendas      	= listaTiendas.isEmpty(); 
        boolean hayFormasCompra 	= listaFormasCompra.isEmpty(); 
        boolean hayFormasPago   	= listaFormasPago.isEmpty(); 
        boolean hayTipoGastosFijos 	= listaTipoGastoFijo.isEmpty(); 


		// Si hace clic en menu Agregar Presupuesto
		if(evento.getSource()==itemPresu ){ 

			AnadirPresupuesto ventanaPresupuesto=new AnadirPresupuesto();
			ventanaPresupuesto.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaPresupuesto.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Agregar Ingreso
		if(evento.getSource()==itemIngreso){

			AnadirIngreso ventanaAnadirIngreso=new AnadirIngreso();
			ventanaAnadirIngreso.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirIngreso.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Agregar Tienda
		if(evento.getSource()==itemTienda){

			AnadirTienda ventanaAnadirTienda=new AnadirTienda();
			ventanaAnadirTienda.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirTienda.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Agregar Forma de Compra
		if(evento.getSource()==itemFormaCompra){

			AnadirFormaCompra ventanaAnadirFormaCompra=new AnadirFormaCompra();
			ventanaAnadirFormaCompra.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirFormaCompra.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Agregar Forma de Pago
		if(evento.getSource()==itemFormaPago){

			AnadirFormaPago ventanaAnadirFormaPago=new AnadirFormaPago();
			ventanaAnadirFormaPago.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirFormaPago.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Agregar Tipos Gastos Fijos
		if(evento.getSource()==itemTiposGastosFijos){

			AnadirTiposGastosFijos ventanaAnadirTiposGastosFijos=new AnadirTiposGastosFijos();
			ventanaAnadirTiposGastosFijos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirTiposGastosFijos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton Agregar Compra
		if(evento.getSource()==btnCompra){ 

			AnadirCompra ventanaCompra=new AnadirCompra();
			ventanaCompra.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCompra.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton Agregar Fijo
		if(evento.getSource()==btnFijo){ 

			AnadirGastoFijo ventanaGastoFijo=new AnadirGastoFijo();
			ventanaGastoFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGastoFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}	
		// Si hace clic en boton Editar Compras
		if(evento.getSource()==btnEditarCompras){

			if( !hayCompras ){
				TablaCompras ventanaTablaCompras=new TablaCompras();
				ventanaTablaCompras.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaCompras.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_COMPRA, Constantes.VISTA_EDITAR_COMPRA, JOptionPane.INFORMATION_MESSAGE); 
		}
		// Si hace clic en  menu Editar presupuesto
		if(evento.getSource()==itemEditarPresu){

			if( !hayPresupuestos ){

				TablaPresupuestos ventanaTablaPresupuestos=new TablaPresupuestos();
				ventanaTablaPresupuestos.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaPresupuestos.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_PRESUPUESTO, Constantes.VISTA_EDITAR_PRESUPUESTO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en  menu Editar Ingreso
		if(evento.getSource()==itemEditarIngreso){

			if( !hayIngresos ){

				TablaIngresos ventanaTablaIngresos=new TablaIngresos();
				ventanaTablaIngresos.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaIngresos.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_INGRESO, Constantes.VISTA_EDITAR_INGRESO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en  menu Editar Gasto Fijo
		if(evento.getSource()==itemEditarGastosFijos){

			if( !hayGastosFijos ){

				TablaGastosFijos ventanaTablaGastosFijos=new TablaGastosFijos();
				ventanaTablaGastosFijos.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaGastosFijos.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_GASTO_FIJO, Constantes.VISTA_EDITAR_GASTO_FIJO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en  menu Editar Tienda
		if(evento.getSource()==itemEditarTienda){

			if( !hayTiendas ){

				TablaTiendas ventanaTablaTiendas=new TablaTiendas();
				ventanaTablaTiendas.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaTiendas.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_TIENDA, Constantes.VISTA_EDITAR_TIENDA, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en  menu Editar Formas de compra
		if(evento.getSource()==itemEditarFormaCompra){

			if( !hayFormasCompra ){

				TablaFormasCompra ventanaTablaFormasCompra=new TablaFormasCompra();
				ventanaTablaFormasCompra.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaFormasCompra.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_FORMA_COMPRA, Constantes.VISTA_EDITAR_FORMA_COMPRA, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en  menu Editar Formas de Pago
		if(evento.getSource()==itemEditarFormaPago){

			if( !hayFormasPago ){
				TablaFormasPago ventanaTablaFormasPago=new TablaFormasPago();
				ventanaTablaFormasPago.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaFormasPago.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);		
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_FORMA_PAGO, Constantes.VISTA_EDITAR_FORMA_PAGO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en  menu Editar Tipo Gasto Fijo
		if(evento.getSource()==itemEditarTipoGastoFijo){

			if( !hayTipoGastosFijos ){

				TablaTiposGastosFijos ventanaTablaTipoGastoFijo=new TablaTiposGastosFijos();
				ventanaTablaTipoGastoFijo.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaTablaTipoGastoFijo.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_TIPO_GASTO_FIJO, Constantes.VISTA_EDITAR_TIPO_GASTO_FIJO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en menu Buscar producto
		if(evento.getSource()==itemProducto){

			BuscarProducto ventanaBuscarProducto=new BuscarProducto();
			ventanaBuscarProducto.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaBuscarProducto.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ver presupuestos
		if(evento.getSource()==itemPresupuestos ){

			if( !hayPresupuestos ){

				VerPresupuestos ventanaPresupuestos=new VerPresupuestos();
				ventanaPresupuestos.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaPresupuestos.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_PRESUPUESTO, Constantes.VISTA_EDITAR_PRESUPUESTO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en menu Ver ingresos
		if(evento.getSource()==itemIngresos){

			if( !hayIngresos ){

				VerIngresos ventanaIngresos=new VerIngresos();
				ventanaIngresos.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaIngresos.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_INGRESO, Constantes.VISTA_EDITAR_INGRESO, JOptionPane.INFORMATION_MESSAGE);

		} 
		// Si hace clic en boton Gastos Diarios
		if(
		//evento.getSource()==btnDiario || 
		 evento.getSource()==itemVerComprasDiarias){ 

			VerComprasDiarias ventanaComprasDiarias = new VerComprasDiarias();
			ventanaComprasDiarias.setLocationRelativeTo(null);
			ventanaComprasDiarias.setVisible(true);
	  		setVisible(false);
		}
		// Se puede ver los gastos concretos entre fechas o no
		/*if(evento.getSource()==btnFechas ||evento.getSource()==itemConcretos){ 

			VerComprasConcretas ventanaConcretos=new VerComprasConcretas();
			ventanaConcretos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaConcretos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}*/
		// Si hace clic en boton Gastos Fijos
		if(evento.getSource()==btnFijos || evento.getSource()==itemVerGastosFijos){

			if( !hayGastosFijos ){

				VerGastosFijos ventanaVerGastosFijo=new VerGastosFijos();
				ventanaVerGastosFijo.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaVerGastosFijo.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_GASTO_FIJO, Constantes.VISTA_EDITAR_GASTO_FIJO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en menu Ver Tiendas
		if(evento.getSource()==itemVerTiendas){

			if( !hayTiendas ){

				VerTiendas ventanaVerTiendas=new VerTiendas();
				ventanaVerTiendas.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaVerTiendas.setVisible(true);
		  		// Deaparece esta ventana
	  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_TIENDA, Constantes.VISTA_EDITAR_TIENDA, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en menu Ver Formas de Compra
		if(evento.getSource()==itemVerFormasCompra){

			if( !hayFormasCompra ){

				VerFormasCompra ventanaVerFormasCompra=new VerFormasCompra();
				ventanaVerFormasCompra.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaVerFormasCompra.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_FORMA_COMPRA, Constantes.VISTA_EDITAR_FORMA_COMPRA, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en menu Ver Formas de Pago
		if(evento.getSource()==itemVerFormasPago){

			if( !hayFormasPago ){

				VerFormasPago ventanaVerFormasPago=new VerFormasPago();
				ventanaVerFormasPago.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaVerFormasPago.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_FORMA_PAGO, Constantes.VISTA_EDITAR_FORMA_PAGO, JOptionPane.INFORMATION_MESSAGE);

		}
		// Si hace clic en menu Ver Tipos Gastos Fijos
		if(evento.getSource()==itemVerTiposGastosFijos){

			if( !hayTipoGastosFijos ){

				VerTiposGastosFijos ventanaVerTiposGastosFijos=new VerTiposGastosFijos();
				ventanaVerTiposGastosFijos.setLocationRelativeTo(null);
				// Hace visible la ventana
				ventanaVerTiposGastosFijos.setVisible(true);
		  		// Deaparece esta ventana
		  		setVisible(false);
		  	}
		  	else
		  		JOptionPane.showMessageDialog(null, Constantes.AVISO_AGREGAR_TIPO_GASTO_FIJO, Constantes.VISTA_EDITAR_TIPO_GASTO_FIJO, JOptionPane.INFORMATION_MESSAGE);

		}
		/*
		// Si hace clic en menu Ahorro semanal 
		if(evento.getSource()==itemSemanal){ 

			VerAhorroSemanal ventanaSemanal=new VerAhorroSemanal();
			ventanaSemanal.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaSemanal.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}*/
		// Si hace clic en el boton Ahorro mensual
		if(evento.getSource()==btnAhorroMensual){ 

			VerAhorro ventanaVerAhorroMensual = new VerAhorro();
			ventanaVerAhorroMensual.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaVerAhorroMensual.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}		
		// Si hace clic en menu Compras Log
		if(evento.getSource()==itemComprasLog){
			
			try{
				
				String texto = utilidades.FicherosLog.leerFicheroLog(Constantes.FICHERO_COMPRAS);
				
				if(!UtilesValida.esNulo(texto)) {
					
		
					LogCompras ventanaLogCompras=new LogCompras(texto);
		
					ventanaLogCompras.setLocationRelativeTo(null);
					// Hace visible la ventana
					ventanaLogCompras.setVisible(true);
			  		// Deaparece esta ventana
			  		setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_VACIO);
				}
			}
			catch (IOException eoi) {
				JOptionPane.showMessageDialog(null, eoi.getMessage());
			}			

		}
		/*
		// Si hace clic en menu Gastos Fijos Log
		if(evento.getSource()==itemFijosLog){

			LogGastosFijos ventanaLogFijos=new LogGastosFijos();
			ventanaLogFijos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaLogFijos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico de comida
		if(evento.getSource()==itemGraficoComida){

			GraficoComida ventanaGraficoComida=new GraficoComida();
			ventanaGraficoComida.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoComida.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico de tarjetas
		if(evento.getSource()==itemGraficoOnline){

			GraficoOnline ventanaGraficoOnline=new GraficoOnline();
			ventanaGraficoOnline.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoOnline.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico de gastos fijos
		if(evento.getSource()==itemGraficoGastoFijo){

			GraficoGastosFijos ventanaGraficoGastoFijo=new GraficoGastosFijos();
			ventanaGraficoGastoFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoGastoFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Enviar email
		if(evento.getSource()==itemCrearMail){

			EnviarMail ventanaCrearMail=new EnviarMail();
			ventanaCrearMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCrearMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Recibir email
		if(evento.getSource()==itemRecibirMail){

			RecibirMail ventanaRecibirMail=new RecibirMail();
			ventanaRecibirMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaRecibirMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}*/
		
	} // Cierre actionPerfomed
} // Cierre de clase