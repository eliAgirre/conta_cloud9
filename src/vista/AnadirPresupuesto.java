package vista;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.ValidacionesComun; 

 
public class AnadirPresupuesto extends JFrame implements ActionListener {
	
		private static final long serialVersionUID = -8624753721794395368L;
        
        // Atributos de la clase 
        private JPanel contentPane; 
        
        private JTextField dateChooser; 
        private JTextField txtPresu;
        private final ButtonGroup grupoBotonTipo = new ButtonGroup(); 
        private JRadioButton rbSemanal; 
        private JRadioButton rbMensual; 
        private JButton btnMenu;
        private JButton btnGuardar; 
        
    	private int idPresu;
        private String resultFecha;
        private String resultTipo; 
        private Double resultPresu;

        public AnadirPresupuesto(){ 
                
            // Caracteristicas de la ventana            
    		setResizable(false); // no reestablece el size
    		setBounds(100, 100, 300, 381); 
    		setTitle(Constantes.VISTA_ANADIR_PRESUPUESTO); // titulo
    		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)).getImage()); //logo
    		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
            
            // Carga los componentes de la ventan
            componentes(); 
                
        } // Cierre constructor 
        
        private void componentes(){ 
                
            // Layout 
            contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
            setContentPane(contentPane);
            
            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(-1, 327, 95, 14));  // Version 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_INFORMACION)); // Aviso
            
            contentPane.add(Utilidades.obtenerLabel(31, 81, 63, 14, Constantes.LABEL_FECHA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label Fecha 
            
    		dateChooser = Utilidades.obtenerTextField(130, 81, 126, 20, 10, dateChooser, null, true); // fecha compra
    		dateChooser.addPropertyChangeListener(Constantes.INPUT_FECHA, (PropertyChangeListener) this);
    		contentPane.add(dateChooser);

            contentPane.add(Utilidades.obtenerLabel(31, 133, 89, 14, Constantes.LABEL_PRESUPUESTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label Presu 
            
            txtPresu = Utilidades.obtenerTextField(130, 133, 126, 20, 10, txtPresu, null, true);  // txtPresu field
            txtPresu.addActionListener((ActionListener)this);
    		contentPane.add(txtPresu);
                    
            contentPane.add(Utilidades.obtenerLabel(31, 196, 89, 14, Constantes.LABEL_TIPO+Constantes.ESPACIO+Constantes.PUNTOS)); // label tipo 
  
            rbSemanal = Utilidades.obtenerRadio(130, 180, 109, 23, Constantes.RADIO_SEMANAL, true, grupoBotonTipo, rbSemanal); // radio semanal 
            rbSemanal.addActionListener((ActionListener)this); 
            contentPane.add(rbSemanal); 
            
            rbMensual = Utilidades.obtenerRadio(130, 216, 109, 23, Constantes.RADIO_MENSUAL, false, grupoBotonTipo, rbMensual); // radio mensual 
            rbMensual.addActionListener((ActionListener)this); 
            contentPane.add(rbMensual); 
            
            btnMenu = Utilidades.obtenerBoton(35, 271, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG)));
            contentPane.add(btnMenu); 
            
            btnGuardar = Utilidades.obtenerBoton(153, 271, 103, 41, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar 
            btnGuardar.addActionListener((ActionListener)this); 
            btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG)));
            contentPane.add(btnGuardar); 
                  
        } 
        
        @SuppressWarnings("unused")
		public void actionPerformed(ActionEvent evento) {

            if(evento.getSource()==rbSemanal){ 

                resultTipo=rbSemanal.getText(); 
            } 

            if(evento.getSource()==rbMensual){ 
 
                resultTipo=rbMensual.getText(); 
            } 
            // Va a la ventana principal 
            if(evento.getSource()==btnMenu){ 
            	
                ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
                setVisible(false); 
            }

            if(evento.getSource()==btnGuardar){ 
                    
                boolean validar = false;
                
                if(UtilesValida.esNulo(resultTipo)) {
                	resultTipo=rbSemanal.getText(); 
                }
                
                validar=ValidacionesComun.validarCamposPresu(resultFecha, resultTipo, txtPresu.getText(), getTxtPresu()); 
                
                if(validar){
                	
					resultPresu	= Double.valueOf(txtPresu.getText());
					
					if(!FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_PRESUPUESTOS))
						FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_PRESUPUESTOS);
					
					if(!FicherosLog.ficheroLogCreado(Constantes.FICHERO_PRESUPUESTOS))
						FicherosLog.ficheroLogCreado(Constantes.FICHERO_PRESUPUESTOS);
					
					// Se obtiene la ultima linea del el fichero log
					//String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_PRESUPUESTOS);
					String ultimaLinea = FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_PRESUPUESTOS);
					
					if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
						
						if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia, se inserta la cabecera y un registro.
							
							Utilidades.escribirCabeceraRegistroPresus(idPresu, resultFecha, resultTipo, String.valueOf(resultPresu));
							
							FicherosRegistros.insertarRegistroPresu(String.valueOf(idPresu), resultFecha, resultTipo, String.valueOf(resultPresu));
							
							JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

					  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

					  		setVisible(false); // Desparece esta ventana
							
						}
						else {
							
							if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){
								
								boolean ficheroLogCreado = FicherosLog.ficheroLogCreado(Constantes.FICHERO_PRESUPUESTOS);
								boolean ficheroCreado = FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_PRESUPUESTOS);
								
								if(ficheroCreado && ficheroLogCreado) {
									
									Utilidades.escribirCabeceraRegistroPresus(idPresu, resultFecha, resultTipo, String.valueOf(resultPresu));
									
									FicherosRegistros.insertarRegistroPresu(String.valueOf(idPresu), resultFecha, resultTipo, String.valueOf(resultPresu));

									JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

							  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

							  		setVisible(false); // Desparece esta ventana
									
								}
								else {
									JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE);
								}
							}
							else {
								
								boolean esCabecera = Utilidades.compararUltimaLineaConCabeceraPresu(ultimaLinea);
								
								boolean esRaya = Utilidades.compararUltimaLineaConRaya(ultimaLinea);
								
								if(!esCabecera && !esRaya){
									int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
									String ultimoID = ultimaLinea.substring(0,pos);
									//String ultimoID = ultimaLinea.substring(32, 35);
									idPresu = Integer.valueOf(ultimoID.trim());
									idPresu++;
								}
								
								utilidades.FicherosLog.escribirPresuLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idPresu, resultFecha, resultTipo, String.valueOf(resultPresu));
								
								FicherosRegistros.insertarRegistroPresu(String.valueOf(idPresu), resultFecha, resultTipo, String.valueOf(resultPresu));
								
								JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

						  		setVisible(false); // Desparece esta ventana
								
							}
							
						}
					}
                } 
                    
            } // guardar
            
        } // actionPerformed
		
		// Getters
	    public JTextField getTxtPresu(){ 
	    	
	    	return txtPresu; 
	    }
                
}