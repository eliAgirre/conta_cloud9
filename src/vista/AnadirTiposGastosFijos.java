package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 

import javax.swing.*; 

import controlador.ControladorPrincipal;
import gestor.TiposGastosFijos;
import utilidades.Constantes; 
import utilidades.FicherosListas;
import utilidades.UtilesValida; 
import utilidades.Utilidades; 
  
public class AnadirTiposGastosFijos extends JFrame implements ActionListener { 
        
    private static final long serialVersionUID = -8624753721794395368L; 
    
    // Atributos de la clase 
    private JPanel contentPane; 
    
    private JTextField txtNombreGasto; 
    private JButton btnMenu; 
    private JButton btnGuardar; 
    private JButton btnOtroMas;
    
    private int idGastoFijo = 0; 
    private String resultNombreGasto;
    
    private static final String FICHERO = Constantes.FICHERO_GASTOS_FIJOS_LISTA;
    
	// Listas
	private TiposGastosFijos listaGastosFijos = new TiposGastosFijos();

    public AnadirTiposGastosFijos(){ 
            
        // Caracteristicas de la ventana             
        setResizable(false); // no reestablece el size 
        setBounds(100, 100, 400, 300); 
        setTitle(Constantes.VISTA_ANADIR_LISTA_GASTO_FIJO); // titulo 
        setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)).getImage()); //logo 
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
        
        // Carga los componentes de la ventan 
        componentes(); 
            
    } // Cierre constructor 
    
    private void componentes(){ 
            
        // Layout 
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
        setContentPane(contentPane); 
        
        // Labels + textFields + comboBox + buttons 
        contentPane.add(Utilidades.obtenerVersionLabel(-1, 250, 95, 14));  // Version 
        
        contentPane.add(Utilidades.obtenerAvisoInfoLabel(31, 35, 254, 14, Constantes.LIT_INFORMACION)); // Aviso 
        
        contentPane.add(Utilidades.obtenerLabel(31, 81, 130, 14, Constantes.LABEL_NOMBRE_GASTO_FIJO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label nombre del gasto fijo 
        
        txtNombreGasto = Utilidades.obtenerTextField(31, 118, 340, 20, 10, txtNombreGasto, null, true);  // nombre deL gasto fijo
        txtNombreGasto.addActionListener((ActionListener)this); 
        contentPane.add(txtNombreGasto); 
        
        btnMenu = Utilidades.obtenerBoton(35, 183, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu); 
        
        btnGuardar = Utilidades.obtenerBoton(153, 183, 102, 41, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar 
        btnGuardar.addActionListener((ActionListener)this); 
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnGuardar); 
        
		btnOtroMas = Utilidades.obtenerBoton(270, 183, 95, 41, Constantes.BTN_OTRO, btnOtroMas);  // boton otro
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnOtroMas);
              
    } 
    
    @SuppressWarnings("unused") 
    public void actionPerformed(ActionEvent evento) { 
        // Va a la ventana principal 
        if(evento.getSource()==btnMenu){ 
                
        	if( listaGastosFijos.length() > 0) {
        		
        		boolean validar=false;	
        		
        		if(UtilesValida.esNulo(txtNombreGasto.getText())){
                    
                    JOptionPane.showMessageDialog(txtNombreGasto, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_GASTO_FIJO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE); 
                    validar=false; 
                }

    			if(!validar) {
    				
    				resultNombreGasto = txtNombreGasto.getText();
    				
    				if( listaGastosFijos.length() > 0) {
						
						idGastoFijo = listaGastosFijos.getUltimoID(listaGastosFijos.length()-1);
						idGastoFijo++;
					}
    				
    				Utilidades.agregarGastoFijoALista(idGastoFijo, resultNombreGasto, listaGastosFijos);
    				
    				for(int i=0;i<listaGastosFijos.length();i++) {
            			FicherosListas.insertarListas(String.valueOf(listaGastosFijos.getGastoFijo(i).getIdGastoFijo()), listaGastosFijos.getGastoFijo(i).getNombreGasto(), FICHERO);
            		}
    			}
        		
        	}
        	
            ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
            setVisible(false); 
        }
        
		// Si hace clic en Otro, se van agregando registros al array hasta que da al Menu
		if(evento.getSource()==btnOtroMas){
			
			boolean validar=false;			
		
     		if(UtilesValida.esNulo(txtNombreGasto.getText())){
                
                JOptionPane.showMessageDialog(txtNombreGasto, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_GASTO_FIJO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE); 
                validar=false; 
            }

			if(!validar) {
				
				resultNombreGasto = txtNombreGasto.getText();
				
				if(!FicherosListas.ficheroTxtCreado(FICHERO))
					FicherosListas.ficheroTxtCreado(FICHERO);
				
				String ultimaLinea = utilidades.FicherosListas.getUltimaLineaRegistros(FICHERO);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia
						
						idGastoFijo++;
						
						Utilidades.agregarGastoFijoALista(idGastoFijo, resultNombreGasto, listaGastosFijos);
						
						// Se limpian los componentes
						limpiar();
					}
					else {
						
						if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){ // Si el fichero no existe
							
							boolean ficheroCreado = FicherosListas.ficheroTxtCreado(FICHERO);
							
							if(ficheroCreado) { // Si el fichero se ha creado
								
								idGastoFijo++;
								
								Utilidades.agregarGastoFijoALista(idGastoFijo, resultNombreGasto, listaGastosFijos);
								
								// Se limpian los componentes
								limpiar();
							}
						}
						else {
							
							String ultimoID = Constantes.VACIO;
							
							if( listaGastosFijos.length() > 0) {
								
								idGastoFijo = listaGastosFijos.getUltimoID(listaGastosFijos.length()-1);
								idGastoFijo++;
							}
							else {
								int pos = ultimaLinea.indexOf(Constantes.COMA);
								ultimoID = ultimaLinea.substring(0,pos); // ultima linea ID desde registros
								//String ultimoID = ultimaLinea.substring(32, 35); // Se obtiene la ultima ID desde el log
								idGastoFijo = Integer.valueOf(ultimoID.trim());
								idGastoFijo++;
							}
							
							Utilidades.agregarGastoFijoALista(idGastoFijo, resultNombreGasto, listaGastosFijos);
							
							// Se limpian los componentes
							limpiar();							
						}
					} // Si la ultima linea no es vacia
				} // Si el fichero es nula
			}
		}

        if(evento.getSource()==btnGuardar){
                
            boolean validar = false; 
            
     		if(UtilesValida.esNulo(txtNombreGasto.getText())){
                
                JOptionPane.showMessageDialog(txtNombreGasto, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_GASTO_FIJO+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE); 
                validar=false; 
            } 
            
            if(!validar){ 

                resultNombreGasto = txtNombreGasto.getText(); 
                
                if(!FicherosListas.ficheroTxtCreado(FICHERO)) 
                    FicherosListas.ficheroTxtCreado(FICHERO); 

                String ultimaLinea = FicherosListas.getUltimaLineaRegistros(FICHERO); 
                
                if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                        
                    if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia, se inserta un registro. 
                            
                        FicherosListas.insertarListas(String.valueOf(idGastoFijo), resultNombreGasto, FICHERO); 
                        
                        JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA); 

                        ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                        setVisible(false); // Desparece esta ventana 
                            
                    } 
                    else { 
                            
                        if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){ 
                            
                            boolean ficheroCreado = FicherosListas.ficheroTxtCreado(FICHERO); 
                            
                            if(ficheroCreado){ 
                                    
                                FicherosListas.insertarListas(String.valueOf(idGastoFijo), resultNombreGasto, FICHERO); 

                                JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA); 

                                ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                                setVisible(false); // Desparece esta ventana 
                                    
                            } 
                            else { 
                                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
                            } 
                        } 
                        else { 
                                                                                    
                            int pos = ultimaLinea.indexOf(Constantes.COMA); 
                            String ultimoID = ultimaLinea.substring(0,pos); 
                            //String ultimoID = ultimaLinea.substring(32, 35); 
                            idGastoFijo = Integer.valueOf(ultimoID.trim()); 
                            idGastoFijo++; 
                            
                            FicherosListas.insertarListas(String.valueOf(idGastoFijo), resultNombreGasto, FICHERO); 
                            
                            JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA); 

                            ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                            setVisible(false); // Desparece esta ventana 
                                
                        } 
                            
                    } 
                } 
            } 
                
        } // guardar 
        
    } // actionPerformed
    
	// Limpia los datos de los componentes
	private void limpiar(){
		
		txtNombreGasto.setText(Constantes.VACIO);
		
	} // Cierre limpiar
        
} 

