package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.*; 

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class EditarIngreso extends JFrame implements ActionListener {
	
		private static final long serialVersionUID = -8624753721794395368L;
        
        // Atributos de la clase 
        private JPanel contentPane; 
        private JTextField txtID; 
        private JTextField txtImporte; 
        private JTextField dateChooser; 
        private int idIngreso; 
        private JTextField txtConcepto; 
        private JButton btnEditar; 
        private JButton btnVolver; 
        private JButton btnBorrar; 

        // Atributos del resultados user 
        private String resultFecha; 
        private String resultConcepto; 
        private Double resultImporte; 
                
        public EditarIngreso(String id, String fecha, String concepto, String cantidad){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 499, 366); // size 
            setTitle(Constantes.VISTA_EDITAR_INGRESO); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana            
            
            // Carga los componentes de la ventana
            componentes(id, concepto, cantidad);
            
            if(UtilesValida.esNumero(id)) 
            	idIngreso = Integer.valueOf(id);
                
        } // Constructor 
        
        private void componentes(String idIngreso, String concepto, String cantidad){ 
                
            // Layout 
            contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
            setContentPane(contentPane);
            
            // Labels + textFields + comboBox + buttons
            contentPane.add(Utilidades.obtenerVersionLabel(10, 304, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 254, 14, Constantes.LIT_INFO_MODIF)); // Aviso modificar 
            
            contentPane.add(Utilidades.obtenerLabel(29, 117, 48, 14, Constantes.LABEL_ID+Constantes.ESPACIO+Constantes.PUNTOS)); // Label ID 
            
            txtID = Utilidades.obtenerTextField(104, 117, 126, 20, 10, txtID, idIngreso, false);  // ID field 
            txtID.addActionListener((ActionListener)this); 
            contentPane.add(txtID); 
            
            contentPane.add(Utilidades.obtenerLabel(31, 164, 46, 14, Constantes.LABEL_FECHA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha 
            
            dateChooser = Utilidades.obtenerTextField(104, 164, 126, 20, 10, dateChooser, "dateChooser", true); // fecha ingreso 
            dateChooser.addPropertyChangeListener(Constantes.INPUT_FECHA, (PropertyChangeListener) this); 
            contentPane.add(dateChooser);

            contentPane.add(Utilidades.obtenerLabel(255, 117, 95, 14, Constantes.LABEL_CONCEPTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label concepto 
            
            txtConcepto = Utilidades.obtenerTextField(328, 114, 126, 20, 10, txtConcepto, concepto, true);  // concepto field 
            txtConcepto.addActionListener((ActionListener)this); 
            contentPane.add(txtConcepto); 

            contentPane.add(Utilidades.obtenerLabel(255, 164, 95, 14, Constantes.LABEL_IMPORTE+Constantes.ESPACIO+Constantes.PUNTOS)); // Label importe 
            
            txtImporte = Utilidades.obtenerTextField(328, 161, 126, 20, 10, txtImporte, cantidad, true);  // cantidad field 
            txtImporte.addActionListener((ActionListener)this); 
            contentPane.add(txtImporte);

            btnVolver = Utilidades.obtenerBoton(96, 250, 108, 41, Constantes.BTN_VOLVER, btnVolver); // boton volver 
            btnVolver.addActionListener((ActionListener)this); 
            btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_BACK+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnVolver); 
            
            btnEditar = Utilidades.obtenerBoton(215, 250, 112, 41, Constantes.BTN_EDITAR, btnEditar); // boton editar 
            btnEditar.addActionListener((ActionListener)this); 
            btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT2+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnEditar);
            
            btnBorrar = Utilidades.obtenerBoton(336, 250, 111, 41, Constantes.BTN_BORRAR, btnBorrar); // boton borrar 
            btnBorrar.addActionListener((ActionListener)this); 
            btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_DELETE+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnBorrar); 
                
        } // Cierre componentes 
        
        @SuppressWarnings("unused")
		public void actionPerformed(ActionEvent evento) {

            if(evento.getSource()==btnVolver){ 
            	
                TablaIngresos ventanaTablaGastosFijos=new TablaIngresos(); 
                ventanaTablaGastosFijos.setLocationRelativeTo(null); 
                // Hace visible la ventana 
                ventanaTablaGastosFijos.setVisible(true); 
                // Desparece esta ventana 
                setVisible(false); 
            } // volver
            
            if(evento.getSource()==btnEditar){
	        	
	        	boolean validar = false;
	        	
	        	validar = utilidades.ValidacionesComun.validarCamposIngreso(resultFecha, txtConcepto.getText(), txtImporte.getText(), txtConcepto, txtImporte);
	        	
	        	if(validar) {
	        		
	        		resultConcepto = txtConcepto.getText();
	        		resultImporte = Double.valueOf(txtImporte.getText());
	        		
					// Se obtiene la ultima linea del el fichero log
					String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_INGRESOS);
					
					if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
						
						if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) {
							
							FicherosLog.escribirIngresoLog(ListaConstantesValidacion.TIPO_COMUNICACION[1], Integer.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));
							
							ArrayList<String> listaIngresos = FicherosRegistros.editarListaIngresos(String.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));
							
							if(!UtilesValida.esNula(listaIngresos)) {
								
								if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_INGRESOS)) {
									
									if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_INGRESOS)) {
										
										FicherosRegistros.insertarRegistros(listaIngresos, Constantes.FICHERO_INGRESOS);
										
										JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_MODI);

								  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

								  		setVisible(false); // Desparece esta ventana
									}
									else 
										JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
								}
								else
									JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO);
							}
							else 
								JOptionPane.showMessageDialog(null, Constantes.ERROR_MODI);
						}
					}
	        		
	        	}
	        	
	        } // editar
            
            if(evento.getSource()==btnBorrar){ 
                
                // Se obtiene la ultima linea del el fichero log 
                String ultimaLinea = utilidades.FicherosLog.getUltimaLineaLog(Constantes.FICHERO_INGRESOS); 
                
                if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                        
                    if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) { 
                    	
                    	boolean validar = false;
                    	
                    	validar = utilidades.ValidacionesComun.validarCamposIngreso(resultFecha, txtConcepto.getText(), txtImporte.getText(), txtConcepto, txtImporte);
                            
                        if(validar) {
                        	
        	        		resultConcepto = txtConcepto.getText();
        	        		resultImporte = Double.valueOf(txtImporte.getText());
                        
        	        		FicherosLog.escribirIngresoLog(ListaConstantesValidacion.TIPO_COMUNICACION[2], Integer.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte)); 
                            
                            ArrayList<String> listaIngresos = FicherosRegistros.borrarRegistroIngreso(String.valueOf(idIngreso)); 
                            
                            if(!UtilesValida.esNula(listaIngresos)) { 
                                    
                                if(FicherosRegistros.eliminarFicheroRegistro(Constantes.FICHERO_INGRESOS)) { 
                                        
                                    if(FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_INGRESOS)) { 
                                            
                                        FicherosRegistros.insertarRegistros(listaIngresos, Constantes.FICHERO_INGRESOS); 
                                        
                                        JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_BAJA); 

                                        ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                                        setVisible(false); // Desparece esta ventana 
                                    } 
                                    else
                                    	JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO);
                                } 
                                else
                                	JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO); 
                            } 
                            else
                                JOptionPane.showMessageDialog(null, Constantes.ERROR_BAJA);
                        }      
                    } 
                } 
            } // borrar
        } // Cierre del metodo actionPerformed
        
        // Getters 
        public JTextField getTxtCantidad() { 
                return txtImporte; 
        } 
        public JTextField getTxtConcepto() { 
                return txtConcepto; 
        }                
        
} // Cierre clase