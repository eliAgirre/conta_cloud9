package vista;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 

import javax.swing.*;
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla; 

public class VerPresupuestos extends JFrame implements ActionListener { 
	
	private static final long serialVersionUID = -8624753721794395368L;
    
    // Atributos de la clase 
    private JPanel contentPane; 
    private JRadioButton rbSemanal; 
    private JRadioButton rbMensual; 
    private final ButtonGroup buttonGroup = new ButtonGroup(); 
    private JButton btnVer; 
    private JButton btnTodos; 
    private JButton btnMenu; 
    private String resultTipoGasto; 
            
    // Atributos relacionados con la tabla 
    private JScrollPane scrollPane; 
    private DefaultTableModel modelo; 
    private String [][] arrayTabla; //array bidimensional 
    private JTable tabla; 
    private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_PRESUS]; 
    private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_PRESUPUESTOS); 
    
    public VerPresupuestos(){ 
            
        // Caracteristicas de la ventana 
        setResizable(false); // no reestablece el size 
        setBounds(100, 100, 446, 544); // size 
        setTitle(Constantes.VISTA_VER_PRESUPUESTOS); // titulo 
        setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
        
        // Carga los componentes de la ventana 
        componentes(); 
        
        modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
            
    } // Cierre constructor 
    
    public void componentes(){ 
            
        // Layout 
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
        
        // Panel para visualizar y hacer scroll 
        scrollPane = Utilidades.obtenerScroll(10, 138, 420, 303, scrollPane, false);
        contentPane.add(scrollPane);
        
        // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
        modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_PRESUPUESTOS); 
        
        // Se le pasa a JTable el modelo de tabla
        tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);
        
        // Labels + textFields + radio buttons + comboBox + buttons 
        contentPane.add(Utilidades.obtenerVersionLabel(0, 490, 95, 14)); // Version label 
        
        contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 35, 182, 14, Constantes.LIT_INFO_VER+Constantes.PUNTOS)); // Aviso de seleccion a visualizar 
        
        rbSemanal = Utilidades.obtenerRadio(144, 58, 109, 23, Constantes.RADIO_SEMANAL, true, buttonGroup, rbSemanal); // radio semanal 
        rbSemanal.addActionListener((ActionListener)this); 
        contentPane.add(rbSemanal); 
        
        rbMensual = Utilidades.obtenerRadio(144, 86, 109, 23, Constantes.RADIO_MENSUAL, false, buttonGroup, rbMensual); // radio mensual 
        rbMensual.addActionListener((ActionListener)this); 
        contentPane.add(rbMensual); 
        
        btnVer = Utilidades.obtenerBoton(289, 35, 109, 41, Constantes.BTN_VER, btnVer); // boton ver 
        btnVer.addActionListener((ActionListener)this); 
        btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnVer); 
        
        btnTodos = Utilidades.obtenerBoton(289, 86, 109, 41, Constantes.BTN_TODOS, btnTodos); // boton todos 
        btnTodos.addActionListener((ActionListener)this); 
        btnTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ALL+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnTodos); 
        
        btnMenu = Utilidades.obtenerBoton(169, 471, 106, 33, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu); 
            
    } // Cierre componentes

    @SuppressWarnings("unused") 
    public void actionPerformed(ActionEvent evento) {         

            if(evento.getSource()==rbSemanal){ 

                resultTipoGasto = rbSemanal.getText();                         
            } 

            if(evento.getSource()==rbMensual){ 

            	resultTipoGasto = rbMensual.getText();                         
            } 
            
            // ver los presupuestos que haya seleccionado 
            if(evento.getSource()==btnVer){ 
                    
                UtilidadesTabla.limpiarTabla(modelo); 
                
                if(!UtilesValida.esNulo(resultTipoGasto)){ // si se ha seleccionado la radio 
                        
                    if(!UtilesValida.esNulo(registros)){ 
                            
                        for(int i=0; i<registros.size(); i++){ 
                            
                            datosFile = registros.get(i).split(Constantes.PUNTO_COMA); 
                            
                            if(datosFile[3].equals(resultTipoGasto)){ 
                                    
                                modelo.addRow(datosFile); 
                            } 
                        } 
                            
                    } 
                        
                } 
                else if(UtilesValida.esNulo(resultTipoGasto)){ 
                        
                    modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo); 
                } 
                    
            } 
            // Se visualizan todos los presupuestos 
            if(evento.getSource()==btnTodos){ 
                    
                UtilidadesTabla.limpiarTabla(modelo); 
                modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
            } 
            
            // Se guardan los datos y vuelve a la principal 
            if(evento.getSource()==btnMenu){ 
                    
                  ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
                  setVisible(false); 
            }                 
        } // Cierre actionPerformed 
        
} // Cierre clase 