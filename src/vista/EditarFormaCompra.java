package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 

import javax.swing.*; 

import controlador.ControladorPrincipal; 
import utilidades.Constantes; 
import utilidades.FicherosListas; 
import utilidades.UtilesValida; 
import utilidades.Utilidades; 

public class EditarFormaCompra extends JFrame implements ActionListener { 
        
        private static final long serialVersionUID = -8624753721794395368L; 
        
        // Atributos de la clase 
        private JPanel contentPane; 
        private JTextField txtID; 
        private JTextField txtFormaCompra; 
        private JButton btnEditar; 
        private JButton btnVolver; 
        private String idFormaCompra; 
        
        // Atributos del resultados user 
        private String resultFormaCompra;
        
        private static final String FICHERO = Constantes.FICHERO_FORMA_COMPRA;
                
        public EditarFormaCompra(String idFormaCompra, String formaCompra){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 300, 366); // size 
            setTitle(Constantes.VISTA_EDITAR_FORMA_COMPRA); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_EDIT+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            this.idFormaCompra = idFormaCompra; 
            
            // Carga los componentes de la vista 
            componentes(this.idFormaCompra, formaCompra); 
                
        } // Constructor 
        
        private void componentes(String idFormaCompra, String formaCompra){ 
                
            // Layout 
	        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane); 
	        setContentPane(contentPane); 
                
            // Labels + textFields + comboBox + buttons 
            // Version label 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 312, 95, 14)); 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 254, 14, Constantes.LIT_INFO_MODIF)); // Aviso de modificacion 
            
            contentPane.add(Utilidades.obtenerLabel(31, 89, 48, 14, Constantes.LABEL_ID+Constantes.ESPACIO+Constantes.PUNTOS)); // Label id 
            
            txtID = Utilidades.obtenerTextField(80, 89, 126, 20, 10, txtID, idFormaCompra, false); 
            txtID.addActionListener((ActionListener)this); 
            contentPane.add(txtID); 
            
            contentPane.add(Utilidades.obtenerLabel(31, 164, 46, 14, Constantes.LABEL_FORMA_COMPRA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label forma de compra 
            
            txtFormaCompra = Utilidades.obtenerTextField(80, 161, 126, 20, 10, txtFormaCompra, formaCompra, true); 
            txtFormaCompra.addActionListener((ActionListener)this); 
            contentPane.add(txtFormaCompra); 
            
            btnEditar = Utilidades.obtenerBoton(150, 250, 108, 41, Constantes.BTN_EDITAR, btnEditar); // boton editar 
            btnEditar.addActionListener((ActionListener)this); 
            btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_EDIT2+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnEditar); 
            
            btnVolver = Utilidades.obtenerBoton(30, 250, 108, 41, Constantes.BTN_VOLVER, btnVolver); // boton volver 
            btnVolver.addActionListener((ActionListener)this); 
            btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_BACK+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnVolver); 
                
        } // Cierre componentes 
        

        @SuppressWarnings("unused") 
        public void actionPerformed(ActionEvent evento) { 

            if(evento.getSource()==btnVolver){ 
                    
				TablaFormasCompra ventanaTablaFormaCompra=new TablaFormasCompra(); 
				ventanaTablaFormaCompra.setLocationRelativeTo(null); 
				// Hace visible la ventana 
				ventanaTablaFormaCompra.setVisible(true); 
				// Desparece esta ventana 
				setVisible(false); 
            } 

            if(evento.getSource()==btnEditar){ 
                    
                boolean validar=true;         
                
                if(UtilesValida.esNulo(txtFormaCompra.getText())){ 
            
	                JOptionPane.showMessageDialog(txtFormaCompra, Constantes.AVISO_INTRO_DATO_CAMPO+Constantes.CAMPO_VISTA_FORMA_COMPRA+Constantes.PUNTO, Constantes.VALIDA_CAMPO_VACIO, JOptionPane.WARNING_MESSAGE); 
	                validar=false; 
	            } 
            
                if(validar) { 
                    
                    resultFormaCompra = txtFormaCompra.getText(); 
                    
                    String ultimaLinea = utilidades.FicherosListas.getUltimaLineaRegistros(FICHERO); 
                    
                    if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula 
                            
                        if(!ultimaLinea.equals(Constantes.VACIO) && !ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE) ) { 
                                
                            ArrayList<String> lista = FicherosListas.editarRegistroListas(String.valueOf(idFormaCompra), resultFormaCompra, FICHERO); 
                            
                            if(!UtilesValida.esNula(lista)) { 
                                    
                                if(FicherosListas.eliminarFicheroRegistroLista(FICHERO)) { 
                                        
                                    if(FicherosListas.ficheroTxtCreado(FICHERO)) { 
                                            
                                        FicherosListas.insertarRegistrosLista(lista, FICHERO); 
                                        
                                        JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_MODI); 

                                        ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 

                                        setVisible(false); // Desparece esta ventana 
                                    } 
                                    else 
                                    	JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO); 
                                } 
                                else 
                                	JOptionPane.showMessageDialog(null, Constantes.ERROR_ELIMINAR_ARCHIVO); 
                            } 
                            else 
                            	JOptionPane.showMessageDialog(null, Constantes.ERROR_MODI); 
                        } 
                    } 
                    
                } 
                    
            } // editar 
                
        } // Cierre del metodo actionPerformed 
        
        // Getters 
        public JTextField getTxtFormaCompra() { 
                return txtFormaCompra; 
        } 
        
} // Cierre clase