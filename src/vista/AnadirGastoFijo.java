package vista;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class AnadirGastoFijo extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private static JTextField txtImporte;
	private static JTextField txtNumFactura;
	private static JTextField txtDuracion;
	private JComboBox<String> cbTipoGasto;
	private int idGastoFijo;
	private JTextField fechaFactura;
	private JTextField fechaInicio;
	private JTextField fechaFin;
	
	private JButton btnMenu;
	private JButton btnGuardar;
	//private JButton btnOtroMas;
		
	// Atributos del resultados user
	private String resultFechaFact;
	private String resultNumFactura;
	private String resultTipoGasto;
	private String resultFechaInicio;
	private String resultFechaFin;
	private String resultDuracion;
	private Double resultImporte;
		
	
	public AnadirGastoFijo(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 605, 351); // size
		setTitle(Constantes.VISTA_ANADIR_GASTO_FIJO); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();
		
        // LISTA DE GASTOS FIJOS
        try { 
            if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_GASTO_FIJO), cbTipoGasto) )) 
                    JOptionPane.showMessageDialog(null, Constantes.LISTA_GASTO_FIJO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (HeadlessException e) { 
                JOptionPane.showMessageDialog(null, Constantes.LISTA_GASTO_FIJO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA); 
        } catch (FileNotFoundException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
        } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
        }
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Labels + textFields + comboBox + buttons
		contentPane.add(Utilidades.obtenerVersionLabel(10, 264, 95, 14)); // Version label
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(36, 31, 254, 14, Constantes.LIT_INFORMACION)); // label info
		
		contentPane.add(Utilidades.obtenerLabel(36, 67, 105, 14, Constantes.LABEL_FECHA_DOC+Constantes.ESPACIO+Constantes.PUNTOS)); // label fecha factura
		
		fechaFactura = Utilidades.obtenerTextField(137, 67, 126, 20, 10, fechaFactura, null, true);  // fechaFactura field
		fechaFactura.addActionListener((ActionListener)this);
		contentPane.add(fechaFactura);
		
		contentPane.add(Utilidades.obtenerLabel(320, 64, 105, 14, Constantes.LABEL_NUM_DOC+Constantes.ESPACIO+Constantes.PUNTOS)); // label num factura

		txtNumFactura = Utilidades.obtenerTextField(421, 61, 126, 20, 10, txtNumFactura, null, true);  // txtNumFactura field
		txtNumFactura.addActionListener((ActionListener)this);
		contentPane.add(txtNumFactura);
		
		contentPane.add(Utilidades.obtenerLabel(36, 164, 75, 14, Constantes.LABEL_TIPO_GASTO+Constantes.ESPACIO+Constantes.PUNTOS)); // label tipo gasto
		
		cbTipoGasto = Utilidades.obtenerCombo(137, 161, 126, 20, cbTipoGasto); // combo gastos fijos
		cbTipoGasto.addActionListener((ActionListener)this);
		contentPane.add(cbTipoGasto);
		
		contentPane.add(Utilidades.obtenerLabel(36, 113, 105, 14, Constantes.LABEL_FECHA_INICIO+Constantes.ESPACIO+Constantes.PUNTOS)); // label fecha inicio
		
		fechaInicio = Utilidades.obtenerTextField(137, 113, 126, 20, 10, fechaInicio, null, true);  // fechaInicio field
		fechaInicio.addActionListener((ActionListener)this);
		contentPane.add(fechaInicio);
		
		contentPane.add(Utilidades.obtenerLabel(320, 119, 105, 14, Constantes.LABEL_FECHA_FIN+Constantes.ESPACIO+Constantes.PUNTOS)); // label fecha fin
		
		fechaFin = Utilidades.obtenerTextField(421, 119, 126, 20, 10, fechaFin, null, true);  // fechaFin field
		fechaFin.addActionListener((ActionListener)this);
		contentPane.add(fechaFin);
		
		contentPane.add(Utilidades.obtenerLabel(320, 167, 75, 14, Constantes.LABEL_DURACION+Constantes.ESPACIO+Constantes.PUNTOS)); // label duracion
		
		contentPane.add(Utilidades.obtenerTextField(421, 164, 126, 20, 10, txtDuracion, Constantes.DURACION_DEFAULT ,false));
		
		contentPane.add(Utilidades.obtenerLabel(320, 210, 75, 14, Constantes.LABEL_IMPORTE+Constantes.ESPACIO+Constantes.PUNTOS)); // label importe
		
		txtImporte = Utilidades.obtenerTextField(421, 207, 126, 20, 10, txtImporte, null, true);  // txtImporte field
		txtImporte.addActionListener((ActionListener)this);
		contentPane.add(txtImporte);
		
		btnMenu = Utilidades.obtenerBoton(168, 254, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG)));
		contentPane.add(btnMenu); 
		
		btnGuardar = Utilidades.obtenerBoton(320, 254, 115, 41, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG)));
		contentPane.add(btnGuardar);
		
		/*btnOtroMas = Utilidades.obtenerBoton(414, 254, 115, 41, Constantes.BTN_OTRO, btnOtroMas);  // boton otro
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)));
		contentPane.add(btnOtroMas);*/
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {

		if(evento.getSource()==cbTipoGasto){
			
			if(cbTipoGasto.isValid())
				resultTipoGasto = cbTipoGasto.getSelectedItem().toString();
		}
		
		// Va a la ventana principal
		if(evento.getSource()==btnMenu){ 

			ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			setVisible(false);
		}
		
		if(evento.getSource()==btnGuardar){
			
			boolean validar = false;
			
			validar = utilidades.ValidacionesComun.validarCamposGastosFijos(fechaFactura.getText(), txtNumFactura.getText(), fechaInicio.getText(), fechaFin.getText(), resultTipoGasto, txtDuracion.getText(), String.valueOf(txtImporte.getText()), txtNumFactura, txtImporte);
			
			if(validar) {
				
				resultFechaFact 	= fechaFactura.getText();
				resultFechaInicio 	= fechaInicio.getText();
				resultFechaFin		= fechaFin.getText();
				resultNumFactura 	= txtNumFactura.getText();
				resultDuracion		= txtDuracion.getText();
				resultImporte 		= Double.valueOf(txtImporte.getText());
				
				if(!FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_GASTOS_FIJOS))
					FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_GASTOS_FIJOS);
				
				if(!FicherosLog.ficheroLogCreado(Constantes.FICHERO_GASTOS_FIJOS))
					FicherosLog.ficheroLogCreado(Constantes.FICHERO_GASTOS_FIJOS);
				
				// Se obtiene la ultima linea
				String ultimaLinea = utilidades.FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_GASTOS_FIJOS);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia, se inserta la cabecera y un registro.
						
						Utilidades.escribirCabeceraRegistroGastosFijos(idGastoFijo, resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte));
						
						FicherosRegistros.insertarRegistroGastoFijo(String.valueOf(idGastoFijo), resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, resultDuracion, String.valueOf(resultImporte));
						
						JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

				  		setVisible(false); // Desparece esta ventana
						
					}
					else {
						
						if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){
							
							boolean ficheroLogCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_GASTOS_FIJOS);
							boolean ficheroCreado = utilidades.FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_GASTOS_FIJOS);
							
							if(ficheroCreado && ficheroLogCreado) {
								
								Utilidades.escribirCabeceraRegistroGastosFijos(idGastoFijo, resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte));
								
								FicherosRegistros.insertarRegistroGastoFijo(String.valueOf(idGastoFijo), resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, resultDuracion, String.valueOf(resultImporte));

								JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

						  		setVisible(false); // Desparece esta ventana
							}
						}
						else {
							
							boolean esCabecera = utilidades.Utilidades.compararUltimaLineaConCabeceraCompras(ultimaLinea);
							
							boolean esRaya = utilidades.Utilidades.compararUltimaLineaConRaya(ultimaLinea);
							
							if(!esCabecera && !esRaya){
								int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
								String ultimoID = ultimaLinea.substring(0,pos);
								//String ultimoID = ultimaLinea.substring(32, 35);
								idGastoFijo = Integer.valueOf(ultimoID.trim());
								idGastoFijo++;
							}
							
							Utilidades.escribirCabeceraRegistroGastosFijos(idGastoFijo, resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte));
							
							FicherosRegistros.insertarRegistroGastoFijo(String.valueOf(idGastoFijo), resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, resultDuracion, String.valueOf(resultImporte));
							
							JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

					  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

					  		setVisible(false); // Desparece esta ventana
						}
					}
				}
				else {
					
					if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){
						
						boolean ficheroLogCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_GASTOS_FIJOS);
						boolean ficheroCreado = utilidades.FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_GASTOS_FIJOS);
						
						if(ficheroCreado) {
							
							Utilidades.escribirCabeceraRegistroGastosFijos(idGastoFijo, resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte));

							FicherosRegistros.insertarRegistroGastoFijo(String.valueOf(idGastoFijo), resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, resultDuracion, String.valueOf(resultImporte));
							
							JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

					  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

					  		setVisible(false); // Desparece esta ventana
						}
						else {
							JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO + "El sistema no puede encontrar la ruta especificada.");
						}
						
					}
					else {
						
						boolean esCabecera = utilidades.Utilidades.compararUltimaLineaConCabeceraGastosFijos(ultimaLinea);
						
						boolean esRaya = utilidades.Utilidades.compararUltimaLineaConRaya(ultimaLinea);
						
						if(!esCabecera && !esRaya){
							int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
							String ultimoID = ultimaLinea.substring(0,pos);
							//String ultimoID = ultimaLinea.substring(32, 35);
							idGastoFijo = Integer.valueOf(ultimoID.trim());
							idGastoFijo++;
						}
						
						FicherosLog.escribirGastoFijoLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idGastoFijo, resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, String.valueOf(resultImporte));
						
						FicherosRegistros.insertarRegistroGastoFijo(String.valueOf(idGastoFijo), resultFechaFact, resultNumFactura, resultFechaInicio, resultFechaFin, resultTipoGasto, resultDuracion, String.valueOf(resultImporte));
						
						JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

				  		setVisible(false); // Desparece esta ventana
						
					}
					
				}
				
			}
		}
	} // Cierre actionPerformed

	// getters
	public static JTextField getTxtImporte() {
		return txtImporte;
	}

	public static JTextField getTxtNumFactura() {
		return txtNumFactura;
	}
	
} // Cierre clase