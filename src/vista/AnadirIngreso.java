package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosLog;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;

public class AnadirIngreso extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
    
    // Atributos de la clase 
    private JPanel contentPane; 
    private JTextField txtFechaIngreso; 
    private JTextField txtImporte; 
    private int idIngreso; 
    private JTextField txtConcepto; 
    private JButton btnMenu; 
    private JButton btnGuardar;

    // Atributos del resultados user 
    private String resultFecha; 
    private String resultConcepto; 
    private Double resultImporte; 
        
    public AnadirIngreso(){ 
            
        // Caracteristicas de la ventana 
        setResizable(false); // no reestablece el size 
        setBounds(100, 100, 313, 357); // size 
        setTitle(Constantes.VISTA_ANADIR_INGRESO); // titulo 
        setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_ADD+Constantes.EXTENSION_PNG)).getImage()); //logo 
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
        
        componentes(); 
            
    } // Constructor 
        
    private void componentes(){
            
        // Layout 
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
        
        // Labels + textFields + comboBox + buttons 
        contentPane.add(Utilidades.obtenerVersionLabel(0, 303, 95, 14)); // Version label 
        
        contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 254, 14, Constantes.LIT_INFORMACION)); // Aviso 
        
        contentPane.add(Utilidades.obtenerLabel(31, 89, 46, 14, Constantes.LABEL_FECHA+Constantes.ESPACIO+Constantes.PUNTOS)); // Label fecha 
        
        txtFechaIngreso = Utilidades.obtenerTextField(104, 89, 126, 20, 10, txtFechaIngreso, null, true);  // fechaIngreso field 
        txtFechaIngreso.addActionListener((ActionListener)this); 
        contentPane.add(txtFechaIngreso);
        
        contentPane.add(Utilidades.obtenerLabel(31, 141, 95, 14, Constantes.LABEL_CONCEPTO+Constantes.ESPACIO+Constantes.PUNTOS)); // Label concepto
        
        txtConcepto = Utilidades.obtenerTextField(104, 138, 126, 20, 10, txtConcepto, null, true);  // concepto field 
        txtConcepto.addActionListener((ActionListener)this); 
        contentPane.add(txtConcepto); 

        contentPane.add(Utilidades.obtenerLabel(31, 188, 95, 14, Constantes.LABEL_IMPORTE+Constantes.ESPACIO+Constantes.PUNTOS)); // Label importe 
        
        txtImporte = Utilidades.obtenerTextField(104, 185, 126, 20, 10, txtImporte, null, true);  // cantidad field 
        txtImporte.addActionListener((ActionListener)this); 
        contentPane.add(txtImporte); 
        
        btnMenu = Utilidades.obtenerBoton(31, 241, 112, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu); 
        
        btnGuardar = Utilidades.obtenerBoton(165, 241, 112, 41, Constantes.BTN_GUARDAR, btnGuardar); // boton guardar 
        btnGuardar.addActionListener((ActionListener)this); 
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SAVE+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnGuardar); 
            
    } // Cierre componentes 
    
    @SuppressWarnings("unused") 
    public void actionPerformed(ActionEvent evento) {
        // Va a la ventana principal 
        if(evento.getSource()==btnMenu){ 

            ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
            // Desparece esta ventana 
            setVisible(false); 
        }

        if(evento.getSource()==btnGuardar){
        	
        	boolean validar = false;
        	
        	validar = utilidades.ValidacionesComun.validarCamposIngreso(txtFechaIngreso.getText(), txtConcepto.getText(), txtImporte.getText(), txtConcepto, txtImporte);
        	
			if(!FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_INGRESOS))
				FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_INGRESOS);
			
			if(!FicherosLog.ficheroLogCreado(Constantes.FICHERO_INGRESOS))
				FicherosLog.ficheroLogCreado(Constantes.FICHERO_INGRESOS);
        	
        	if(validar) {
        		
        		resultFecha		= txtFechaIngreso.getText();
        		resultConcepto	= txtConcepto.getText();
        		resultImporte	= Double.valueOf(txtImporte.getText());
        		
				// Se obtiene la ultima linea
				String ultimaLinea = utilidades.FicherosRegistros.getUltimaLineaRegistros(Constantes.FICHERO_INGRESOS);
				
				if(!UtilesValida.isNull(ultimaLinea)) { // Si la ultima linea no es nula
					
					if(ultimaLinea.equals(Constantes.VACIO)) { // Si la ultima linea es vacia, se inserta la cabecera y un registro.
						
						Utilidades.escribirCabeceraRegistroIngresos(idIngreso, resultFecha, resultConcepto, String.valueOf(resultImporte));
						
						FicherosRegistros.insertarRegistroIngreso(String.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));
						
						JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

				  		setVisible(false); // Desparece esta ventana
						
					}
					else {
						
						if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){
							
							boolean ficheroLogCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_INGRESOS);
							boolean ficheroCreado = utilidades.FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_INGRESOS);
							
							if(ficheroCreado && ficheroLogCreado) {
								
								Utilidades.escribirCabeceraRegistroIngresos(idIngreso, resultFecha, resultConcepto, String.valueOf(resultImporte));
								
								FicherosRegistros.insertarRegistroIngreso(String.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));

								JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

						  		setVisible(false); // Desparece esta ventana
							}
						}
						else {
							
							boolean esCabecera = utilidades.Utilidades.compararUltimaLineaConCabeceraCompras(ultimaLinea);
							
							boolean esRaya = utilidades.Utilidades.compararUltimaLineaConRaya(ultimaLinea);
							
							if(!esCabecera && !esRaya){
								int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
								String ultimoID = ultimaLinea.substring(0,pos);
								//String ultimoID = ultimaLinea.substring(32, 35);
								idIngreso = Integer.valueOf(ultimoID.trim());
								idIngreso++;
							}
							
							Utilidades.escribirCabeceraRegistroIngresos(idIngreso, resultFecha, resultConcepto, String.valueOf(resultImporte));
							
							FicherosRegistros.insertarRegistroIngreso(String.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));
							
							JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

					  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

					  		setVisible(false); // Desparece esta ventana
						}
						
					}
				}
				else {
					
					if(ultimaLinea.equals(Constantes.AVISO_FICHERO_NO_EXISTE)){
						
						boolean ficheroLogCreado = utilidades.FicherosLog.ficheroLogCreado(Constantes.FICHERO_INGRESOS);
						boolean ficheroCreado = utilidades.FicherosRegistros.ficheroTxtCreado(Constantes.FICHERO_INGRESOS);
						
						if(ficheroCreado && ficheroLogCreado) {
							
							Utilidades.escribirCabeceraRegistroIngresos(idIngreso, resultFecha, resultConcepto, String.valueOf(resultImporte));

							FicherosRegistros.insertarRegistroIngreso(String.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));
							
							JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

					  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

					  		setVisible(false); // Desparece esta ventana
						}
						else {
							JOptionPane.showMessageDialog(null, Constantes.ERROR_CREAR_ARCHIVO + "El sistema no puede encontrar la ruta especificada.");
						}
						
					}
					else {
						
						boolean esCabecera = utilidades.Utilidades.compararUltimaLineaConCabeceraGastosFijos(ultimaLinea);
						
						boolean esRaya = utilidades.Utilidades.compararUltimaLineaConRaya(ultimaLinea);
						
						if(!esCabecera && !esRaya){
							int pos = ultimaLinea.indexOf(Constantes.PUNTO_COMA);
							String ultimoID = ultimaLinea.substring(0,pos);
							//String ultimoID = ultimaLinea.substring(32, 35);
							idIngreso = Integer.valueOf(ultimoID.trim());
							idIngreso++;
						}
						
						utilidades.FicherosLog.escribirIngresoLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idIngreso, resultFecha, resultConcepto, String.valueOf(resultImporte));
						
						FicherosRegistros.insertarRegistroIngreso(String.valueOf(idIngreso), resultFecha, resultConcepto, String.valueOf(resultImporte));
						
						JOptionPane.showMessageDialog(null, Constantes.MSG_CORRECTO_ALTA);

				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();

				  		setVisible(false); // Desparece esta ventana
						
					}
					
				}
        		
        	}
        	
        }

    } // Cierre del metodo actionPerformed
    
    // Getters
    public JTextField getTxtImporte() { 
            return txtImporte; 
    } 
    public JTextField getTxtConcepto() { 
            return txtConcepto; 
    }
        
} // Cierre clase 