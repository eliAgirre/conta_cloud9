package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.Utilidades;

public class LogCompras extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnMenu;
	
	public LogCompras(String texto){

		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 862, 536); // size
		setTitle(Constantes.VISTA_LOG_COMPRA); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_FILE+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();
		
		textArea.append(texto);
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Panel para visualizar y hacer scroll
        scrollPane = Utilidades.obtenerScroll(35, 65, 789, 374, scrollPane, false);
        contentPane.add(scrollPane);
		
		// Label
		// Version label
		contentPane.add(Utilidades.obtenerVersionLabel(10, 482, 95, 14));
		
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(35, 22, 343, 14, Constantes.LIT_INFO_LOG_COMPRAS+Constantes.PUNTOS)); // Aviso de log

		textArea = Utilidades.obtenerArea(textArea, false, scrollPane); // Area de texto
		
        btnMenu = Utilidades.obtenerBoton(385, 455, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	

		if(evento.getSource()==btnMenu){ 

	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		setVisible(false);
		}
		
	} // Cierre actionPerformed
	
} // Cierre de clase