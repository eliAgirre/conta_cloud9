package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla; 

public class TablaIngresos extends JFrame implements ActionListener {
	
		private static final long serialVersionUID = -8624753721794395368L;
        
        // Atributos de la clase 
        private JPanel contentPane; 
        private String id; 
        private String fecha; 
        private String concepto; 
        private String importe; 
        private JButton btnMenu; 
        
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla; 
        private static String[] datosFile=new String[Constantes.NUM_COLUMNAS_INGRESOS]; 
        private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_INGRESOS);
        
        public TablaIngresos(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 555, 475); // size 
            setTitle(Constantes.VISTA_INBRESOS_A_EDITAR); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Llama al metodo componentes 
            componentes(); 
                
        } // Constructor 
        
        private void componentes(){
                
            // Layout 
            contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
            setContentPane(contentPane);
            
            // Panel para visualizar y hacer scroll 
            scrollPane = Utilidades.obtenerScroll(24, 77, 498, 263, scrollPane, false);
            contentPane.add(scrollPane);
            
            // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
            modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_INGRESOS); 
            
            // Se le pasa a JTable el modelo de tabla 
            tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);
            modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
            
            // Se agrega un listener a la tabla 
            tabla.addMouseListener(new MouseAdapter() { 
                    // Cuando el raton seleccione un dato de una fila muestra un aviso 
                    public void mouseClicked(MouseEvent evento) { 
                        
                    String nl = System.getProperty(Constantes.PROPERTY_SALTO_LINEA); 
                    int fila = tabla.rowAtPoint(evento.getPoint()); 
                    int columna = 0; 
                    if ((fila > -1) && (columna > -1)) 
                    id=(String) modelo.getValueAt(fila,columna); 
                    fecha=(String) modelo.getValueAt(fila,1); 
                    concepto=(String) modelo.getValueAt(fila,2); 
                    importe=(String) modelo.getValueAt(fila,3); 
                    JOptionPane.showMessageDialog(null, Constantes.MSG_REGISTRO_A_EDITAR+Constantes.PUNTOS+Constantes.ESPACIO+nl+nl+"   Fecha: "+fecha+nl+"   Concepto: "+concepto+nl+"   Importe: "+importe+nl+Constantes.ESPACIO); 
                    EditarIngreso editarIngreso = new EditarIngreso(id,fecha,concepto,importe); 
                    // Coloca la ventana en el centro de la pantalla 
                    editarIngreso.setLocationRelativeTo(null); 
                    // Hace visible la ventana 
                    editarIngreso.setVisible(true); 
                    // Desaparece esta ventana 
                    setVisible(false); 
                              
                } // Cierre del mouseClicked 
            });         
            scrollPane.setViewportView(tabla);// Se agrega la tabla al panel scroll 
            
            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 414, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(24, 36, 254, 14, Constantes.LIT_INFO_EDITAR_INGRESO)); // Aviso de seleccion 
            
            btnMenu = Utilidades.obtenerBoton(226, 368, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnMenu); 
                
        } // Cierre componentes 
        
        @SuppressWarnings("unused")
		public void actionPerformed(ActionEvent evento) {         

            // Se guardan los datos y vuelve a la principal 
            if(evento.getSource()==btnMenu){ 
                  ControladorPrincipal controladorPrincipal=new ControladorPrincipal(); 
                  this.setVisible(false); 
            }
                
        } // Cierre del metodo actionPerformed
        
} // Cierre clase