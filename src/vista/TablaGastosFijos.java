package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.Constantes;
import utilidades.FicherosRegistros;
import utilidades.ListaConstantesValidacion;
import utilidades.UtilesValida;
import utilidades.Utilidades;
import utilidades.UtilidadesTabla;

public class TablaGastosFijos extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = -8624753721794395368L;
	
	// Atributos de la clase
	private JPanel contentPane;
	private JComboBox<String> cbTipoGasto;
	private String id;
	private String fechaDoc;
	private String doc;
	private String gastoFijo;
	private String fechaInicio;
	private String fechaFin;
	private String duracion;
	private String importe;
	private JButton btnBuscar;
	private JButton btnMenu;
	
	// Atributos relacionados con la tabla
	private JScrollPane scrollPane;
	private DefaultTableModel modelo;
	private String [][] arrayTabla; //array bidimensional
	private JTable tabla;
	private static String[] datosFile = new String[Constantes.NUM_COLUMNAS_GASTOS_FIJOS];
	private ArrayList<String> registros = FicherosRegistros.leerRegistros(Constantes.FICHERO_GASTOS_FIJOS);
	
	// Atributos del resultados user
	private String resultTipoGasto;
	
	public TablaGastosFijos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(100, 100, 777, 621); // size
		setTitle(Constantes.VISTA_GASTOS_A_EDITAR ); // titulo
		setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
        
        // Carga los componentes de la vista
        componentes();
        
		// LISTA DE TIPO DE GASTO	
		/*if(utilidades.UtilesValida.esNulo( Utilidades.getCombo( Utilidades.getLista(Constantes.MODELO_GASTO_FIJO), cbTipoGasto) ))
			JOptionPane.showMessageDialog(null, Constantes.LISTA_GASTO_FIJO+Constantes.ESPACIO+Constantes.AVISO_NO_SE_CARGA);*/

		modelo = UtilidadesTabla.mostrarTodosRegistrosTabla(registros, datosFile, modelo);
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
        contentPane = Utilidades.obtenerPanel(5, 5, 5, 5, contentPane);
        setContentPane(contentPane);
		
		// Panel para visualizar y hacer scroll
        scrollPane = Utilidades.obtenerScroll(24, 153, 732, 303, scrollPane, false);
        contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_GASTOS_FIJOS);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane);

		// Se agrega un listener a la tabla
		tabla.addMouseListener(new MouseAdapter() {
			// Cuando el raton seleccione un dato de una fila muestra un aviso
			public void mouseClicked(MouseEvent evento) {
		    	
		    	String nl = System.getProperty(Constantes.PROPERTY_SALTO_LINEA);
		        int fila = tabla.rowAtPoint(evento.getPoint());
		        int columna = 0;
		        if ((fila > -1) && (columna > -1))
		        id = (String)modelo.getValueAt(fila,columna);
		        fechaDoc = (String) modelo.getValueAt(fila,1);
		        doc = (String) modelo.getValueAt(fila,2);
		        fechaInicio = (String) modelo.getValueAt(fila,3);
		        fechaFin = (String) modelo.getValueAt(fila,4);
		        gastoFijo = (String) modelo.getValueAt(fila,5);
		        importe = (String) modelo.getValueAt(fila,7);
		        JOptionPane.showMessageDialog(null, Constantes.MSG_REGISTRO_A_EDITAR+Constantes.PUNTOS+Constantes.ESPACIO+nl+nl+"   Fecha doc: "+fechaDoc+nl+"   Gasto: "+gastoFijo+nl+"   Duracion: "+Constantes.DURACION_DEFAULT+nl+"   Importe: "+importe+nl+" ");
		        EditarGastosFijos editarGastosFijos = new EditarGastosFijos(id,fechaDoc,doc,gastoFijo,fechaInicio,fechaFin,duracion,importe);
				// Coloca la ventana en el centro de la pantalla
		        editarGastosFijos.setLocationRelativeTo(null);
				// Hace visible la ventana
		        editarGastosFijos.setVisible(true);
		        // Desaparece esta ventana
				setVisible(false);
				 
		    } // Cierre del mouseClicked
		});	
		scrollPane.setViewportView(tabla);

        // Labels + textFields + comboBox + buttons
        // Version label
		contentPane.add(Utilidades.obtenerVersionLabel(0, 567, 95, 14));
        
		contentPane.add(Utilidades.obtenerAvisoInfoLabel(24, 36, 254, 14, Constantes.LIT_INFO_EDITAR_GASTO)); // Aviso de gasto a editar
		
		contentPane.add(Utilidades.obtenerLabel(220, 85, 75, 14, Constantes.LABEL_TIPO_GASTO+Constantes.ESPACIO+Constantes.PUNTOS)); // label tipo gasto

        cbTipoGasto = Utilidades.obtenerCombo(305, 78, 141, 30, cbTipoGasto);
		cbTipoGasto.addActionListener((ActionListener)this);
		contentPane.add(cbTipoGasto);
    
        btnBuscar = Utilidades.obtenerBoton(486, 68, 110, 41, Constantes.BTN_BUSCAR, btnBuscar); // boton buscar 
		btnBuscar.addActionListener((ActionListener)this); 
		btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_SEARCH+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnBuscar);

        btnMenu = Utilidades.obtenerBoton(334, 540, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
        btnMenu.addActionListener((ActionListener)this); 
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
        contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource()==cbTipoGasto){
			if(cbTipoGasto.isValid())
				resultTipoGasto = cbTipoGasto.getSelectedItem().toString();
		}
		
		if(evento.getSource()==btnBuscar){

			utilidades.UtilidadesTabla.limpiarTabla(modelo);
			
			if(!utilidades.UtilesValida.esNulo(cbTipoGasto.getSelectedItem()) && !utilidades.UtilesValida.esNulo(resultTipoGasto)){// Si la seleccion de el tipo de gasto no es nula
				
				if(!UtilesValida.esNulo(registros)) {
					
					for(int i=0;i<registros.size();i++) {
						
						datosFile = registros.get(i).split(Constantes.PUNTO_COMA);
						
						if(datosFile[5].equals(cbTipoGasto.getSelectedItem()) || datosFile[5].equals(resultTipoGasto)) {
							// Se agrega cada fila al modelo de tabla
							modelo.addRow(datosFile);
						}
					}
				}
			}
		}
		
		
		if(evento.getSource()==btnMenu){ 
			
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		setVisible(false);
		}
		
	} // Cierre del metodo actionPerformed
	
} // Cierre clase
