package vista; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.text.DecimalFormat; 
import java.util.ArrayList; 

import javax.swing.*; 
import javax.swing.border.EmptyBorder; 
import javax.swing.table.DefaultTableModel; 

import controlador.ControladorPrincipal; 
import utilidades.Constantes; 
import utilidades.FicherosRegistros; 
import utilidades.ListaConstantesValidacion; 
import utilidades.UtilesValida; 
import utilidades.Utilidades; 
import utilidades.UtilidadesTabla; 

public class VerAhorro extends JFrame implements ActionListener{ 

        private static final long serialVersionUID = -8624753721794395368L; 
                
        // Atributos de la clase 
        private JPanel contentPane; 
        private JButton btnConsultar; 
        private JButton btnMenu;   
        private JLabel lblTotal; 
        
        // Atributos relacionados con la tabla 
        private JScrollPane scrollPane; 
        private DefaultTableModel modelo; 
        private String [][] arrayTabla; //array bidimensional 
        private JTable tabla; 
        private static String[] datosFile 		= new String[Constantes.NUM_COLUMNAS_AHORRO];
        
        // Listas de registros
        ArrayList<String> registrosCompras 		= FicherosRegistros.leerRegistros(Constantes.FICHERO_COMPRAS); 
        ArrayList<String> registrosGastosFijos	= FicherosRegistros.leerRegistros(Constantes.FICHERO_GASTOS_FIJOS); 
        ArrayList<String> registrosIngresos		= FicherosRegistros.leerRegistros(Constantes.FICHERO_INGRESOS); 
        
        // Atributos del resultados user 
        
        public VerAhorro(){ 
                
            // Caracteristicas de la ventana 
            setResizable(false); // no reestablece el size 
            setBounds(100, 100, 678, 609); // size 
            setTitle(Constantes.VISTA_AHORRO_MENSUAL); // titulo 
            setIconImage(new ImageIcon(getClass().getResource(Constantes.RUTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG)).getImage()); //logo 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana 
            
            // Carga los componentes de la ventana 
            componentes(); 
            
            modelo = UtilidadesTabla.mostrarTodosRegistrosTablaAhorro(registrosCompras, registrosGastosFijos, registrosIngresos, datosFile, modelo); 
                
        } // cierra constructor 
        
        private void componentes(){ 
                
            // Layout 
            contentPane = new JPanel(); 
            contentPane.setBorder(new EmptyBorder(5, 5, 5, 5)); 
            setContentPane(contentPane); 
            contentPane.setLayout(null); 
            
            // Panel para visualizar y hacer scroll 
            scrollPane = new JScrollPane(); 
            scrollPane.setViewportBorder(null); 
            scrollPane.setEnabled(false); 
            scrollPane.setBounds(24, 153, 617, 303); 
            contentPane.add(scrollPane); 
                    
            // Modelo tabla, contiene cabecera y arrayBidimensional para los datos 
            modelo = new DefaultTableModel(arrayTabla, ListaConstantesValidacion.CABECERA_AHORRO); 
                        
            // Se le pasa a JTable el modelo de tabla                 
            tabla = Utilidades.obtenerTabla(tabla, modelo, false, scrollPane); 
            
            // Labels + textFields + comboBox + buttons 
            contentPane.add(Utilidades.obtenerVersionLabel(0, 554, 95, 14)); // Version label 
            
            contentPane.add(Utilidades.obtenerAvisoInfoLabel(10, 26, 323, 14, Constantes.LIT_INFO_VER+Constantes.PUNTOS)); // Aviso de seleccion 
            
            contentPane.add(Utilidades.obtenerLabel(422, 481, 50, 14, Constantes.LABEL_AHORRO+Constantes.PUNTOS+Constantes.ESPACIO)); // label ahorro 
            
            //contentPane.add(Utilidades.obtenerLabel(470, 481, 78, 14, String.valueOf(total))); // label importe 
            lblTotal = Utilidades.obtenerLabel(470, 481, 78, 14, calcularAhorro(registrosCompras, registrosGastosFijos, registrosIngresos)); // cantidad total             
            contentPane.add(lblTotal); 
            
            /* 
            btnConsultar = Utilidades.obtenerBoton(500, 54, 140, 59, Constantes.BTN_CONSULTAR, btnConsultar); // boton consultar 
            btnConsultar.addActionListener((ActionListener)this); 
            btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_VIEW+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnConsultar); */ 
            
            btnMenu = Utilidades.obtenerBoton(220, 528, 95, 41, Constantes.BTN_MENU, btnMenu); // boton menu 
            btnMenu.addActionListener((ActionListener)this); 
            btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.RUTA_VISTA_IMAGENES+Constantes.IMG_HOME+Constantes.EXTENSION_PNG))); 
            contentPane.add(btnMenu); 
                
        } // Cierre componentes 

        @SuppressWarnings("unused") 
        @Override 
        public void actionPerformed(ActionEvent evento) { 
            
            if(evento.getSource()==btnConsultar){ 
        
                // Limpia la tabla 
                UtilidadesTabla.limpiarTabla(modelo); 
                    
            } 
            
            if(evento.getSource()==btnMenu){ 
                    
                  ControladorPrincipal controladorPrincipal = new ControladorPrincipal(); 
                  setVisible(false); 
            } 
                
        } // cierra actionPerformed 
        
        private String calcularAhorro(ArrayList<String> listaCompras, ArrayList<String> listaGastosFijos, ArrayList<String> listaIngresos) { 

            double ahorro = 0.0; 
            
            if(!UtilesValida.esNulo(listaIngresos)) { 
            	
            	if( listaIngresos.size() > 0 ) { 
            		
                    for(int i=0;i<listaIngresos.size();i++) { 
                        
                        String[] datosFile = listaIngresos.get(i).split(Constantes.PUNTO_COMA); 
                        ahorro = ahorro + Double.parseDouble(datosFile[3]); 

                    } 
            	}
                    
            } 
            
            if(!UtilesValida.esNulo(listaCompras)) { 
            	
            	if( listaCompras.size() > 0 ) { 
            		
                    for(int i=0;i<listaCompras.size();i++) { 
                        
                        String[] datosFile = listaCompras.get(i).split(Constantes.PUNTO_COMA); 
                        ahorro = ahorro - Double.parseDouble(datosFile[4]); 

                    } 
            	}
                   
            } 
            
            if(!UtilesValida.esNulo(listaGastosFijos)) { 
            	
            	if( listaGastosFijos.size() > 0 ) { 
            		
            		for(int i=0;i<listaGastosFijos.size();i++) { 
                        
                        String[] datosFile = listaGastosFijos.get(i).split(Constantes.PUNTO_COMA); 
                        ahorro = ahorro - Double.parseDouble(datosFile[7]); 
                    } 
            	}
                
            } 
            
            DecimalFormat df = new DecimalFormat("#.00"); 
            
            return df.format(ahorro); 
        } 

} // cierre de clase 

