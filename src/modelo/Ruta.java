package modelo; 

public class Ruta { 
        
    private String ruta; 
    private String filename; 
    private String extension; 
    
    public Ruta(){ 

    } 
    
    public Ruta(String ruta, String extension){ 
            
        this.ruta = ruta; 
        this.extension = extension;
    } 
    
    public Ruta(String ruta, String filename, String extension){ 
            
        this.ruta = ruta; 
        this.filename = filename; 
        this.extension = extension; 
    } 

    public String getRuta() { 
        return ruta; 
    } 
    public String getFilename() { 
        return filename; 
    } 
    
    public String getExtension() { 
        return extension; 
    } 

    public void setFilename(String filename) { 
        this.filename = filename; 
    } 

    public void setExtension(String extension) { 
        this.extension = extension; 
    } 

    public void setRuta(String ruta) { 
        this.ruta = ruta; 
    } 
    
    public String obtenerRuta(){ 
            
        return this.ruta+this.filename+this.extension;                 
    } 

}