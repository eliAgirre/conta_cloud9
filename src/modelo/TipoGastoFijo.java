package modelo;

public class TipoGastoFijo {

	// Atributos de la clase
	private int idGastoFijo;
	private String nombreGasto;
	private String periodo;
	
	public TipoGastoFijo() {}
	
	/**
	 * Constructor con parametros todos los atributos.
	 * 
	 * @param idTienda Es un identificador del gasto fijo.
	 * @param nombre Nombre del gasto.
	 * 
    */
	public TipoGastoFijo(int idGastoFijo, String nombreGasto) {

		this.idGastoFijo = idGastoFijo;
		this.nombreGasto = nombreGasto;
		
	}// Cierre constructor
	
	public TipoGastoFijo(int idGastoFijo, String nombreGasto, String periodo) {

		this.idGastoFijo = idGastoFijo;
		this.nombreGasto = nombreGasto;
		this.periodo = periodo;
		
	}// Cierre constructor

	// Getters
	public int getIdGastoFijo() {
		return idGastoFijo;
	}

	public String getNombreGasto() {
		return nombreGasto;
	}
	
	public String getPeriodo() {
		return periodo;
	}
	
	// Setters
	public void setIdGastoFijo(int idGastoFijo) {
		this.idGastoFijo = idGastoFijo;
	}

	public void setNombreGasto(String nombreGasto) {
		this.nombreGasto = nombreGasto;
	}
	
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	/**
	 * El nombre de la tienda aparece en el JComboBox.
	 * 
	 * @return nombre Devuelve el nombre de la tienda.
	 * 
    */
	@Override
	public String toString() {
		return this.nombreGasto;
	} // Cierre del metodo toString	

	
} // Cierre de la clase