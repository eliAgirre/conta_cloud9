package modelo;

public class TipoCompra {
	
	// Atributos de la clase
	private int idTipoCompra;
	private String tipoCompra;
	
	public TipoCompra(){}
	
	public TipoCompra(String string){}
	
	public TipoCompra(int idTipoCompra, String tipoCompra) {
		
		this.idTipoCompra = idTipoCompra;
		this.tipoCompra = tipoCompra;
	} // Cierre constructor

	// Getters
	public int getIdTipoCompra() {
		return idTipoCompra;
	}

	public String getTipoCompra() {
		return tipoCompra;
	}

	// Setters
	public void setIdTipoCompra(int idTipoCompra) {
		this.idTipoCompra = idTipoCompra;
	}

	public void setTipoCompra(String tipoCompra) {
		this.tipoCompra = tipoCompra;
	}

	/**
	 * El tipo de tienda aparece en el JComboBox.
	 * 
	 * @return tipoTienda Devuelve el tipo de compra en String.
	 * 
    */
	@Override
	public String toString() {
		return tipoCompra;
	} // Cierre toString
		
} // Cierre de clase