package modelo; 

import java.io.File; 

public class Directorio { 

        private String ruta; 
        private String nombreDir; 

        public Directorio(){} 

        public Directorio(String ruta, String nombreDir){ 
                
                this.ruta = ruta; 
                this.nombreDir = nombreDir; 
        } 

        public String getRuta() { 
                return ruta; 
        } 
        public String getNombreDir() { 
                return nombreDir; 
        } 

        public void setRuta(String ruta) { 
                this.ruta = ruta; 
        } 

        public void setNombreDir(String nombreDir) { 
                this.nombreDir = nombreDir; 
        } 

        public String obtenerDir(){ 
                
                return this.ruta+this.nombreDir;                 
        } 

        public File crearDir(String directorio){ 
                
                File dir = new File(directorio); 
                dir.mkdir(); 
                
                return dir; 
                
        } 

        public boolean existeDir(String directorio){ 
                
                boolean existe = true; 
                
                File dir = new File(directorio); 
                
                if( !dir.exists() ) 
                        existe = false; 
                
                return existe; 
                
        } 

}
