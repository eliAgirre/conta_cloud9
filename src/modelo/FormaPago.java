package modelo;

public class FormaPago {
	
	// Atributos de la clase
	private int idFormaPago;
	private String tipoPago;
	
	public FormaPago(){}
	
	public FormaPago(String s){}
	
	public FormaPago(int idFormaPago, String tipoPago) {
		
		this.idFormaPago = idFormaPago;
		this.tipoPago = tipoPago;
	} // Constructor

	// Getters
	public int getIdFormaPago() {
		return idFormaPago;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	// Setters
	public void setIdFormaPago(int idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	// Sirve para que apareca en JComboBox el tipo de pago.
	@Override
	public String toString() {
		
		return tipoPago;
	} // Cierre toString

} // Cierre clase