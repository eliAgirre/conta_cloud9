package modelo;

public class FormaCompra {
	
	// Atributos de la clase
	private int idFormaCompra;
	private String nombreFormaCompra;
	
	public FormaCompra(){}
	
	public FormaCompra(String s){}
	
	public FormaCompra(int idFormaCompra, String formaCompra) {
		
		this.idFormaCompra = idFormaCompra;
		this.nombreFormaCompra = formaCompra;
	} // Constructor

	// Getters
	public int getIdFormaCompra() {
		return idFormaCompra;
	}

	public String getNombreFormaCompra() {
		return nombreFormaCompra;
	}

	// Setters
	public void setIdFormaCompra(int idFormaCompra) {
		this.idFormaCompra = idFormaCompra;
	}

	public void setNombreFormaCompra(String formaCompra) {
		this.nombreFormaCompra = formaCompra;
	}

	// Sirve para que apareca en JComboBox el forma de compra.
	@Override
	public String toString() {
		
		return nombreFormaCompra;
	} // Cierre toString

} // Cierre clase