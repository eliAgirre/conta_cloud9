package modelo;

public class Compra {
	
	// Atributos de la clase
	private int idCompra;
	private String fechaCompra;
	private String tienda;
	private String producto;
	private double importe;
	private String formaCompra;
	private String formaPago;
	
	public Compra() {}
	
	public Compra(int idCompra, String fechaCompra, String tienda, String producto, 
				  double importe, String formaCompra, String formaPago) {

		this.idCompra = idCompra;
		this.fechaCompra = fechaCompra;
		this.tienda = tienda;
		this.producto = producto;
		this.importe = importe;
		this.formaCompra = formaCompra;
		this.formaPago = formaPago;
		
	} // Cierre constructor

	// Getters
	public int getIdCompra() {
		return idCompra;
	}
	
	public String getFechaCompra(){
		return fechaCompra;
	}

	public String getTienda() {
		return tienda;
	}

	public String getProducto() {
		return producto;
	}

	public double getImporte() {
		return importe;
	}
	
	public String getFormaCompra() {
		return formaCompra;
	}

	public String getFormaPago() {
		return formaPago;
	}

	// Setters
	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}
	
	public void setFechaCompra(String fechaCompra){
		this.fechaCompra = fechaCompra;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}
	
	public void setFormaCompra(String formaCompra) {
		this.formaCompra = formaCompra;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

} // Cierre de la clase