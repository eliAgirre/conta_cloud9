package modelo;

public class Tienda {

	// Atributos de la clase
	private int idTienda;
	private String nombre;
	
	public Tienda() {}
	
	/**
	 * Constructor con parametros todos los atributos.
	 * 
	 * @param idTienda Es un identificador de la tienda.
	 * @param nombre Nombre de la tienda.
	 * 
    */
	public Tienda(int idTienda, String nombre) {

		this.idTienda = idTienda;
		this.nombre = nombre;
	}// Cierre constructor

	// Getters
	public int getIdTienda() {
		return idTienda;
	}

	public String getNombre() {
		return nombre;
	}
	
	// Setters
	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * El nombre de la tienda aparece en el JComboBox.
	 * 
	 * @return nombre Devuelve el nombre de la tienda.
	 * 
    */
	@Override
	public String toString() {
		return this.nombre;
	} // Cierre del metodo toString	

	
} // Cierre de la clase