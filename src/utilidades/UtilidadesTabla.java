package utilidades;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class UtilidadesTabla {
	
	/**
	 * Limpia los datos de la tabla.
	 * 
	 * @param modelo Es de tipo DefaultTableModel a limpiar
	 * 
	 */
	public static void limpiarTabla(DefaultTableModel modelo) {
		
		modelo.setRowCount(0);	
	}
	
    /** 
     * Establece el tipo de columna del modelo. 
     * 
     * @param DefaultTableModel Es el modelo de tipo DefaultTableModel 
     * 
     */ 
    @SuppressWarnings("serial")
	public static DefaultTableModel tipoColumnasTablaComprasConcretas(DefaultTableModel modelo) { 
    
        modelo = new DefaultTableModel() { 

           public Class<?> getColumnClass(int column) { 
                        
                switch (column) { 
			        case 0: 
			            return Boolean.class; 
			        case 1: 
			            return Integer.class; 
			        case 2: 
			                return String.class; 
			        case 3: 
			                return String.class; 
			        case 4: 
			                return String.class; 
			        case 5: 
			                return String.class; 
			        case 6: 
			                return String.class; 
			        case 7: 
			                return Double.class; 
			        case 8: 
			                return String.class; 
			        case 9: 
			                return String.class; 
			        default: 
			        		return Boolean.class; 
                } 
            } 
        }; 
        
        return modelo;
    } 

	
	/**
	 * Estable la anchura de las columnas de la tabla Compras
	 * 
	 * @param tablaCompras Es la tabla de tipo JTable
	 * 
	 */
	public static void anchoColumnasTablaCompras(JTable tablaCompras) {
		
		// Se establece una anchura de las columnas
		tablaCompras.getColumn(Constantes.COMPRAS_COLUMNA_FECHA).setPreferredWidth(40); 
		tablaCompras.getColumn(Constantes.COMPRAS_COLUMNA_TIENDA).setPreferredWidth(40); 
		tablaCompras.getColumn(Constantes.COMPRAS_COLUMNA_PRODUCTO).setPreferredWidth(200);
		tablaCompras.getColumn(Constantes.COMPRAS_COLUMNA_IMPORTE).setPreferredWidth(20);
		tablaCompras.getColumn(Constantes.COMPRAS_COLUMNA_FORMA_COMPRA).setPreferredWidth(30);
		tablaCompras.getColumn(Constantes.COMPRAS_COLUMNA_PAGO).setPreferredWidth(20);
		
	}
	
    /** 
     * Estable la anchura de las columnas de la tabla Gastos Fijos 
     * 
     * @param tablaGastos Es la tabla de tipo JTable 
     * 
     */ 
    public static void anchoColumnasTablaGastos(JTable tablaGastos) { 
            
        // Se establece una anchura de las columnas 
    	ocultaColumnaID(tablaGastos);
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_FECHA_DOC).setPreferredWidth(20); 
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_NUM_DOC).setPreferredWidth(20); 
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_GASTO).setPreferredWidth(200); 
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_FECHA_INICIO).setPreferredWidth(20); 
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_FECHA_FIN).setPreferredWidth(20); 
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_DURACION).setPreferredWidth(20); 
        tablaGastos.getColumn(Constantes.COMPRAS_COLUMNA_IMPORTE).setPreferredWidth(20); 
            
    }
    
    /** 
     * Establece la anchura de las columnas de la tabla Compras Concretas 
     * 
     * @param comprasConcretas Es la tabla de tipo JTable 
     * 
     */ 
    public static void anchoColumnasTablaComprasConcretas(JTable comprasConcretas) { 
            
        ocultaColumnaID(comprasConcretas); 
        comprasConcretas.getColumn(Constantes.COMPRAS_COLUMNA_FECHA).setPreferredWidth(40); 
        ocultaColumna(comprasConcretas, Constantes.COMPRAS_COLUMNA_COMPRA);         
        comprasConcretas.getColumn(Constantes.COMPRAS_COLUMNA_TIENDA).setPreferredWidth(40); 
        comprasConcretas.getColumn(Constantes.COMPRAS_COLUMNA_PRODUCTO).setPreferredWidth(200); 
        comprasConcretas.getColumn(Constantes.COMPRAS_COLUMNA_IMPORTE).setPreferredWidth(20); 
        ocultaColumna(comprasConcretas, Constantes.COMPRAS_COLUMNA_FORMA_COMPRA); 
        comprasConcretas.getColumn(Constantes.COMPRAS_COLUMNA_PAGO).setPreferredWidth(30); 
            
    } 

    
    /** 
     * Oculta el campo ID de la tabla. 
     * 
     * @param tabla Es la tabla de tipo JTable 
     * 
     */ 
    public static void ocultaColumnaID(JTable tabla) {
    	tabla.getColumn(Constantes.COMPRAS_COLUMNA_ID).setMinWidth(0);
    	tabla.getColumn(Constantes.COMPRAS_COLUMNA_ID).setMaxWidth(0);
    	tabla.getColumn(Constantes.COMPRAS_COLUMNA_ID).setPreferredWidth(0); 
    }

    /** Oculta una columna de la tabla 
     * @param tabla Es la tabla de tipo JTable 
     * 
     */ 
    public static void ocultaColumna(JTable tabla, String nombreColumna) { 
            
            tabla.getColumn(nombreColumna).setMinWidth(0); 
            tabla.getColumn(nombreColumna).setMaxWidth(0); 
            tabla.getColumn(nombreColumna).setPreferredWidth(0); 
    }
    
    /** Se agregan varias columnas al modelo. 
     * @param modelo Es el modelo de tipo DefaultTableModel. 
     * 
     */ 
    public static DefaultTableModel agregarColumnasModelo(DefaultTableModel modelo) { 
            
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_SELECCIONAR); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_ID); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_FECHA); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_COMPRA); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_TIENDA); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_PRODUCTO); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_IMPORTE); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_FORMA_COMPRA); 
            modelo.addColumn(Constantes.COMPRAS_COLUMNA_PAGO); 

            return modelo; 
    } 

	
	public static DefaultTableModel mostrarTodosRegistrosTabla(ArrayList<String> listaRegistros, String[] datosFile, DefaultTableModel modelo) {
		
		if(!UtilesValida.esNulo(listaRegistros)) {
			for(int i=0;i<listaRegistros.size();i++) {
				datosFile = listaRegistros.get(i).split(Constantes.PUNTO_COMA);
				// Se agrega cada fila al modelo de tabla
				modelo.addRow(datosFile);
			}
		}
		
		return modelo;
	}
	
    public static DefaultTableModel mostrarTodosRegistrosTablaAhorro(ArrayList<String> listaCompras, ArrayList<String> listaGastosFijos, ArrayList<String> listaIngresos, String[] datosFile, DefaultTableModel modelo) { 
    
        String[] registroCompra = new String[5]; 
        String[] registroGastosFijos = new String[5]; 
        String[] registroIngresos = new String[5];
        DecimalFormat df = new DecimalFormat("#.00");
        
        if(!UtilesValida.esNulo(listaCompras)) { 
                
            for(int i=0;i<listaCompras.size();i++) { 
                
                datosFile       = listaCompras.get(i).split(Constantes.PUNTO_COMA); 
                String id       = datosFile[0]; 
                String fecha    = datosFile[1]; 
                String concepto = datosFile[2]; 
                Double gasto    = Double.parseDouble(datosFile[4]); 
                
                registroCompra[0] = id; 
                registroCompra[1] = fecha; 
                registroCompra[2] = concepto; 
                registroCompra[3] = Constantes.VACIO; 
                registroCompra[4] = df.format(gasto);
                
                modelo.addRow(registroCompra); 
            } 
        } 
        
        if(!UtilesValida.esNulo(listaGastosFijos)) { 
            
            for(int i=0;i<listaGastosFijos.size();i++) { 
                    
                datosFile       = listaGastosFijos.get(i).split(Constantes.PUNTO_COMA); 
                String id       = datosFile[0]; 
                String fecha    = datosFile[1]; 
                String concepto = datosFile[5]; 
                Double gasto    = Double.parseDouble(datosFile[7]); 
                
                registroGastosFijos[0] = id; 
                registroGastosFijos[1] = fecha; 
                registroGastosFijos[2] = concepto; 
                registroGastosFijos[3] = Constantes.VACIO; 
                registroGastosFijos[4] = df.format(gasto); 
                
                modelo.addRow(registroGastosFijos); 
            } 
        } 
        
        if(!UtilesValida.esNulo(listaIngresos)) { 
            
            for(int i=0;i<listaIngresos.size();i++) { 
                    
                datosFile       = listaIngresos.get(i).split(Constantes.PUNTO_COMA); 
                String id       = datosFile[0]; 
                String fecha    = datosFile[1]; 
                String concepto = datosFile[2]; 
                Double ingreso  = Double.parseDouble(datosFile[3]); 
                
                registroIngresos[0] = id; 
                registroIngresos[1] = fecha; 
                registroIngresos[2] = concepto; 
                registroIngresos[3] = df.format(ingreso); 
                registroIngresos[4] = Constantes.VACIO; 
                
                modelo.addRow(registroIngresos); 
            } 
        } 
        
        return modelo; 
	} 

	
    public static DefaultTableModel mostrarTodasListasTabla(ArrayList<String> listaRegistros, String[] datosFile, DefaultTableModel modelo) { 
        
        if(!UtilesValida.esNulo(listaRegistros)) { 
            for(int i=0;i<listaRegistros.size();i++) { 
                datosFile = listaRegistros.get(i).split(Constantes.COMA);
                if(!datosFile[0].equals(Constantes.DEFAULT_S_CERO)) 
                    modelo.addRow(datosFile); // Se agrega cada fila al modelo de tabla
            } 
        } 
        
        return modelo; 
    }


}
