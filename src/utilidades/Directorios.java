package utilidades; 

import modelo.Directorio; 

public class Directorios { 
        
	public static final String RUTA_DIR = ListaConstantesValidacion.RUTA_DIR[0]; // 0 - src 
	//public static final String RUTA_DIR = ListaConstantesValidacion.RUTA_DIR[1]; // 1- jar
	public static final String RUTA_RAIZ_DIR = ListaConstantesValidacion.RUTA_RAIZ_DIR[0]; // 0 - src
	//public static final String RUTA_RAIZ_DIR = ListaConstantesValidacion.RUTA_RAIZ_DIR[1]; // 1- jar
	
	public static boolean creadoListasDir() { 
	        
	    boolean dirCreado = true; 
	    
	    Directorio dir = new Directorio(); 
	    dir.setRuta(RUTA_DIR); 
	    dir.setNombreDir(Constantes.NOMBRE_DIR_LISTAS); 
	    String rutaDir = dir.obtenerDir(); 
	    dir.crearDir( rutaDir ); 
	    dirCreado = dir.existeDir(rutaDir);                 
	    
	    return dirCreado; 
	        
	} 
	
	public static boolean creadoRegistrosDir() { 
	        
	    boolean dirCreado = true; 
	    
	    Directorio dir = new Directorio(); 
	    dir.setRuta(RUTA_DIR); 
	    dir.setNombreDir(Constantes.NOMBRE_DIR_REGISTROS); 
	    String rutaDir = dir.obtenerDir(); 
	    dir.crearDir( rutaDir ); 
	    dirCreado = dir.existeDir(rutaDir);                 
	    
	    return dirCreado; 
	        
	} 
	
	public static boolean creadoLogsDir() { 
	        
	    boolean dirCreado = true; 
	    
	    Directorio dir = new Directorio(); 
	    dir.setRuta(RUTA_DIR); 
	    dir.setNombreDir(Constantes.NOMBRE_DIR_LOGS); 
	    String rutaDir = dir.obtenerDir(); 
	    dir.crearDir( rutaDir ); 
	    dirCreado = dir.existeDir(rutaDir);                 
	    
	    return dirCreado; 
	        
	}
	
	public static boolean dirRaizCreado(){ 
        
        boolean dirCreado = true; 
                
        Directorio dir = new Directorio(); 
        dir.setRuta(RUTA_RAIZ_DIR); 
        dir.setNombreDir(Constantes.NOMBRE_DIR_RAIZ); 
        String rutaDir = dir.obtenerDir(); 
        dir.crearDir( rutaDir ); 
        dirCreado = dir.existeDir(rutaDir);                 
                
        return dirCreado; 
	} 
	
	public static boolean existeDir(String directorio) {
		
		boolean existe = true;

		Directorio dir = new Directorio();
		dir.setRuta(RUTA_DIR);
		dir.setNombreDir(directorio);
		String rutaDir = dir.obtenerDir();
		existe = dir.existeDir(rutaDir);
		
		return existe;
	}
	
	public static boolean existeDirRaiz(String directorio) {
		
		boolean existe = true;

		Directorio dir = new Directorio();
		dir.setRuta(RUTA_RAIZ_DIR);
		dir.setNombreDir(directorio);
		String rutaDir = dir.obtenerDir();
		existe = dir.existeDir(rutaDir);
		
		return existe;
	}
	
	public static boolean esRaizJar() {
		
		boolean esRaizJar = false;
		
		if(RUTA_RAIZ_DIR.equals(ListaConstantesValidacion.RUTA_RAIZ_DIR[1])) 
			esRaizJar = true;		
		
		return esRaizJar;
	}
        
}
