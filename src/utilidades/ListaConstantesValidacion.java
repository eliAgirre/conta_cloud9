package utilidades;

public class ListaConstantesValidacion {
	
	/**
	 * Es el tipo de comunicacion. [0] ALTA, [1] EDITAR, [2] BAJA.
	 */
	public static final String[] TIPO_COMUNICACION 		= { "ALTA", "EDIT", "BAJA" };
	
	public static final String[] CABECERA_COMPRAS 		= {"ID", "Fecha", "Tienda", "Producto", "Importe", "Forma Compra", "Pago"};
    
    public static final String[] CABECERA_GASTOS_FIJOS	= {"ID", "Fecha doc", "Num doc",  "F. inicio", "F. Fin", "Gasto", "Duracion", "Importe"}; 
    
    public static final String[] CABECERA_INGRESOS      = {"ID", "Fecha", "Concepto", "Importe"};
    
    public static final String[] CABECERA_PRESUPUESTOS  = {"ID", "Fecha", "Tipo", "Importe"};
    
    public static final String[] CABECERA_AHORRO  		= {"ID",  "Fecha", "Concepto", "Ingreso", "Gasto"}; 

    public static final String[] LISTA_OPCIONES         = {"1 Fecha", "Entre fechas", "Tiendas"};
    
    public static final String[] CABECERA_COMPRAS_SELECT= {"ID", "Fecha", "Tienda", "Producto", "Importe", "Forma Compra", "Pago", "Seleccionar"};
    
    public static final String[] CABECERA_TIENDAS       = {"ID", "Tienda"}; 
    
    public static final String[] CABECERA_FORMAS_COMPRA = {"ID", "Forma de compra"}; 
    
    public static final String[] CABECERA_FORMAS_PAGO   = {"ID", "Forma de pago"};
    
    public static final String[] CABECERA_TIPOS_GATOS_FIJOS= {"ID", "Gasto fijo"};

    public static final String[] RUTA_LISTAS_TXT         = {Constantes.RUTA_LISTAS_ARCHIVOS_TXT, Constantes.RUTA_LISTAS_TXT}; 

    public static final String[] RUTA_FICHEROS_REGISTROS= {Constantes.RUTA_SRC_ARCHIVOS_TXT, Constantes.RUTA_CREACION_TXT};

    public static final String[] RUTA_FICHEROS_LOG      = {Constantes.RUTA_SRC_ARCHIVOS_LOG, Constantes.RUTA_CREACION_LOG}; 

    public static final String[] RUTA_DIR               = {Constantes.RUTA_SRC_DIR, Constantes.RUTA_JAR_DIR};
    
    public static final String[] RUTA_RAIZ_DIR          = {Constantes.RUTA_SRC_RAIZ, Constantes.RUTA_JAR_RAIZ}; 

}
