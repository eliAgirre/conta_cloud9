package utilidades; 

import java.io.BufferedInputStream;
import java.io.BufferedReader; 
import java.io.BufferedWriter; 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException; 
import java.io.FileReader; 
import java.io.FileWriter; 
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelo.Ruta; 

public class FicherosListas {

        public static final String RUTA_LISTAS = ListaConstantesValidacion.RUTA_LISTAS_TXT[0]; // 0 - src 
        //public static final String RUTA_LISTAS = ListaConstantesValidacion.RUTA_LISTAS_TXT[1]; // 1 - jar 
        public static Ruta RUTA = new Ruta(RUTA_LISTAS, Constantes.EXTENSION_TXT);
        
        public static boolean ficheroTxtCreado(String filename) {
                
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
              
            //Create the file 
            try { 
                if (file.createNewFile()){ 
                        return true; 
                        
                }else{ 
                        return false; 
                } 
            } catch (IOException e) { 
                System.out.println(e.getMessage()); 
                return false; 
            } 
        }
        
        public static boolean existeFichero(String filename) {
        	
        	boolean existe = true;
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
            
            if( !file.exists() )
            	existe = false;
            	
        	return existe;
        }
        
        /** 
         * Obtiene la ultima linea de registros. 
         *   
         * @return lastLine Devuelve la ultima linea. 
         * 
         */ 
        public static String getUltimaLineaRegistros(String modelo) { 
                
            RUTA.setFilename(modelo); 
            File file = new File( RUTA.obtenerRuta() );
            RandomAccessFile fileHandler = null; 
                                        
            try { 
                fileHandler = new RandomAccessFile( file, "r" ); 
                long fileLength = fileHandler.length() - 1; 
                StringBuilder sb = new StringBuilder(); 

                for(long filePointer = fileLength; filePointer != -1; filePointer--){ 
                    fileHandler.seek( filePointer ); 
                    int readByte = fileHandler.readByte(); 

                    if( readByte == 0xA ) { 
                        if( filePointer == fileLength ) { 
                            continue; 
                        } 
                        break; 

                    } else if( readByte == 0xD ) { 
                        if( filePointer == fileLength - 1 ) { 
                            continue; 
                        } 
                        break; 
                    } 

                    sb.append( ( char ) readByte ); 
                } 

                String lastLine = sb.reverse().toString(); 
                return lastLine; 
            } catch(FileNotFoundException fne) {
                return Constantes.AVISO_FICHERO_NO_EXISTE; 
            } catch( IOException ioe) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
                return null; 
            } finally { 
	            if (fileHandler != null ) 
	                try { 
	                    fileHandler.close(); 
	                } catch (IOException e) { 
	                    /* ignore */ 
	                } 
            } 
                
                        
        } // getUltimaLineaRegistros 
        
        public static void insertarListas(String id, String nombre, String fichero) { 
                
            try {
                RUTA.setFilename(fichero); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(), true);
                BufferedWriter bW=new BufferedWriter(ficheroW); 
                
                String registro=id+Constantes.COMA+nombre+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA; 

                bW.write(registro); 
                bW.close(); 
                    
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
        } 
        
        public static String[] obtenerLista(String modelo) throws FileNotFoundException, IOException { 
                
            String[] lista = new String[100]; 
            String[] registro = null; 
            int i=0; 
            File archivo = new File(Constantes.RUTA_LISTAS_ARCHIVOS_TXT+modelo+Constantes.EXTENSION_TXT);                 
            BufferedReader br = new BufferedReader(new FileReader(archivo)); 
            String texto; 
            texto = br.readLine(); 
            
            while( texto != null ){ 

                registro = texto.split(Constantes.ALMOHADILLA); 
                lista[i] = registro[0]; 
                texto = br.readLine(); 
                i++; 
            } 
            
            br.close(); 
            
            return lista; 
                
        }
        
        public static ArrayList<String> leerRegistros(String filename){ 
            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
            int numLineas = 0; 
            
            try { 
            	numLineas = FicherosListas.numLineas(filename); 
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
            
            String[] listaTemp = null; 
            String[] listaCampos = null; 
            ArrayList<String> listaRegistros = new ArrayList<String>(); 
            
            // Abre el fichero                 
            String texto="";                 
            FileReader ficheroR; 
            
            try {
                ficheroR = new FileReader(file); 
                BufferedReader bR=new BufferedReader(ficheroR); 
                texto=bR.readLine(); 
                
                // Mientras que el fichero contenga texto mostrara 
                while(texto!= null){
                        
                    if(numLineas!=0) 
                    	listaTemp = texto.split(Constantes.ALMOHADILLA); 
                    
                    texto=bR.readLine(); 
                    
                    if(!UtilesValida.esNulo(listaTemp)) { 
                        listaCampos = listaTemp[0].split(Constantes.COMA); 
                            
                        if(listaCampos.length > Constantes.INT_UNO) { 
                        	listaRegistros.add(listaTemp[0]); 
                        } 
                            
                    } 

                } // Cierre while 

                bR.close(); 
                    
            } catch (FileNotFoundException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
            
            return listaRegistros; 
        } 

    
	    public static int numLineas(String filename) throws IOException { 
	            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() );
	        InputStream is = new BufferedInputStream(new FileInputStream(file)); 
	        
	        try { 
	            byte[] c = new byte[1024]; 
	            int count = 0; 
	            int readChars = 0; 
	            boolean empty = true; 
	            while ((readChars = is.read(c)) != -1) { 
	                empty = false; 
	                for (int i = 0; i < readChars; ++i) { 
	                    if (c[i] == '\n') { 
	                        ++count; 
	                    } 
	                } 
	            } 
	            return (count == 0 && !empty) ? 1 : count; 
	        } finally { 
	            is.close(); 
	        } 
	    }
	    
        public static ArrayList<String> editarRegistroListas(String id, String nombre, String fichero) { 
            
            ArrayList<String> registros = leerRegistros(fichero); 
            String[] datosRegistro        = new String[2]; 
            String[] registroEdit   = new String[Constantes.INT_UNO]; 
            
            if(!UtilesValida.esNulo(registros)) { 
                    
                for(int i=0;i<registros.size();i++) { 
                        
                    boolean registroEditado = false; 
                    datosRegistro = registros.get(i).split(Constantes.COMA); 
                    
                    if(!registroEditado) { 
                            
                        if(datosRegistro[0].equals(id)) { 
                                
                            datosRegistro[1] = nombre; 
                            
                            registroEdit[0] = datosRegistro[0] + Constantes.COMA + datosRegistro[1]; 
                            
                            registroEditado = true; 
                                
                        } 
                    } 
                    
                    if(registroEditado) {
                        registros.set(i, registroEdit[0]); 
                    } 
                        
                } 
            } 
            
            return registros; 
        } 
    
	    public static boolean eliminarFicheroRegistroLista(String filename) { 
	            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() );
	        boolean borrado = false; 
	        
	        if(file.delete()) 
	        	borrado = true; 
	        else 
	        	borrado = false;
	        
	        return borrado; 
	    } 
    
	    public static void insertarRegistrosLista(ArrayList<String> listaRegistros, String filename) { 
	            
	        try { 
                RUTA.setFilename(filename); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(), true);
	            BufferedWriter bW=new BufferedWriter(ficheroW); 
	            String registro = null; 
	            
	            for(int i=0;i<listaRegistros.size();i++) { 
	                    
	                registro = listaRegistros.get(i); 
	                
	                if(!UtilesValida.esNulo(registro)) 
	                        bW.write(registro+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA); 
	            } 
	            
	            //bW.write(Constantes.HTML_SALTO_LINEA); 
	            bW.close(); 
	                
	        } catch (IOException e) { 
	                JOptionPane.showMessageDialog(null, e.getMessage()); 
	        } 
	    } 
    
		public static ArrayList<String> borrarRegistroLista(String id, String filename) { 
		    
		    ArrayList<String> registros = leerRegistros(filename); 
		    String[] datosRegistro        = new String[2]; 
		    String[] registroEdit   = new String[Constantes.INT_UNO]; 
		    
		    if(!UtilesValida.esNulo(registros)) { 
		            
		        for(int i=0;i<registros.size();i++) { 
		                
		            boolean registroEditado = false; 
		            datosRegistro = registros.get(i).split(Constantes.COMA); 
		            
		            if(!registroEditado) { 
		                    
		                if(datosRegistro[0].equals(id)) { 
		                    
		                        datosRegistro[0] = Constantes.VACIO; 
		                    datosRegistro[1] = Constantes.VACIO; 
		                    
		                    registroEdit[0] = datosRegistro[0] + Constantes.COMA + datosRegistro[1]; 
		                    
		                    registroEditado = true; 
		                        
		                } 
		            } 
		            
		            if(registroEditado) {
		                registros.set(i, registroEdit[0]); 
		            } 
		                
		        } 
		    } 
		    
		    return registros; 
	    } 

	    
        public static ArrayList<String> modificarRegistroEnLista(ArrayList<String> lista, String id) { 
            
            if(!UtilesValida.esNula(lista)) {
                            
                for(int i=0;i<lista.size();i++){
                        
                    String[] registro = lista.get(i).split(Constantes.COMA); 
                    String idRegistro = registro[0]; 
                    String nombre = registro[1]; 
                    
                    if(idRegistro.equals(String.valueOf(id))){ 
                        lista.set(i, idRegistro+Constantes.COMA+nombre); 
                    } 
                } 
            } 
        
            return lista; 
        }

}

