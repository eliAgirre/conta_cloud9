package utilidades;

import java.awt.Color;
import java.awt.Font;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import gestor.Compras;
import gestor.FormasCompra;
import gestor.FormasPago;
import gestor.Tiendas;
import gestor.TiposGastosFijos;
import modelo.Compra;
import modelo.FormaCompra;
import modelo.FormaPago;
import modelo.Tienda;
import modelo.TipoGastoFijo;

public class Utilidades {
	
	/**
	 * Obtiene la hora y fecha del sistema
	 * @return ts Devuelve un objeto de tipo Timestamp.
	 */
 	public static Timestamp obtenerHoraSistema(){
 		
 		// Se crea el objeto calendar y obtiene la hora del sistema
 		Calendar cal = Calendar.getInstance();
 		// Se convierte el objeto calendar a timestamp
 		Timestamp ts = new Timestamp(cal.getTimeInMillis()); 
 		// Devuelve fecha y hora en Timestamp 
 		return ts;
 		
 	} //Cierre obtenerHoraSistema
 	
	/**
	 * Obtiene la version de la aplicacion.
	 * @return lblVersion Devuelve una version de tipo JLabel.
	 */
	public static JLabel obtenerVersionLabel(int x, int y, int width, int height) {
		
		JLabel lblVersion = new JLabel(Constantes.LIT_VERSION+Constantes.PUNTOS+Constantes.ESPACIO+Constantes.VERSION);
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(x, y, width, height);
		return lblVersion;
	}
    
    /** Obtiene el label del aviso de informacion. 
    * @return lblAviso Devuelve una informacion de tipo JLabel. 
    */ 
    public static JLabel obtenerAvisoInfoLabel(int x, int y, int width, int height, String literal) { 

        JLabel lblAviso = new JLabel(literal); 
        lblAviso.setFont(new Font(Constantes.FUENTE_ARIAL, Font.BOLD, 12)); 
        lblAviso.setHorizontalAlignment(SwingConstants.LEFT); 
        lblAviso.setBounds(x, y, width, height); 
        return lblAviso; 
    } 
    
    /** Obtiene el label. 
    * @return label Devuelve el label. 
    */ 
    public static JLabel obtenerLabel(int x, int y, int width, int height, String literal) { 

        JLabel label = new JLabel(literal); 
        label.setForeground(new Color(0, 139, 139)); 
        label.setFont(new Font(Constantes.FUENTE_TAHOMA, Font.BOLD, 11)); 
        label.setBounds(x, y, width, height); 
        return label; 
    }

    /** Obtiene el boton. 
    * @return boton Devuelve el boton de tipo JButton. 
    */ 
    public static JButton obtenerBoton(int x, int y, int width, int height, String nombreBoton, JButton boton) { 
            
        boton = new JButton(nombreBoton);  
        boton.setBackground(new Color(184, 231, 255)); 
        boton.setBounds(x, y, width, height);
        return boton; 
    }
    
    /** Obtiene el combo. 
    * @return combo Devuelve el combo de tipo JComboBox. 
    */ 
    public static JComboBox<String> obtenerCombo(int x, int y, int width, int height, JComboBox<String> combo) { 
            
    	combo = new JComboBox<String>();  
    	//combo.setBackground(new Color(184, 231, 255)); 
    	combo.setBounds(x, y, width, height); 
        return combo; 
    }
    
    /** Obtiene el input de text. 
    * @return txtField Devuelve el input de tipo txtField. 
    */ 
    public static JTextField obtenerTextField(int x, int y, int width, int height, int columns, JTextField txtField, String texto, boolean editable) { 
            
    	txtField = new JTextField();  
    	txtField.setColumns(columns);
    	if(!UtilesValida.esNulo(texto)) { txtField.setText(texto); }
    	txtField.setEditable(editable);
    	txtField.setBounds(x, y, width, height); 
        return txtField; 
    }

   /** Obtiene la radio. 
   * @return radio Devuelve un radio de tipo JRadioButton. 
   */ 
   public static JRadioButton obtenerRadio(int x, int y, int width, int height, String nombreRadio, boolean selected, ButtonGroup buttonGroup, JRadioButton radio) { 
           
       radio = new JRadioButton(nombreRadio); 
       radio.setSelected(selected); 
       buttonGroup.add(radio); 
       radio.setBounds(x, y, width, height); 
       return radio; 
   } 

    
    /** Obtiene el panel de scroll a mostrar. 
    * @return scrollPane Devuelve el boton de todos de tipo JButton. 
    */ 
    public static JScrollPane obtenerScroll(int x, int y, int width, int height, JScrollPane scrollPane, boolean enabled) { 
            
        scrollPane = new JScrollPane(); 
        scrollPane.setViewport(null); 
        scrollPane.setViewportBorder(null);
        scrollPane.setEnabled(enabled); 
        scrollPane.setBounds(x, y, width, height); 
        return scrollPane; 
    }
    
    /** Obtiene la tabla a mostrar.
    * @return tabla Devuelve la tabla de tipo JTable. 
    */
    public static JTable obtenerTabla(JTable tabla, DefaultTableModel modelo, boolean enabled, JScrollPane scrollPane) {
    	
    	tabla = new JTable(modelo);
    	UtilidadesTabla.ocultaColumnaID(tabla);
        tabla.setEnabled(enabled);
        tabla.setBorder(null);
        scrollPane.setViewportView(tabla);
    	return tabla;
    }
    
    /** Obtiene el panel para mostrar los elementos de la ventana.
    * @return contentPane Devuelve el panel de tipo JPanel. 
    */
    public static JPanel obtenerPanel(int x, int y, int width, int height, JPanel contentPane) {
    	
    	contentPane = new JPanel();
    	contentPane.setBorder(new EmptyBorder(x, y, width, height)); 
        contentPane.setLayout(null);
    	return contentPane;
    	
    }
    
    public static JTextArea obtenerArea(JTextArea textArea, boolean editable, JScrollPane scrollPane) {
    	
    	textArea = new JTextArea();
    	textArea.setEditable(editable);
    	scrollPane.setViewportView(textArea);
    	textArea.setFont(new java.awt.Font(null, 0, 12));
    	return textArea;
    }
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idCompra Es el identificador de la compra.
	 * @param fechaCompra Fecha de la compra.
	 * @param tipoCompra Es el tipo de compra que se ha realizado, como comida, pescaderia, famarcia, etc.
	 * @param tienda Tienda efectuado la compra.
	 * @param importe El importe de la compra.
	 * @param formaCompra Puede ser forma Online o Fisica.
	 * @param formaPago Es la forma de pago en Efectivo, con Tarjeta o Pay Pal.
	 * @param producto Es el producto de la compra.
	 * 
	 */
	public static void escribirCabeceraRegistroCompras(int idCompra, String fechaCompra, String tienda, String importe,  String formaCompra, String formaPago, String producto) {
		
		utilidades.FicherosLog.cabeceraComprasLog();
		
		utilidades.FicherosLog.escribirComprasLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idCompra, fechaCompra, tienda, importe, formaCompra, formaPago, producto);
		
	}
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idGastoFijo Es el identificador del gasto fijo.
	 * @param fechaFactura Fecha de la factura del gasto.
	 * @param numFactura Es el numero de la factura del gasto.
	 * @param fechaInicio Es la fecha de inicio del consumo del gasto fijo.
	 * @param fechaFin Es la fecha fin del consumo del gasto fijo.
	 * @param tipoGasto Es el tipo de gasto como la luz, el gas, el agua, el telefono, etc.
	 * @param importe Es el importe del gasto fijo.
	 * 
	 */
	public static void escribirCabeceraRegistroGastosFijos(int idGastoFijo, String fechaFactura, String numFactura, String fechaInicio,  String fechaFin, String tipoGasto, String importe) {
		
		utilidades.FicherosLog.cabeceraGastoFijoLog();
		
		utilidades.FicherosLog.escribirGastoFijoLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idGastoFijo, fechaFactura, numFactura, fechaInicio, fechaFin, tipoGasto, importe);
		
	}
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idPresu Es el identificador del presupuesto.
	 * @param fechaPresu Fecha del presupuesto.
	 * @param tipo Es el tipo de presupuesto, Semanal o Mensual.
	 * @param presupuesto Es la cuantia del presupuesto.
	 * 
	 */
	public static void escribirCabeceraRegistroPresus(int idPresu, String fechaPresu, String tipo, String presupuesto) {
		
		utilidades.FicherosLog.cabeceraPresuLog();
		
		utilidades.FicherosLog.escribirPresuLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idPresu, fechaPresu, tipo, presupuesto);
		
	}
	
	/**
	 * Escribe la cabecera y un registro en el fichero log de Compras.
	 * 
	 * @param idGastoFijo Es el identificador del ingreso
	 * @param fechaIngreso Fecha del ingreso
	 * @param concepto Es el concepto del ingreso.
	 * @param importe Es el importe del ingreso.
	 * 
	 */
	public static void escribirCabeceraRegistroIngresos(int idIngreso, String fechaIngreso, String concepto, String importe) {
		
		utilidades.FicherosLog.cabeceraIngresoLog();
		
		utilidades.FicherosLog.escribirIngresoLog(ListaConstantesValidacion.TIPO_COMUNICACION[0], idIngreso, fechaIngreso, concepto, importe);
		
	}
	
	public static boolean compararUltimaLineaConCabeceraCompras(String ultimaLinea) {
		
		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_COMPRAS_LOG)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	public static boolean compararUltimaLineaConCabeceraGastosFijos(String ultimaLinea) {
		
		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_GASTOS_FIJOS_LOG)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	public static boolean compararUltimaLineaConCabeceraPresu(String ultimaLinea) {

		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_PRESUPUESTOS_LOG)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	public static boolean compararUltimaLineaConRaya(String ultimaLinea) {
		
		boolean sonIguales = false;
		
		if(ultimaLinea.equals(Constantes.CABECERA_RAYA)) {
			sonIguales = true;
		}
		return sonIguales;
		
	}
	
	/**
	 * Agrega los datos de la compra a la lista de la compra.
	 * 
	 * @param idCompra Es el identificador de la compra.
	 * @param fechaCompra Fecha de la compra.
	 * @param tipoCompra Es el tipo de compra que se ha realizado, como comida, pescaderia, famarcia, etc.
	 * @param tienda Tienda efectuado la compra.
	 * @param importe El importe de la compra en decimales.
	 * @param formaCompra Puede ser forma Online o Fisica.
	 * @param formaPago Es la forma de pago en Efectivo, con Tarjeta o Pay Pal.
	 * @param producto Es el producto de la compra.
	 * @param listaCompras Es la lista de las compras.
	 * 
	 */
	public static void agregarCompraALista(int idCompra, String fechaCompra, String tienda, double importe, String formaCompra, String formaPago, String producto, Compras listaCompras) {
		
		Compra compra = new Compra(idCompra, fechaCompra, tienda, producto, importe, formaCompra, formaPago);
		
		// Se agrega cada objeto a la lista
		listaCompras.anadir(compra);
		
	}
	
	/**
	 * Agrega los datos de la tienda a la lista de tiendas.
	 * 
	 * @param idTienda Es el identificador de la tienda.
	 * @param nombreTienda Nombre de la tienda.
	 * @param listaTiendas Es la lista de las tiendas.
	 * 
	 */
	public static void agregarTiendaALista(int idTienda, String nombreTienda, Tiendas listaTiendas) {
		
		Tienda tienda = new Tienda(idTienda, nombreTienda);
		
		// Se agrega cada objeto a la lista
		listaTiendas.anadir(tienda);
		
	}
	
	/** 
     * Agrega los datos de la forma de pago a la lista. 
     * 
     * @param idFormaPago Es el identificador de la forma de pago. 
     * @param tipoPago Es el tipo de pago. 
     * @param listaFormasPago Es la lista de las formas de pago. 
     * 
     */ 
    public static void agregarFormaPagoALista(int idFormaPago, String tipoPago, FormasPago listaFormasPago) { 
            
        FormaPago formaPago = new FormaPago(idFormaPago, tipoPago); 
        
        // Se agrega cada objeto a la lista 
        listaFormasPago.anadir(formaPago); 
            
    } 
    
    /** 
     * Agrega los datos de la forma de compra a la lista. 
     * 
     * @param idFormaCompra Es el identificador de la forma de compra. 
     * @param nombreFormaCompra Es el nombre de la forma de compra. 
     * @param listaFormasCompra Es la lista de las formas de compra. 
     * 
     */ 
    public static void agregarFormaCompraALista(int idFormaCompra, String nombreFormaCompra, FormasCompra listaFormasCompra) { 
            
        FormaCompra formaCompra = new FormaCompra(idFormaCompra, nombreFormaCompra); 
        
        // Se agrega cada objeto a la lista 
        listaFormasCompra.anadir(formaCompra); 
            
    }
    
	/**
	 * Agrega los datos del gasto fijo a la lista.
	 * 
	 * @param idGastoFijo Es el identificador del gasto fijo.
	 * @param nombreGasto Nombre del gasto.
	 * @param listaGastosFijos Es la lista de los gastos fijos.
	 * 
	 */
	public static void agregarGastoFijoALista(int idGastoFijo, String nombreGasto, TiposGastosFijos listaGastosFijos) {
		
		TipoGastoFijo gastoFijo = new TipoGastoFijo(idGastoFijo, nombreGasto);
		
		// Se agrega cada objeto a la lista
		listaGastosFijos.anadir(gastoFijo);
		
	}
	
	/**
	 * Obtiene los datos de la lista para cargar el JComboBox a mostrar.
	 * 
	 * @param lista Es la lista en String que viene cargadas con los nombres de la tienda.
	 * @param comboBox Es el JComboBox a cargar segun la lista
	 * @return comboBox Devuelve un JComboBox de String para mostrar en la vista. Si la lista viene vacia sera nulo.
	 * 
	 */
	public static JComboBox<String> getCombo(ArrayList<String> lista, JComboBox<String> comboBox) {
		
		if(!utilidades.UtilesValida.esNula(lista)){
			for(int i=0;i<lista.size();i++) {
				comboBox.addItem(lista.get(i));
			}
		}
		else {
			comboBox = null;
		}
		
		return comboBox;
	}
	
	/**
	 * Obtiene los datos de la lista para cargar el JComboBox a mostrar.
	 * 
	 * @param lista Es la lista en String que viene cargadas con los nombres de la tienda.
	 * @param comboBox Es el JComboBox a cargar segun la lista
	 * @param nombreItem Es el nombre de item a buscar en la lista a devolver.
	 * @return comboBox Devuelve un JComboBox de String para mostrar en la vista. Si la lista viene vacia sera nulo.
	 * 
	 */
	public static JComboBox<String> getItem(ArrayList<String> lista, JComboBox<String> comboBox, String nombreItem) {
		
		if(!utilidades.UtilesValida.esNula(lista)){
			for(int i=0;i<lista.size();i++) {
				
				if(lista.get(i).equals(nombreItem)) {
					comboBox.setSelectedItem(nombreItem);
				}
			}
		}
		else {
			comboBox = null;
		}
		
		return comboBox;
	}
	
	/**
	 * Parsea un String a una fecha de tipo Date.
	 * 
	 * @param cadena Es la fecha a formatear o a parsear.
	 * @return fecha Devuelve una fecha de tipo date si no sea ha dado ParseException.
	 * 
	 */
	public static Date parseaFecha(String cadena) {
    	
    	Date fecha = null;
         
		try {
			 SimpleDateFormat sdf = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO);
		     fecha = sdf.parse(cadena);
		     return fecha;
		} catch (ParseException e) {
		     return fecha;
		}
    }

     /** 
     * Función que nos valida si la fecha que se le pasa es mayor a la fecha actual.           
     * @param java.sql.Date.fechaValidar fecha a validar. 
     * @return true si lo es y false en el caso contrario o en el caso de que la fecha sea nula. 
     **/ 
     public static boolean esFechaMayorqueHoy(Date fechaValidar){ 
              
         if(!UtilesValida.esNulo(fechaValidar) && fechaValidar.after(new Date(new GregorianCalendar().getTimeInMillis()))){ 
             return true;            
         } 
         return false;                 
     } 

     /** 
      * Función que nos valida si la fecha que se le pasa es menor a la fecha actual.           
      * @param java.sql.Date.fechaValidar fecha a validar. 
      * @return true si lo es y false en el caso contrario o en el caso de que la fecha sea nula. 
     **/ 
     public static boolean esFechaMenorqueHoy(Date fechaValidar){ 
              
         if(!UtilesValida.esNulo(fechaValidar) 
             && !new Date(new GregorianCalendar().getTimeInMillis()).toString().equals(fechaValidar.toString()) 
             &&  fechaValidar.before(new Date(new GregorianCalendar().getTimeInMillis()))){ 
              
             return true;                         
         } 
         return false;                 
     } 
      
    /** 
     * Función que nos valida si la fecha que se le pasa sea del mismo ejercicio.           
     * @param java.sql.Date.fechaValidar fecha a validar. 
     * @return false si lo es y true en el caso contrario o en el caso de que la fecha sea nula. 
    **/ 
    public static boolean esEjercicioActual(Date fechaValidar){ 
        
        if(!UtilesValida.esNulo(fechaValidar) 
            && Integer.valueOf(fechaValidar.toString().substring(0, 4))!=new GregorianCalendar().get(Calendar.YEAR)){ 
            
            return false; 
        } 
        return true; 
    } 

	
    /** 
     * Obtiene los datos del archivo TXT. 
     * 
     * @param modelo Es el modelo asociado a la lista para obtener los datos del archivo TXT especifico. 
     * @param listaString Es una lista de tipo String para cargar los datos del TXT. 
     * @return listaString Devuelve una lista cargada de datos, sino es que no se puede leer el archivo o no se puede parsear el TXT. 
     * 
     */ 
     public static ArrayList<String> getLista(String modelo) throws FileNotFoundException, IOException { 
            
         ArrayList<String> listaString = new ArrayList<String>(); 
         String[] lista = null;                 
                          
         lista = FicherosListas.obtenerLista(modelo); 
          
         if( !UtilesValida.esNulo( lista )){ 
                  
                 String[] registros = null; 
                  
                 for(int i=0;i<lista.length;i++){ 
                          
                         if( !UtilesValida.esNulo(lista[i])){ 
                                 registros = lista[i].split(Constantes.COMA); 
                                 listaString.add(registros[1]); 
                         } 
                          
                 } 
         } 
          
         return listaString; 
     } 

}
