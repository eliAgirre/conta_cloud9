package utilidades;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Timestamp;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import modelo.Ruta;

public class FicherosLog {
	
	static JTextArea textArea = null;
	public static final String RUTA_lOG = ListaConstantesValidacion.RUTA_FICHEROS_LOG[0]; // 0 - src 
    //public static final String RUTA_lOG = ListaConstantesValidacion.RUTA_FICHEROS_LOG[1]; // 1 - jar
	public static Ruta RUTA = new Ruta(RUTA_lOG, Constantes.EXTENSION_LOG);
	
	public static String FICHERO_COMPRAS 		= Constantes.FICHERO_COMPRAS;
	public static String FICHERO_GASTOS_FIJOS 	= Constantes.FICHERO_GASTOS_FIJOS;
	public static String FICHERO_PRESUPUESTOS 	= Constantes.FICHERO_PRESUPUESTOS;
	public static String FICHERO_INGRESOS	 	= Constantes.FICHERO_INGRESOS;

	
	public static boolean ficheroLogCreado(String filename) {
		
        RUTA.setFilename(filename); 
        File file = new File( RUTA.obtenerRuta() );
		  
		//Create the file
		try {
			if (file.createNewFile()){
				return true;
				
			}else{
				return false;
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return false;
		}

	}
	
    public static boolean existeFichero(String filename) {
    	
    	boolean existe = true;
        RUTA.setFilename(filename); 
        File file = new File( RUTA.obtenerRuta() ); 
        
        if( !file.exists() )
        	existe = false;
        	
    	return existe;
    }
	
	public static void cabeceraComprasLog() {
		
		try {
            RUTA.setFilename(FICHERO_COMPRAS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			String cabecera=Constantes.CABECERA_COMPRAS_LOG+Constantes.HTML_SALTO_LINEA+Constantes.CABECERA_RAYA+Constantes.HTML_SALTO_LINEA;

			bW.write(cabecera);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	/**
	 * Escribe y edita el fichero log de Compras.
	 *  
	 * @param evento Es el tipo de comunicacion. Alta, Editar o Baja.
	 * @param idCompra Es el identificador de la compra.
	 * @param fechaCompra Fecha de la compra.
	 * @param tipoCompra Es el tipo de compra que se ha realizado, como comida, pescaderia, famarcia, etc.
	 * @param tienda Tienda efectuado la compra.
	 * @param importe El importe de la compra.
	 * @param formaCompra Puede ser forma Online o Fisica.
	 * @param formaPago Es la forma de pago en Efectivo, con Tarjeta o Pay Pal.
	 * @param producto Es el producto de la compra.
	 * 
	 */
	public static void escribirComprasLog(String evento, int idCompra, String fechaCompra, String tienda, String importe,  String formaCompra, String formaPago, String producto){
		
		try {
			
            RUTA.setFilename(FICHERO_COMPRAS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora= Utilidades.obtenerHoraSistema();
			
			String datos=Constantes.HTML_SALTO_LINEA+evento+"   "+hora +"   "+Integer.toString(idCompra)+"   "+fechaCompra+"          "+tienda+"      "+importe+"          "+formaCompra+"          "+formaPago+"      "+producto+Constantes.HTML_SALTO_LINEA;

			bW.write(datos);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL);
		}
		
	} //Cierre escribirComprasLog
	
	public static void cabeceraGastoFijoLog() {
		
		try {
            RUTA.setFilename(FICHERO_GASTOS_FIJOS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			String cabecera=Constantes.CABECERA_GASTOS_FIJOS_LOG+Constantes.HTML_SALTO_LINEA+Constantes.CABECERA_RAYA+Constantes.HTML_SALTO_LINEA;

			bW.write(cabecera);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	/**
	 * Escribe y edita el fichero log de Gastos fijo.
	 *  
	 * @param evento Es el tipo de comunicacion. Alta, Editar o Baja.
	 * @param idGastoFijo Es el identificador del gasto fijo.
	 * @param fechaFactura Fecha de la factura del gasto.
	 * @param numFactura Es el numero de la factura del gasto.
	 * @param fechaInicio Es la fecha de inicio del consumo del gasto fijo.
	 * @param fechaFin Es la fecha fin del consumo del gasto fijo.
	 * @param tipoGasto Es el tipo de gasto como la luz, el gas, el agua, el telefono, etc.
	 * @param importe Es el importe del gasto fijo.
	 * 
	 */
	
	public static void escribirGastoFijoLog(String evento, int idGastoFijo, String fechaFactura, String numFactura, String fechaInicio,  String fechaFin, String tipoGasto, String importe){
		
		try {
            RUTA.setFilename(FICHERO_GASTOS_FIJOS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora= Utilidades.obtenerHoraSistema();
			
			String datos=Constantes.HTML_SALTO_LINEA+evento+"   "+hora +"   "+Integer.toString(idGastoFijo)+"   "+fechaFactura+"          "+numFactura+"      "+fechaInicio+"          "+fechaFin+"          "+tipoGasto+"      "+importe+Constantes.HTML_SALTO_LINEA;

			bW.write(datos);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL);
		}
		
	} //Cierre escribirGastoFijoLog
	
	public static void cabeceraPresuLog() {
		
		try {
            RUTA.setFilename(FICHERO_PRESUPUESTOS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			String cabecera=Constantes.CABECERA_PRESUPUESTOS_LOG+Constantes.HTML_SALTO_LINEA+Constantes.CABECERA_RAYA+Constantes.HTML_SALTO_LINEA;

			bW.write(cabecera);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	/**
	 * Escribe y edita el fichero log de Gastos fijo.
	 *  
	 * @param evento Es el tipo de comunicacion. Alta, Editar o Baja.
	 * @param idPresu Es el identificador del gasto fijo.
	 * @param fechaPresu Fecha de la factura del gasto.
	 * @param tipo Es el numero de la factura del gasto.
	 * @param importe Es la fecha de inicio del consumo del gasto fijo.
	 * 
	 */
	
	public static void escribirPresuLog(String evento, int idPresu, String fechaPresu, String tipo, String importe){
		
		try {
            RUTA.setFilename(FICHERO_PRESUPUESTOS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora= Utilidades.obtenerHoraSistema();
			
			String datos=Constantes.HTML_SALTO_LINEA+evento+"   "+hora +"   "+Integer.toString(idPresu)+"   "+fechaPresu+"          "+tipo+"      "+importe+Constantes.HTML_SALTO_LINEA;

			bW.write(datos);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL);
		}
		
	} //Cierre escribirPresuLog
	
	
	public static void cabeceraIngresoLog() {
		
		try {
            RUTA.setFilename(FICHERO_INGRESOS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			String cabecera=Constantes.CABECERA_INGRESO_LOG+Constantes.HTML_SALTO_LINEA+Constantes.CABECERA_RAYA+Constantes.HTML_SALTO_LINEA;

			bW.write(cabecera);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	/**
	 * Escribe y edita el fichero log de Ingreso Log.
	 *  
	 * @param evento Es el tipo de comunicacion. Alta, Editar o Baja.
	 * @param idIngreso Es el identificador del ingreso.
	 * @param fechaIngreso Fecha del ingreso.
	 * @param concepto Es el concepto del ingreso.
	 * @param importe Es el importe del ingreso.
	 * 
	 */
	
	public static void escribirIngresoLog(String evento, int idIngreso, String fechaIngreso, String concepto, String importe){
		
		try {
            RUTA.setFilename(FICHERO_INGRESOS); 
            FileWriter ficheroW=new FileWriter( RUTA.obtenerRuta(), true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora= Utilidades.obtenerHoraSistema();
			
			String datos=Constantes.HTML_SALTO_LINEA+evento+"   "+hora +"   "+Integer.toString(idIngreso)+"   "+fechaIngreso+"          "+concepto+"      "+importe+Constantes.HTML_SALTO_LINEA;

			bW.write(datos);
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL);
		}
		
	} //Cierre escribirIngresoLog
	
	/**
	 * Lee ficheros log y muestra en el JTextArea el contenido.
	 *  
	 * @param filename Es el nombre del fichero de tipo String.
	 * @return buffer Es la area de tipo String para mostrar el contenido del fichero.
	 * 
	 */
	public static String leerFicheroLog(String filename) throws IOException{
		
		// Obtiene el fichero gastos.log
        RUTA.setFilename(filename); 
        File file = new File( RUTA.obtenerRuta() );
		
		// Abre el fichero		
		String texto="";		
		FileReader ficheroR=new FileReader(file);
		BufferedReader bR=new BufferedReader(ficheroR);
		StringBuffer buffer=new StringBuffer();
		texto=bR.readLine();
		
		// Mientras que el fichero contenga texto mostrara
		while(texto!= null){
			
			buffer.append(texto+ Constantes.HTML_SALTO_LINEA); // Se agrega una linea de salto
			texto=bR.readLine();

		} // Cierre while

		bR.close();

		return buffer.toString();
			
	} // leerFicheroLog
	
    static String getKey(String line){
        return line.split(Constantes.PUNTO_COMA)[0];
    }
	
	/**
	 * Obtiene la ultima linea del log.
	 *  
	 * @return lastLine Devuelve la ultima linea.
	 * 
	 */
	public static String getUltimaLineaLog(String modelo) {
		
		// Obtiene el fichero
        RUTA.setFilename(modelo); 
        File file = new File( RUTA.obtenerRuta() );
		RandomAccessFile fileHandler = null;
					
	    try {
	        fileHandler = new RandomAccessFile( file, "r" );
	        long fileLength = fileHandler.length() - 1;
	        StringBuilder sb = new StringBuilder();

	        for(long filePointer = fileLength; filePointer != -1; filePointer--){
	            fileHandler.seek( filePointer );
	            int readByte = fileHandler.readByte();

	            if( readByte == 0xA ) {
	                if( filePointer == fileLength ) {
	                    continue;
	                }
	                break;

	            } else if( readByte == 0xD ) {
	                if( filePointer == fileLength - 1 ) {
	                    continue;
	                }
	                break;
	            }

	            sb.append( ( char ) readByte );
	        }

	        String lastLine = sb.reverse().toString();
	        return lastLine;
	    } catch(FileNotFoundException fne) {
	        return Constantes.AVISO_FICHERO_NO_EXISTE;
	    } catch( IOException ioe) {
	    	JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL);
	        return null;
	    } finally {
	        if (fileHandler != null )
	            try {
	                fileHandler.close();
	            } catch (IOException e) {
	                /* ignore */
	            }
	    }
		
			
	} // leerFicheroLog
	
}
