package utilidades; 

/*import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.Document; 
import com.itextpdf.text.DocumentException; 
import com.itextpdf.text.Element; 
import com.itextpdf.text.Paragraph; 
import com.itextpdf.text.Phrase; 
import com.itextpdf.text.Rectangle; 
import com.itextpdf.text.pdf.PdfPCell; 
import com.itextpdf.text.pdf.PdfPTable; 
import com.itextpdf.text.pdf.PdfWriter; 
import com.itextpdf.text.Font; 

import modelo.Ahorro;
import modelo.Compra;

public class UtilPDF { 
        
    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD); 
    private static Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD); 
    private static Font cellHeaderFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
    
    public static Document crearFicheroPDF(String filename) throws FileNotFoundException, DocumentException { 
            
        Document documento = new Document(); 

        // Constantes.RUTA_SRC_ARCHIVOS_PDF+filename+Constantes.EXTENSION_PDF
        PdfWriter.getInstance(documento, new FileOutputStream(Constantes.RUTA_SRC_ARCHIVOS_PDF+filename+Constantes.EXTENSION_PDF)); 
        
        return documento; 

    } // crearFicheroPDF
        
    public static void agregarTitulo(Document documento, String titulo) throws DocumentException{
        
        Paragraph parrafo = new Paragraph(); // Se crea un parrafo 
        
        agregarSaltoLinea(parrafo,1); // Se agrega 1 linea vacia 
        
        PdfPTable tabla = new PdfPTable(1); // Se crea una tabla de una celda 
        
        PdfPCell celda = new PdfPCell(new Phrase(titulo, titleFont)); // Se crea una celda con el titulo 
        
        celda.setHorizontalAlignment(Element.ALIGN_CENTER); // Se escribe en el centro 
        
        celda.setBorder(Rectangle.NO_BORDER); // No tiene borde 
        
        tabla.addCell(celda); // Se agrega la celda a la tabla 
        
        documento.add(tabla); // Se agrega la tabla al documento 
        
        agregarSaltoLinea(parrafo,1); // Se agrega 1 linea vacia 
        
        documento.add(parrafo); // Se agregan los parrafos al documento 
        
    } // agregarTitulo 
    
    public static void agregarTablaContenidoAhorro(Document documento, String[] header, ArrayList<Ahorro> datos, int numColumnas) throws DocumentException{
            
            PdfPTable tabla = new PdfPTable(numColumnas); // Se crea una tabla con varias columnas 
        
        for(int i=0;i<header.length;i++){ 
            
            PdfPCell cellHead = new PdfPCell(new Phrase(header[i],headerFont)); // Se crea la celda cabecera 
            
            cellHead.setHorizontalAlignment(Element.ALIGN_CENTER); 
            
            tabla.addCell(cellHead); // Se agrega la celda a la tabla 
        } 
        
        // Se agrega la cabecera a la tabla 
        tabla.setHeaderRows(1); 
        
        // Se agraga cada celda con dato a la tabla 
        for(int i=0;i<datos.size();i++){ 
    
            tabla.addCell(datos.get(i).getFechaInicio()); // fecha_inicio 
            tabla.addCell(datos.get(i).getFechaFin()); // fecha_fin 
            tabla.addCell(Double.toString(datos.get(i).getPresu())); // presu 
            tabla.addCell(Double.toString(datos.get(i).getGasto())); // gasto 
            tabla.addCell(Double.toString(datos.get(i).getAhorro())); // ahorro 
        } 
            
        documento.add(tabla); // Se agrega la tabla al documento 
        
    } // agregarTablaContenidoAhorro 
    
    public static void agregarTablaContenidoCompra(Document documento, String[] header, ArrayList<Compra> datos, int numColumnas) throws DocumentException{ 
        
        // Se crea una tabla de 9 columnas 
        PdfPTable tabla=new PdfPTable(9); 
        // 100% width of columns table 
        //tabla.setWidthPercentage(100); 
        tabla.setTotalWidth(documento.getPageSize().getWidth() - 60); 
        tabla.setLockedWidth(true); 
    
	    for(int i=0;i<header.length;i++){ 
	            // Se crea la celda cabecera 
	            PdfPCell cellHead = new PdfPCell(new Phrase(header[i],cellHeaderFont)); 
	            // Se escribe en el centro 
	            cellHead.setHorizontalAlignment(Element.ALIGN_CENTER); 
	            // Se agrega la celda a la tabla 
	            tabla.addCell(cellHead); 
	    } 
	    
	    // Se agrega la cabecera a la tabla 
	    tabla.setHeaderRows(1); 
	    
	    // Se agragan cada celda con algun dato a la tabla 
	    for(int i=0;i<datos.size();i++){ 
	
	            tabla.addCell(Integer.toString(datos.get(i).getIdCompra())); // ID 
	            tabla.addCell(datos.get(i).getFechaCompra()); // fecha 
	            tabla.addCell(datos.get(i).getTienda()); // tienda 
	            tabla.addCell(datos.get(i).getProducto()); // producto 
	            tabla.addCell(Double.toString(datos.get(i).getImporte())); // importe
	            tabla.addCell(datos.get(i).getFormaCompra()); // forma compra 
	            tabla.addCell(datos.get(i).getFormaPago()); // forma pago 
	    } 
	    
	        // Se agrega la tabla al documento 
	         documento.add(tabla); 
	        
	} // agregarTablaContenidoCompra 

        
    private static void agregarSaltoLinea(Paragraph paragraph, int number) { 
            
        for (int i=0;i<number;i++) { 
                    
                paragraph.add(new Paragraph(Constantes.VACIO)); 
        } 
    } // agregarSaltoLinea 

} // cierre clase*/
