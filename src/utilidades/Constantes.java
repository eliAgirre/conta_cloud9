package utilidades;

public class Constantes {

	// Version de la app
	public static final String VERSION = "6.7";
	
	// Titulos de las vistas
	public static final String VISTA_CONTABILIDAD 		= "Contabilidad";
	public static final String VISTA_ANADIR_COMPRA 		= "Agregar compra";
	public static final String VISTA_ANADIR_TIENDA      = "Agregar tienda";
	public static final String VISTA_ANADIR_FORMA_COMPRA= "Agregar forma de compra";
	public static final String VISTA_ANADIR_FORMA_PAGO	= "Agregar forma de pago";
	public static final String VISTA_ANADIR_LISTA_GASTO_FIJO = "Agregar gasto fijo en  lista";
	public static final String VISTA_LOG_COMPRA 		= "Fichero de compras";
	public static final String VISTA_LOG_GASTOS_FIJOS   = "Fichero de gastos fijos";
	public static final String VISTA_COMPRAS_A_EDITAR 	= "Compras a editar";
	public static final String VISTA_EDITAR_COMPRA		= "Editar compra";
	public static final String VISTA_EDITAR_TIENDA		= "Editar tienda";
	public static final String VISTA_EDITAR_FORMA_COMPRA= "Editar forma de compra";
	public static final String VISTA_EDITAR_FORMA_PAGO	= "Editar forma de pago";
	public static final String VISTA_EDITAR_TIPO_GASTO_FIJO	= "Editar tipo gasto fijo";
	public static final String VISTA_BUSCAR_PRODUCTO	= "Buscar producto";
	public static final String VISTA_ANADIR_GASTO_FIJO	= "Agregar gasto fijo";
	public static final String VISTA_ANADIR_PRESUPUESTO	= "Agregar presupuesto";
    public static final String VISTA_EDITAR_PRESUPUESTO = "Editar presupuesto";
    public static final String VISTA_ANADIR_INGRESO     = "Agregar ingreso"; 
    public static final String VISTA_EDITAR_INGRESO     = "Editar ingreso"; 
    public static final String VISTA_GASTOS_A_EDITAR    = "Gastos a editar"; 
    public static final String VISTA_INBRESOS_A_EDITAR  = "Ingresos a editar";
    public static final String VISTA_PRESUS_A_EDITAR  	= "Presupuestos a editar";
    public static final String VISTA_TIENDAS_A_EDITAR  	= "Tiendas a editar";
    public static final String VISTA_FORMAS_COMPRA_A_EDITAR= "Formas de compra a editar";
    public static final String VISTA_FORMAS_PAGO_A_EDITAR= "Formas de pago a editar";
    public static final String VISTA_GASTOS_LISTA_A_EDITAR = "Tipo gastos fijos a editar";
    public static final String VISTA_EDITAR_GASTO_FIJO	= "Editar gasto fijo";
    public static final String VISTA_VER_GASTOS_FIJOS	= "Ver gastos fijos";
    public static final String VISTA_VER_INGRESOS		= "Ver ingresos";
    public static final String VISTA_VER_PRESUPUESTOS   = "Ver presupuestos";
    public static final String VISTA_VER_COMPRAS_DIARIAS= "Ver compras diarias";
    public static final String VISTA_VER_TIENDAS  		= "Ver las tiendas";
    public static final String VISTA_VER_FORMAS_COMPRA  = "Ver las formas de compra"; 
    public static final String VISTA_VER_FORMAS_PAGO    = "Ver las formas de pago";
    public static final String VISTA_VER_TIPOS_GASTOS_FIJOS= "Ver tipos gastos fijos";
    public static final String VISTA_AHORRO_SEMANAL		= "Ahorro semanal";
    public static final String VISTA_COMPRAS_CONCRETAS	= "Compras concretas";
    public static final String VISTA_AHORRO_MENSUAL		= "Ahorro mensual"; 
	
	// Rutas
	public static final String RUTA_IMAGENES 			= "Img/";
	public static final String RUTA_VISTA_IMAGENES 		= "/vista/Img/";
	public static final String RUTA_ARCHIVOS_LOG		= "archivos/log/";
	public static final String RUTA_ARCHIVOS_TXT		= "archivos/log/";
	public static final String RUTA_SRC_ARCHIVOS_LOG	= "src/archivos/log/";
	public static final String RUTA_SRC_ARCHIVOS_TXT	= "src/archivos/registros/";
	public static final String RUTA_LISTAS_ARCHIVOS_TXT = "src/archivos/listas/";
	public static final String RUTA_SRC_ARCHIVOS_PDF	= "src/archivos/pdf/";
	public static final String RUTA_CREACION_LOG		= "H://workspace//gastosNoBD//src//archivos//log//";
	public static final String RUTA_CREACION_TXT		= "H://workspace//gastosNoBD//src//archivos//registros//";
	public static final String RUTA_LISTAS_TXT			= "H://workspace//gastosNoBD//src//archivos//listas//";
	public static final String RUTA_ARCHIVOS	 		= "src/archivos/";
	public static final String RUTA_ARCHIVO_JSON 		= "archivos/json/";
	public static final String RUTA_SRC_ARCHIVO_JSON 	= "src/archivos/json/";
	public static final String RUTA_SRC_DIR             = "src/archivos/"; 
    public static final String RUTA_JAR_DIR             = "Z://contabilidad//";
    public static final String RUTA_JAR_RAIZ            = "Z://";
    public static final String RUTA_SRC_RAIZ            = "src/";
    
    // Nombres de los directorios
    public static final String NOMBRE_DIR_LISTAS	= "listas";
    public static final String NOMBRE_DIR_REGISTROS	= "registros";
    public static final String NOMBRE_DIR_LOGS		= "log";
    public static final String NOMBRE_DIR_JSON		= "json";
    public static final String NOMBRE_DIR_PDF		= "pdf";
    public static final String NOMBRE_DIR_RAIZ      = "contabilidad";

	
	// Extensiones
	public static final String EXTENSION_JSON 		= ".json";
	public static final String EXTENSION_LOG 		= ".log";
	public static final String EXTENSION_PNG 		= ".png";
	public static final String EXTENSION_TXT 		= ".txt";
	public static final String EXTENSION_PDF 		= ".pdf";
	
	// Nombre de los iconos
	public static final String IMG_HOME				= "home";
	public static final String IMG_ADD				= "add";
	public static final String IMG_SAVE				= "save";
	public static final String IMG_FILE				= "file";
	public static final String IMG_COINS			= "coins";
	public static final String IMG_EDIT				= "edit";
	public static final String IMG_VIEW				= "view";
	public static final String IMG_ALL				= "all";
	public static final String IMG_SEARCH			= "search";
	public static final String IMG_DELETE			= "borrar";
	public static final String IMG_EDIT2			= "edit2";
	public static final String IMG_BACK				= "back";
	public static final String IMG_DIARY            = "diary";
	public static final String IMG_PDF            	= "pdf";
	public static final String IMG_CALC            	= "calc";
	
	// Nombre de los modelos
	public static final String MODELO_TIENDAS 		= "Tiendas";
	public static final String MODELO_TIPO_COMPRA 	= "TipoCompra";
	public static final String MODELO_FORMA_COMPRA 	= "FormaCompra";
	public static final String MODELO_FORMA_PAGO 	= "FormaPago";
	public static final String MODELO_GASTO_FIJO 	= "GastoFijo";
	
	// Nombre archivos
	public static final String FICHERO_COMPRAS 			= "compras";
	public static final String FICHERO_GASTOS_FIJOS 	= "gastosFijos";
	public static final String FICHERO_PRESUPUESTOS 	= "presupuestos";
	public static final String FICHERO_INGRESOS 		= "ingresos";
	public static final String FICHERO_AHORRO_SEMANAL	= "ahorroSemanal";
	public static final String FICHERO_AHORRO_MENSUAL	= "ahorroMensual";
	public static final String FICHERO_TIENDAS          = "tiendas";
	public static final String FICHERO_FORMA_COMPRA     = "formaCompra";
	public static final String FICHERO_FORMA_PAGO       = "formaPago";
	public static final String FICHERO_GASTOS_FIJOS_LISTA= "gastoFijo";
	
	// Listas
	public static final String LISTA_TIENDAS		= "Lista de Tiendas";
	public static final String LISTA_TIPO_COMPRA	= "Lista de Tipo de Compra";
	public static final String LISTA_FORMA_COMPRA	= "Lista de Forma de Compra";
	public static final String LISTA_FORMA_PAGO		= "Lista de Forma de Pago";
	public static final String LISTA_GASTO_FIJO		= "Lista de Gasto fijo";
	
	// Campos JSON
	public static final String CAMPO_JSON_ID 		= "id";
	public static final String CAMPO_JSON_NOMBRE 	= "nombre";
	
	// Campos vista
	public static final String CAMPO_VISTA_FECHA_COMPRA	= "Fecha compra";
	public static final String CAMPO_VISTA_PRODUCTO		= "Producto";
	public static final String CAMPO_VISTA_IMPORTE		= "Importe";
	public static final String CAMPO_VISTA_FORMA_COMPRA	= "Forma compra";
	public static final String CAMPO_VISTA_TIPO_COMPRA	= "Tipo compra";
	public static final String CAMPO_VISTA_TIENDA		= "Tienda";
	public static final String CAMPO_VISTA_FORMA_PAGO	= "Forma pago";
	public static final String CAMPO_VISTA_GASTO_FIJO	= "Gasto fijo";
	
	public static final String CAMPO_VISTA_FECHA_DOC	= "Fecha doc";
	public static final String CAMPO_VISTA_FECHA_INICIO	= "Fecha inicio";
	public static final String CAMPO_VISTA_FECHA_FIN	= "Fecha fin";
	public static final String CAMPO_VISTA_NUM_FACTURA	= "Num documento";
	public static final String CAMPO_VISTA_TIPO_GASTO	= "Tipo Gasto";
	public static final String CAMPO_VISTA_DURACON		= "Duracion";
	
	public static final String CAMPO_VISTA_FECHA		= "Fecha";
	public static final String CAMPO_VISTA_PRESUPUESTO	= "Presupuesto";
	public static final String CAMPO_VISTA_TIPO			= "Tipo";
	
	public static final String CAMPO_VISTA_CONCEPTO		= "Concepto";
	
	
	// Literales
	public static final String LIT_VERSION 				= "Version";
	public static final String LIT_INFORMACION	 		= "Introduzca los datos, por favor.";
	public static final String LIT_INFO_EDITAR_COMPRAS	= "Seleccione una compra a editar, por favor.";
    public static final String LIT_INFO_EDITAR_GASTO    = "Seleccione un gasto a editar, por favor."; 
    public static final String LIT_INFO_EDITAR_INGRESO  = "Seleccione una ingreso a editar, por favor.";
    public static final String LIT_INFO_EDITAR_PRESU	= "Seleccione un presupuesto a editar, por favor.";
    public static final String LIT_INFO_EDITAR_TIENDA	= "Seleccione una tienda a editar, por favor.";
    public static final String LIT_INFO_EDITAR_FORMA_COMPRA	= "Seleccione una forma de compra a editar, por favor.";
    public static final String LIT_INFO_EDITAR_FORMA_PAGO	= "Seleccione una forma de pago a editar, por favor.";
    public static final String LIT_INFO_EDITAR_TIPO_GASTO_FIJO	= "Seleccione un gasto fijo a editar, por favor.";
	public static final String LIT_ERROR				= "Error ";
    public static final String LIT_INFO_LOG_COMPRAS     = "Todos los movimientos sobre las compras"; 
    public static final String LIT_INFO_LOG_GASTOS      = "Todos los movimientos sobre los gastos";
	public static final String LIT_INFO_MODIF			= "Modifique algun dato si es necesario.";
	public static final String LIT_INFO_BUSCAR_PRODUCTO	= "Introduzca el nombre del producto:";
	public static final String LIT_INFO_VER_GASTOS		= "Puede seleccionar para ver algun gasto fijo";
	public static final String LIT_INFO_VER_INGRESOS	= "Puede visualizar todos los ingresos";
	public static final String LIT_INFO_VER             = "Puede seleccionar alguna opcion";
	public static final String LIT_INFO_VER_TODO        = "Estos son todos los registros";
	public static final String LIT_VER_AHORRO_SEMANAL   = "Puedes ver el ahorro semanal.";
	public static final String LIT_VER_AHORRO_MENSUAL   = "Puedes ver el ahorro mensual.";
	
	public static final String LIT_DESDE   = "Desde";
	public static final String LIT_HASTA   = "hasta";
	
	
	// Avisos
	public static final String AVISO_NO_SE_CARGA 				= "no se puede cargar.";
	public static final String AVISO_INTRO_DATO_CAMPO 			= "Introduzca el dato en el campo ";
	public static final String AVISO_FECHA_FORMATO_INCORRECTO 	= "Formato incorrecto de fecha del campo ";
	public static final String AVISO_CAMPO_FORMA_PAGO 			= "Seleccione la forma de pago";
	public static final String AVISO_FICHERO_NO_EXISTE 			= "El fichero no existe.";
	public static final String AVISO_FICHERO_ESTA_MAL 			= "El fichero no esta bien.";
	public static final String AVISO_FICHERO_VACIO	 			= "El fichero esta vacio.";
	public static final String AVISO_CAMPO_VACIO	 			= "Introduzca ";
	public static final String AVISO_FECHA_FORMATO_CORRECTO     = "El formato ha de ser dd/MM/aaaa"; 
	public static final String AVISO_FECHA_NO_SUP_FECHA_ACTUAL  = "no puede ser superior a la fecha actual"; 
	public static final String AVISO_FECHA_NO_SUP_ANIO_ACTUAL   = "no puede ser superior a la año actual"; 
	public static final String AVISO_AGREGAR_COMPRA             = "Agregue una compra, por favor."; 
    public static final String AVISO_AGREGAR_GASTO_FIJO         = "Agregue un gasto fijo, por favor."; 
    public static final String AVISO_AGREGAR_PRESUPUESTO        = "Agregue un presupuesto, por favor."; 
    public static final String AVISO_AGREGAR_INGRESO            = "Agregue un ingreso, por favor."; 
    public static final String AVISO_AGREGAR_TIENDA             = "Agregue una tienda, por favor."; 
    public static final String AVISO_AGREGAR_FORMA_COMPRA       = "Agregue una forma de compra, por favor."; 
    public static final String AVISO_AGREGAR_FORMA_PAGO         = "Agregue una forma de pago, por favor."; 
    public static final String AVISO_AGREGAR_TIPO_GASTO_FIJO    = "Agregue un tipo de gasto fijo, por favor.";
    public static final String AVISO_DIRECTORIO_NO_CREADO       = "No se han creado los directorios.";
    
    // Titulo de los avisos 
    public static final String AVISO_CREACION_DIRECTORIOS       = "Creaci�n de directorios";

	
	
	// Errores
	public static final String ERROR_DEBE_SER_NUMERO	= "debe ser numero.";
	public static final String ERROR_MODI				= "Error al modificar los datos.";
	public static final String ERROR_BAJA				= "Error al dar de baja.";

	public static final String ERROR_CREAR_ARCHIVO		= "Error al crear el archivo.";
	public static final String ERROR_ELIMINAR_ARCHIVO	= "Error al eliminar el archivo.";
	
	
	// Valida
	public static final String VALIDA_CAMPO_VACIO		= "Campo vacio";
	public static final String VALIDA_CAMPO_INCORRECTO	= "Campo incorrecto";
	public static final String VALIDA_CAMPO_FECHA		= "Validar fecha";
	
	
	// Labels
	public static final String LABEL_FECHA_COMPRA	= "Fecha compra";
	public static final String LABEL_FORMA_COMPRA	= "Forma compra";
	public static final String LABEL_TIPO_TIENDA	= "Tipo tienda";
	public static final String LABEL_TIPO_COMPRA	= "Tipo compra";
	public static final String LABEL_TIENDA			= "Tienda";
	public static final String LABEL_PRODUCTO		= "Producto";
	public static final String LABEL_IMPORTE		= "Importe";
	public static final String LABEL_FORMA_PAGO		= "Forma pago";
	public static final String LABEL_ID				= "ID";
	public static final String LABEL_NOMBRE_TIENDA  = "Nombre de tienda";
	public static final String LABEL_NOMBRE_GASTO_FIJO  = "Nombre del gasto fijo";
	public static final String LABEL_GASTO_FIJO  	= "Gasto";
	public static final String LABEL_AHORRO		  	= "Ahorro";
	
	public static final String LABEL_FECHA_DOC		= "Fecha doc";
	public static final String LABEL_NUM_DOC		= "Num Documento";
	public static final String LABEL_TIPO_GASTO		= "Tipo Gasto";
	public static final String LABEL_FECHA_INICIO	= "Fecha inicio";
	public static final String LABEL_FECHA_FIN		= "Fecha fin";
	public static final String LABEL_DURACION		= "Duracion";
	
	public static final String LABEL_FECHA			= "Fecha";
	public static final String LABEL_PRESUPUESTO	= "Presupuesto";
	public static final String LABEL_TIPO			= "Tipo";
	public static final String LABEL_CONCEPTO       = "Concepto";
    public static final String LABEL_CANTIDAD       = "Cantidad";
    
    public static final String LABEL_TOTAL       	= "Total";
	
	public static final String RADIO_SEMANAL		= "Semanal";
	public static final String RADIO_MENSUAL		= "Mensual";
	
	// INPUT FECHAS
	public static final String INPUT_FECHA_FACTURA	= "fechaFactura";
	public static final String INPUT_FECHA_INICIO	= "fechaInicio";
	public static final String INPUT_FECHA_FIN		= "fechaFin";
	public static final String INPUT_FECHA			= "date";
	
	// Mensajes
	public static final String MSG_CORRECTO_ALTA		= "Los datos han sido guardados correctamente.";
	public static final String MSG_CORRECTO_MODI		= "Los datos han sido modificados correctamente.";
	public static final String MSG_CORRECTO_BAJA		= "Los datos han sido borrados correctamente.";
	public static final String MSG_DOC_CREADO_PDF       = "El documento pdf ha sido creado correctamente."; 
	
	public static final String MSG_REGISTRO_A_EDITAR	= "Registro a editar";
	
	
	// Tipo de fuente
	public static final String FUENTE_ARIAL			= "Arial";
	public static final String FUENTE_TAHOMA		= "Tahoma";
	
	// Formatos de fecha
	public static final String FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO		= "dd/MM/yyyy";
	public static final String FORMATO_FECHA_ANIO_MES_DIA_AMERICANO			= "yyyy-MM-dd";
	
	// Simbolos
	public static final String ESPACIO 		= " ";
	public static final String PUNTO 		= ".";
	public static final String PUNTOS 		= ":";
	public static final String PUNTO_COMA 	= ";";
	public static final String VACIO		= "";
	public static final String ALMOHADILLA	= "#";
	public static final String GUION_BAJO	= "_";
	public static final String COMA         = ","; 
	
	// Numeros, double, true, false
	public static final String DEFAULT_S_CERO 		= "0";
	public static final int DEFAULT_INT_CERO 		= 0;
	public static final double DEFAULT_DOBLE_CERO 	= 0.0;
	public static final int INT_UNO 				= 1;
	public static final int STRING_UNO 				= 1;
	public static final String BOOLEAN_FALSE_STRING ="false";
	public static final String BOOLEAN_TRUE_STRING 	="true";
	
	// HTML
	public static final String HTML_SALTO_LINEA 	= "\n";
	public static final String PROPERTY_SALTO_LINEA = "line.separator";
	
	// Menu / boton
	public static final String ANADIR 	= "Agregar";
	public static final String EDITAR	= "Editar";
	public static final String BUSCAR	= "Buscar";
	public static final String VER	 	= "Ver";
	
	public static final String BTN_MENU	 	= "Menu";
	public static final String BTN_GUARDAR	= "Guardar";
	public static final String BTN_OTRO	 	= "Otro";
	public static final String BTN_TODOS	= "Todos";
	public static final String BTN_BUSCAR	= "Buscar";
	public static final String BTN_BORRAR	= "Borrar";
	public static final String BTN_EDITAR	= "Editar";
	public static final String BTN_VOLVER	= "Volver";
	public static final String BTN_VER		= "Ver";
	public static final String BTN_CONSULTAR= "Consultar";
	public static final String BTN_DIARIO   = "Diario";
	public static final String BTN_CALCULAR = "Calcular";
	public static final String BTN_CREAR 	= "Crear";
	
	// Items menu
	public static final String MENU_PRESUPUESTO 		= "Presupuesto";
	public static final String MENU_INGRESO 			= "Ingreso";
	public static final String MENU_TIENDA				= "Tienda";
	public static final String MENU_FORMA_COMPRA		= "Forma de compra";
	public static final String MENU_FORMA_PAGO			= "Forma de pago";
	public static final String MENU_TIPOS_GASTOS_FIJOS	= "Tipos Gastos Fijos";
	public static final String MENU_PRESUPUESTOS 		= "Presupuestos";
	public static final String MENU_INGRESOS 			= "Ingresos";
	public static final String MENU_GASTOS_FIJOS 		= "Gastos Fijos";
	public static final String MENU_TIENDAS 			= "Tiendas";
	public static final String MENU_FORMAS_COMPRA		= "Formas de compra";
	public static final String MENU_FORMAS_PAGO			= "Formas de pago";
	public static final String MENU_PRODUCTO 			= "Producto";
	public static final String MENU_COMPRAS_DIARIAS 	= "Compras Diarias";
	public static final String MENU_COMPRAS_CONCRETAS 	= "Compras Concretas";
	public static final String MENU_AHORRO			 	= "Ahorro";
	public static final String MENU_SEMANAL			 	= "Semanal";
	public static final String MENU_FICHERO			 	= "Fichero";
	public static final String MENU_COMPRAS_LOG		 	= "Compras log";
	public static final String MENU_FIJOS_LOG		 	= "Fijos log";
	public static final String MENU_GRAFICO			 	= "Grafico";
	public static final String MENU_COMIDA			 	= "Comida";
	public static final String MENU_TARJETA_NET			= "Tarjeta NET";
	public static final String MENU_GASTO_FIJOS 		= "Gasto Fijos";
	public static final String MENU_EMAIL		 		= "Email";
	public static final String MENU_ENVIAR		 		= "Enviar";
	public static final String MENU_RECIBIR		 		= "Recibir";
	
	// Accesos director - principal
	public static final String ACCESO_COMPRA 	= "Compra";
	public static final String ACCESO_FIJO	 	= "Fijo";
	public static final String ACCESO_COMPRAS 	= "Compras";
	public static final String ACCESO_DIARIO 	= "Diario";
	public static final String ACCESO_FECHAS 	= "Fechas";
	public static final String ACCESO_FIJOS	 	= "Fijos";
	
	public static final int MAX_TIENDAS = 18;
	public static final int MAX_TIPO_COMPRA = 9; 
	public static final int MAX_FORMA_COMPRA = 3;
	public static final int MAX_FORMA_PAGO = 6;
	public static final int MAX_GASTOS_FIJOS = 7;
	
	// Columnas - Tabla Compras
	public static final String COMPRAS_COLUMNA_ID 			= "ID";
	public static final String COMPRAS_COLUMNA_FECHA 		= "Fecha";
	public static final String COMPRAS_COLUMNA_COMPRA 		= "Compra";
	public static final String COMPRAS_COLUMNA_TIENDA 		= "Tienda";
	public static final String COMPRAS_COLUMNA_PRODUCTO 	= "Producto";
	public static final String COMPRAS_COLUMNA_IMPORTE 		= "Importe";
	public static final String COMPRAS_COLUMNA_FORMA_COMPRA	= "Forma Compra";
	public static final String COMPRAS_COLUMNA_PAGO			= "Pago";
	public static final String COMPRAS_COLUMNA_SELECCIONAR	= "Seleccionar";
	
    // Columnas - Tabla Gastos Fijos 
    public static final String COMPRAS_COLUMNA_FECHA_DOC    = "Fecha doc"; 
    public static final String COMPRAS_COLUMNA_NUM_DOC      = "Num doc"; 
    public static final String COMPRAS_COLUMNA_GASTO        = "Gasto"; 
    public static final String COMPRAS_COLUMNA_FECHA_INICIO = "F. inicio"; 
    public static final String COMPRAS_COLUMNA_FECHA_FIN    = "F. fin"; 
    public static final String COMPRAS_COLUMNA_DURACION     = "Duracion"; 


	// Numero de columnas de las tablas
	public static final int NUM_COLUMNAS_COMPRAS = 7;
    public static final int NUM_COLUMNAS_GASTOS_FIJOS = 8; 
    public static final int NUM_COLUMNAS_INGRESOS = 4;
    public static final int NUM_COLUMNAS_PRESUS = 4;
    public static final int NUM_COLUMNAS_AHORRO = 5;
    public static final int NUM_COLUMNAS_FORMAS_PAGO = 2;
    public static final int NUM_COLUMNAS_FORMAS_COMPRA = 2;
    public static final int NUM_COLUMNAS_TIENDAS = 2;
    public static final int NUM_COLUMNAS_TIPOS_GASTOS_FIJOS = 2;

	
	// Duracion gastos fijos
	public static final String DURACION_DEFAULT				= "1 mes";
	
	// Cabeceras ficheros
	public static final String CABECERA_RAYA = "------------------------------------------------------------------------------------------------------------------------------------------------------------";
	public static final String CABECERA_COMPRAS_LOG			= "Evento        Hora evento                ID  Fecha compra      Tienda          Importe        Tipo           Pago           Producto						";
	public static final String CABECERA_GASTOS_FIJOS_LOG	= "Evento        Hora evento       ID  Fecha fact     	Num Fact    F. inicio        F. fin           		Gasto    Importe						";
	public static final String CABECERA_PRESUPUESTOS_LOG	= "Evento        Hora evento        ID  Fecha               Tipo         Presupuesto						";
	public static final String CABECERA_INGRESO_LOG			= "Evento        Hora evento        ID  Fecha               Concepto         Ingreso						";
	
	// Titulos - PDF
	public static final String TITULO_PDF_AHORRO_SEMANAL 	= "Ahorro semanal";
	public static final String TITULO_PDF_AHORRO_MENSUAL 	= "Ahorro mensual";
	public static final String TITULO_PDF_COMPRAS_CONCRETAS = "Compras concretas";

}
