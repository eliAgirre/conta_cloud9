package utilidades; 

import java.io.BufferedInputStream; 
import java.io.BufferedReader; 
import java.io.BufferedWriter; 
import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException; 
import java.io.FileReader; 
import java.io.FileWriter; 
import java.io.IOException; 
import java.io.InputStream; 
import java.io.RandomAccessFile; 
import java.util.ArrayList; 
import java.util.Arrays; 

import javax.swing.JOptionPane; 

import modelo.Ruta; 

public class FicherosRegistros { 
        
        public static final String RUTA_TXT = ListaConstantesValidacion.RUTA_FICHEROS_REGISTROS[0]; // 0 - src 
        //public static final String RUTA_TXT = ListaConstantesValidacion.RUTA_FICHEROS_REGISTROS[1]; // 1 - jar 
        public static Ruta RUTA = new Ruta(RUTA_TXT, Constantes.EXTENSION_TXT); 
        
        public static boolean ficheroTxtCreado(String filename) { 
            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
              
            //Create the file 
            try { 
                if (file.createNewFile()){ 
                        return true; 
                        
                }else{ 
                        return false; 
                } 
            } catch (IOException e) { 
                System.out.println(e.getMessage()); 
                return false; 
            } 
        } 
        
        public static boolean existeFichero(String filename){ 
            
            boolean existe = true; 
            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
            existe = file.exists(); 
            return existe; 
            
        }
        
        public static void insertarRegistros(ArrayList<String> listaRegistros, String filename) { 
                
            try { 
                    
                RUTA.setFilename(filename); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(),true); 
                BufferedWriter bW=new BufferedWriter(ficheroW); 
                String registro = null; 
                
                for(int i=0;i<listaRegistros.size();i++) { 
                        
                        registro = listaRegistros.get(i); 
                        
                        if(!UtilesValida.esNulo(registro)) 
                                bW.write(registro+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA); 
                } 
                
                //bW.write(Constantes.HTML_SALTO_LINEA); 
                bW.close(); 
                    
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
        } 
	        
	    public static boolean eliminarFicheroRegistro(String filename) { 
	            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() );                 
            return file.delete(); 
	    }
        
        
        public static void insertarRegistroCompra(String id, String fecha, String tienda, String producto, String importe, String formaCompra, String pago) { 
            
            try { 
                    
                RUTA.setFilename(Constantes.FICHERO_COMPRAS); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(), true); 
                BufferedWriter bW=new BufferedWriter(ficheroW); 
                
                String registro=id+Constantes.PUNTO_COMA+fecha+Constantes.PUNTO_COMA+tienda+Constantes.PUNTO_COMA+producto+Constantes.PUNTO_COMA+importe+Constantes.PUNTO_COMA 
                                                +formaCompra+Constantes.PUNTO_COMA+pago+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA; 

                bW.write(registro); 
                bW.close(); 
                    
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
        } 
        
        public static void insertarRegistroPresu(String id, String fecha, String tipo, String presupuesto) { 
            
            try { 
                
                RUTA.setFilename(Constantes.FICHERO_PRESUPUESTOS); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(), true); 
                BufferedWriter bW=new BufferedWriter(ficheroW); 
                
                String registro=id+Constantes.PUNTO_COMA+fecha+Constantes.PUNTO_COMA+tipo+Constantes.PUNTO_COMA+presupuesto+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA; 

                bW.write(registro); 
                bW.close(); 
                    
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
        } 
        
        
        public static void insertarRegistroGastoFijo(String idGastoFijo, String fechaFactura, String numFactura, String fechaInicio, String fechaFin, String tipoGasto, String duracion, String importe) { 
                
            try { 
                    
                RUTA.setFilename(Constantes.FICHERO_GASTOS_FIJOS); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(), true); 
                BufferedWriter bW=new BufferedWriter(ficheroW); 
                
                String registro=idGastoFijo+Constantes.PUNTO_COMA+fechaFactura+Constantes.PUNTO_COMA+numFactura+Constantes.PUNTO_COMA+fechaInicio+Constantes.PUNTO_COMA+fechaFin+Constantes.PUNTO_COMA 
                                                +tipoGasto+Constantes.PUNTO_COMA+Constantes.STRING_UNO+Constantes.PUNTO_COMA+duracion+Constantes.PUNTO_COMA+importe+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA; 

                bW.write(registro); 
                bW.close(); 
                    
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
        } 
        
        public static void insertarRegistroIngreso(String idIngreso, String fechaIngreso, String concepto, String importe) { 
                
            try { 
                    
                RUTA.setFilename(Constantes.FICHERO_INGRESOS); 
                FileWriter ficheroW=new FileWriter(  RUTA.obtenerRuta(), true); 
                BufferedWriter bW=new BufferedWriter(ficheroW); 
                
                String registro=idIngreso+Constantes.PUNTO_COMA+fechaIngreso+Constantes.PUNTO_COMA+concepto+Constantes.PUNTO_COMA+importe+Constantes.ALMOHADILLA+Constantes.HTML_SALTO_LINEA; 

                bW.write(registro); 
                bW.close(); 
                    
            } catch (IOException e) { 
                JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
        } 
        
        public static ArrayList<String> editarListaCompras(String id, String fecha, String tienda, String producto, String importe, String formaCompra, String pago) { 
                
                ArrayList<String> registros = leerRegistros(Constantes.FICHERO_COMPRAS); 
                String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_COMPRAS]; 
                String[] registroEdit   = new String[Constantes.INT_UNO]; 
                
                if(!UtilesValida.esNulo(registros)) { 
                        
                    for(int i=0;i<registros.size();i++) { 
                            
                        boolean registroEditado = false; 
                        datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
                        
                        if(!registroEditado) { 
                                
                            if(datosRegistro[0].equals(id)) { 
                                    
                                datosRegistro[1] = fecha; 
                                datosRegistro[2] = tienda; 
                                datosRegistro[3] = producto; 
                                datosRegistro[4] = importe; 
                                datosRegistro[5] = formaCompra; 
                                datosRegistro[6] = pago; 
                                
                                registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3] 
                                                + Constantes.PUNTO_COMA + datosRegistro[4] + Constantes.PUNTO_COMA + datosRegistro[5] + Constantes.PUNTO_COMA + datosRegistro[6]; 
                                
                                registroEditado = true; 
                                    
                            } 
                        } 
                        
                        if(registroEditado) { 
                            //registros[i] = registroEdit[0]; 
                            registros.set(i, registroEdit[0]); 
                        } 
                            
                    } 
                } 
                
                return registros; 
        } 
        
        
        public static ArrayList<String> editarListaPresus(String id, String fecha, String tipo, String importe) { 
                
            ArrayList<String> registros = leerRegistros(Constantes.FICHERO_PRESUPUESTOS); 
            String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_PRESUS]; 
            String[] registroEdit   = new String[Constantes.INT_UNO]; 
            
            if(!UtilesValida.esNulo(registros)) { 
                    
                for(int i=0;i<registros.size();i++) { 
                        
                    boolean registroEditado = false; 
                    datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
                    
                    if(!registroEditado) { 
                            
                        if(datosRegistro[0].equals(id)) { 
                            
                            datosRegistro[1] = fecha; 
                            datosRegistro[2] = tipo; 
                            datosRegistro[3] = importe; 
                            
                            registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3]; 
                            
                            registroEditado = true; 
                                
                        } 
                    } 
                    
                    if(registroEditado) { 
                        //registros[i] = registroEdit[0]; 
                        registros.set(i, registroEdit[0]); 
                    } 
                        
                } 
            } 
            
            return registros; 
        } 
        
        
        public static ArrayList<String> editarListaIngresos(String id, String fecha, String concepto, String importe) { 
                
            ArrayList<String> registros = leerRegistros(Constantes.FICHERO_INGRESOS); 
            String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_INGRESOS]; 
            String[] registroEdit   = new String[Constantes.INT_UNO]; 
            
            if(!UtilesValida.esNulo(registros)) { 
                
                for(int i=0;i<registros.size();i++) { 
                        
                    boolean registroEditado = false; 
                    datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
                    
                    if(!registroEditado) { 
                            
                        if(datosRegistro[0].equals(id)) { 
                                
                            datosRegistro[1] = fecha; 
                            datosRegistro[2] = concepto; 
                            datosRegistro[3] = importe; 
                            
                            registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3]; 
                            
                            registroEditado = true; 
                                
                        } 
                    } 
                    
                    if(registroEditado) { 
                        //registros[i] = registroEdit[0]; 
                        registros.set(i, registroEdit[0]); 
                    } 
                        
                } 
            } 
            
            return registros; 
        } 
        
        public static ArrayList<String> obtenerListaTemporal(String filename){ 
            
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
            int numLineas = 0; 
            
            try { 
                    numLineas = FicherosRegistros.numLineas(Constantes.FICHERO_COMPRAS); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
            
            String[] listaTemp = null; 
            ArrayList<String> listTemp = new ArrayList<String>(); 
            
            // Abre el fichero                 
            String texto="";                 
            FileReader ficheroR; 
            
            try { 
                    ficheroR = new FileReader(file); 
                    BufferedReader bR=new BufferedReader(ficheroR); 
                    texto=bR.readLine(); 
                    
                    // Mientras que el fichero contenga texto mostrara 
                    while(texto!= null){ 
                            
                            if(numLineas!=0) { 
                                    listaTemp = texto.split(Constantes.ALMOHADILLA); 
                                    listTemp.add(listaTemp[0]); 
                            } 
                            
                            texto=bR.readLine(); 

                    } // Cierre while 

                    bR.close(); 
                    
            } catch (FileNotFoundException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
            
            return listTemp; 
        } 
        
        public static ArrayList<ArrayList<Object>> obtenerRegistros(ArrayList<String> listaTemp){ 
                
            ArrayList<Object> listCampos = null; 
            ArrayList<ArrayList<Object>> registros = new ArrayList<ArrayList<Object>>(); 
            Object[] listaCampos = null; 
            
            if(!UtilesValida.esNula(listaTemp)) { 
                    
                    for(int i=0;i<listaTemp.size();i++) { 
                            
                            listaCampos = listaTemp.get(i).split(Constantes.PUNTO_COMA); 
                            
                            listCampos = new ArrayList<Object>(Arrays.asList(listaCampos)); 
                            
                            if(listCampos.size()>1) 
                                    registros.add(i, listCampos); 
                            
                            
                    } 
                    
            } 
            
            return registros; 
        } 
        
        
        public static ArrayList<String> leerRegistros(String filename){ 
                
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
            int numLineas = 0; 
            
            try { 
                    numLineas = FicherosRegistros.numLineas(RUTA.getFilename()); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
            
            String[] listaTemp = null; 
            String[] listaCampos = null; 
            ArrayList<String> listaRegistros = new ArrayList<String>(); 
            
            // Abre el fichero                 
            String texto="";                 
            FileReader ficheroR; 
            
            try { 
                    ficheroR = new FileReader(file); 
                    BufferedReader bR=new BufferedReader(ficheroR); 
                    texto=bR.readLine(); 
                    
                    // Mientras que el fichero contenga texto mostrara 
                    while(texto!= null){ 
                            
                            if(numLineas!=0) 
                                    listaTemp = texto.split(Constantes.ALMOHADILLA); 
                            
                            texto=bR.readLine(); 
                            
                            if(!UtilesValida.esNulo(listaTemp)) { 
                                    listaCampos = listaTemp[0].split(Constantes.PUNTO_COMA); 
                                    
                                    if(listaCampos.length > Constantes.INT_UNO) { 
                                            listaRegistros.add(listaTemp[0]); 
                                    } 
                                    
                            } 

                    } // Cierre while 

                    bR.close(); 
                    
            } catch (FileNotFoundException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } catch (IOException e) { 
                    JOptionPane.showMessageDialog(null, e.getMessage()); 
            } 
            
            return listaRegistros; 
        } 
        
        public static String[][] obtenerRegistrosBidimensional(ArrayList<ArrayList<String>> listaRegistros){ 
            
            String[][] array = null; 
            
            if(!UtilesValida.esNula(listaRegistros)) { 
                    
                    array = new String[listaRegistros.size()][]; 
                    
                    for (int i = 0; i < listaRegistros.size(); i++) { 
                            
                        ArrayList<String> row = listaRegistros.get(i); 
                        array[i] = row.toArray(new String[row.size()]); 
                    } 
            } 
            
            
            return array; 
        } 
        
        
    	public static ArrayList<String> borrarRegistroCompra(String id) { 
        
            ArrayList<String> registros = leerRegistros(Constantes.FICHERO_COMPRAS); 
            String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_COMPRAS]; 
            String[] registroEdit   = new String[Constantes.INT_UNO]; 
            
            if(!UtilesValida.esNulo(registros)) { 
                    
	            for(int i=0;i<registros.size();i++) { 
	                    
	                boolean registroEditado = false; 
	                datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
	                
	                if(!registroEditado) { 
	                    
	                    if(datosRegistro[0].equals(id)) { 
	                            
	                        datosRegistro[1] = Constantes.VACIO; 
	                        datosRegistro[2] = Constantes.VACIO; 
	                        datosRegistro[3] = Constantes.VACIO; 
	                        datosRegistro[4] = Constantes.VACIO; 
	                        datosRegistro[5] = Constantes.VACIO; 
	                        datosRegistro[6] = Constantes.VACIO; 
	                        
	                        registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3] 
	                                        + Constantes.PUNTO_COMA + datosRegistro[4] + Constantes.PUNTO_COMA + datosRegistro[5] + Constantes.PUNTO_COMA + datosRegistro[6]; 
	                        
	                        registroEditado = true; 
		                            
	                    } 
	                } 
	                
	                if(registroEditado) { 
                        //registros[i] = registroEdit[0]; 
                        registros.set(i, registroEdit[0]); 
	                } 
		                        
		        } 
	        } 
	        
	        return registros; 
        } 

        
    	public static ArrayList<String> borrarRegistroPresu(String id) { 
        
	        ArrayList<String> registros = leerRegistros(Constantes.FICHERO_PRESUPUESTOS); 
	        String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_PRESUS]; 
	        String[] registroEdit   = new String[Constantes.INT_UNO]; 
	        
	        if(!UtilesValida.esNulo(registros)) { 
	                
	            for(int i=0;i<registros.size();i++) { 
	                    
	                boolean registroEditado = false; 
	                datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
	                
	                if(!registroEditado) { 
	                        
	                    if(datosRegistro[0].equals(id)) { 
	                            
	                        datosRegistro[1] = Constantes.VACIO; 
	                        datosRegistro[2] = Constantes.VACIO; 
	                        datosRegistro[3] = Constantes.VACIO; 
	                        
	                        registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3]; 
	                        
	                        registroEditado = true; 
	                            
	                    } 
	                } 
	                
	                if(registroEditado) { 
	                    //registros[i] = registroEdit[0]; 
	                    registros.set(i, registroEdit[0]); 
	                } 
	                    
	            } 
	        } 
	        
	        return registros; 
        } 
    

        public static ArrayList<String> borrarRegistroIngreso(String id) { 
                
	        ArrayList<String> registros = leerRegistros(Constantes.FICHERO_INGRESOS); 
	        String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_INGRESOS]; 
	        String[] registroEdit   = new String[Constantes.INT_UNO]; 
	        
	        if(!UtilesValida.esNulo(registros)) { 
	                
	            for(int i=0;i<registros.size();i++) { 
	                    
	                boolean registroEditado = false; 
	                datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
	                
	                if(!registroEditado) { 
	                        
	                    if(datosRegistro[0].equals(id)) { 
	                            
	                        datosRegistro[1] = Constantes.VACIO; 
	                        datosRegistro[2] = Constantes.VACIO; 
	                        datosRegistro[3] = Constantes.VACIO; 
	                        
	                        registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3]; 
	                        
	                        registroEditado = true; 
	                            
	                    } 
	                } 
	                
	                if(registroEditado) { 
	                    //registros[i] = registroEdit[0]; 
	                    registros.set(i, registroEdit[0]); 
	                } 
	                    
	            } 
	        } 
	        
	        return registros; 
        } 

        
        public static ArrayList<String> editarListaGastosFijos(String id, String fechaDoc, String numDoc, String nombreGasto, String fechaInicio, String fechaFin, String duracion, String importe) { 
        
	        ArrayList<String> registros = leerRegistros(Constantes.FICHERO_GASTOS_FIJOS); 
	        String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_GASTOS_FIJOS]; 
	        String[] registroEdit   = new String[Constantes.INT_UNO]; 
	        
	        if(!UtilesValida.esNulo(registros)) { 
	                
	            for(int i=0;i<registros.size();i++) { 
	                    
	                boolean registroEditado = false; 
	                datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
	                
	                if(!registroEditado) { 
	                        
	                    if(datosRegistro[0].equals(id)) { 
	                            
	                        datosRegistro[1] = fechaDoc; 
	                        datosRegistro[2] = numDoc; 
	                        datosRegistro[3] = nombreGasto; 
	                        datosRegistro[4] = fechaInicio; 
	                        datosRegistro[5] = fechaFin; 
	                        datosRegistro[6] = duracion; 
	                        datosRegistro[7] = importe; 
	                        
	                        registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3] 
	                                        + Constantes.PUNTO_COMA + datosRegistro[4] + Constantes.PUNTO_COMA + datosRegistro[5] + Constantes.PUNTO_COMA + datosRegistro[6] + Constantes.PUNTO_COMA + datosRegistro[7]; 
	                        
	                        registroEditado = true; 
	                            
	                        } 
	                    } 
	                    
	                    if(registroEditado) { 
	                        //registros[i] = registroEdit[0]; 
	                        registros.set(i, registroEdit[0]); 
	                    } 
	                    
	            } 
	        } 
	        
	        return registros; 
        } 
        
                
        public static ArrayList<String> borrarRegistroGastoFijo(String id) { 
                
	        ArrayList<String> registros = leerRegistros(Constantes.FICHERO_GASTOS_FIJOS); 
	        String[] datosRegistro        = new String[Constantes.NUM_COLUMNAS_GASTOS_FIJOS]; 
	        String[] registroEdit   = new String[Constantes.INT_UNO]; 
	        
	        if(!UtilesValida.esNulo(registros)) { 
	                
	            for(int i=0;i<registros.size();i++) { 
	                    
	                boolean registroEditado = false; 
	                datosRegistro = registros.get(i).split(Constantes.PUNTO_COMA); 
	                
	                if(!registroEditado) { 
	                        
	                    if(datosRegistro[0].equals(id)) { 
	                            
	                        datosRegistro[1] = Constantes.VACIO; 
	                        datosRegistro[2] = Constantes.VACIO; 
	                        datosRegistro[3] = Constantes.VACIO; 
	                        datosRegistro[4] = Constantes.VACIO; 
	                        datosRegistro[5] = Constantes.VACIO; 
	                        datosRegistro[6] = Constantes.VACIO; 
	                        datosRegistro[7] = Constantes.VACIO; 
	                        
	                        registroEdit[0] = datosRegistro[0] + Constantes.PUNTO_COMA + datosRegistro[1] + Constantes.PUNTO_COMA + datosRegistro[2] + Constantes.PUNTO_COMA + datosRegistro[3] 
	                                        + Constantes.PUNTO_COMA + datosRegistro[4] + Constantes.PUNTO_COMA + datosRegistro[5] + Constantes.PUNTO_COMA + datosRegistro[6] + Constantes.PUNTO_COMA + datosRegistro[7]; 
	                        
	                        registroEditado = true; 
	                            
	                    } 
	                } 
	                
	                if(registroEditado) { 
	                    //registros[i] = registroEdit[0]; 
	                    registros.set(i, registroEdit[0]); 
	                } 
	                    
	            } 
	        } 
	        
	        return registros; 
        } 

        
        public static int numLineas(String filename) throws IOException { 
                
            RUTA.setFilename(filename); 
            File file = new File( RUTA.obtenerRuta() ); 
	        InputStream is = new BufferedInputStream(new FileInputStream(file)); 
            
            try { 
                byte[] c = new byte[1024]; 
                int count = 0; 
                int readChars = 0; 
                boolean empty = true; 
                while ((readChars = is.read(c)) != -1) { 
                    empty = false; 
                    for (int i = 0; i < readChars; ++i) { 
                        if (c[i] == '\n') { 
                            ++count; 
                        } 
                    } 
                } 
                return (count == 0 && !empty) ? 1 : count; 
            } finally { 
                is.close(); 
            } 
        } 
        
        /** 
         * Obtiene la ultima linea de registros. 
         *   
         * @return lastLine Devuelve la ultima linea. 
         * 
         */ 
        public static String getUltimaLineaRegistros(String modelo) { 
                
            RUTA.setFilename(modelo); 
            File file = new File( RUTA.obtenerRuta() ); 
            RandomAccessFile fileHandler = null; 
                                        
            try { 
                fileHandler = new RandomAccessFile( file, "r" ); 
                long fileLength = fileHandler.length() - 1; 
                StringBuilder sb = new StringBuilder(); 

                for(long filePointer = fileLength; filePointer != -1; filePointer--){ 
                    fileHandler.seek( filePointer ); 
                    int readByte = fileHandler.readByte(); 

                    if( readByte == 0xA ) { 
                        if( filePointer == fileLength ) { 
                            continue; 
                        } 
                        break; 

                    } else if( readByte == 0xD ) { 
                        if( filePointer == fileLength - 1 ) { 
                            continue; 
                        } 
                        break; 
                    } 

                    sb.append( ( char ) readByte ); 
                } 

                String lastLine = sb.reverse().toString(); 
                return lastLine; 
            } catch(FileNotFoundException fne) { 
                    //JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_NO_EXISTE); 
                return Constantes.AVISO_FICHERO_NO_EXISTE; 
            } catch( IOException ioe) { 
                    JOptionPane.showMessageDialog(null, Constantes.AVISO_FICHERO_ESTA_MAL); 
                return null; 
            } finally { 
                if (fileHandler != null ) 
                    try { 
                        fileHandler.close(); 
                    } catch (IOException e) { 
                        /* ignore */ 
                    } 
            } 
                
                        
        } // getUltimaLineaRegistros 

} 
