package utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UtilesValida {
	
	/**
	 * Comprueba si una cadena de string es nula.
	 * @param cadena Es una cadena de tipo String.
	 * @return false Si la cadena no es nula. Sino true.
	 */
	public static boolean isNull(String cadena) {
		
		if(cadena==null)
			return true;
		
		return false;
	}
	
    /** 
     * Comprueba si una cadena de string es nula, vacia o literal null. 
     * @param cadena Es una cadena de tipo String. 
     * @return false Si la cadena no es nula, vacia ni literal a null. Sino true. 
     */ 
    public static boolean esNulo(String cadena) { 
            
            if(cadena==null){ 
                    return true; 
            } 
            else if(cadena.equals("")) { 
                    return true; 
            } 
            if(UtilesValida.eliminaBlancos(cadena).isEmpty()){ 
                    return true;         
            } 
            else if(cadena.equals("null")) { 
                    return true; 
            } 
            
            return false; 
    } 
    
    /** 
     * Metodo que elimina los espacios de una cadena 
     * @param valor a comprobar 
     * @return nulo o el propio valor sin espacios en caso de no ser nulo 
     * **/ 
    public static String eliminaBlancos(String valor) { 
            if (valor == null){ 
                    return null; 
            } 
            return valor.trim(); 
            
    } 

	
	/**
	 * Comprueba si un objeto de string es nulo.
	 * @param objecto Es una objecto de tipo Object
	 * @return false Si la cadena no es nula, vacia ni literal a null. Sino true.
	 */
	public static boolean esNulo(Object objecto) {
		
		if(objecto==null)
			return true;
		
		return false;
		
	}
	
	/**
	 * Comprueba si una lista es nula o esta vacia.
	 * @param lista Es una lista de tipo ArrayList
	 * @return false Si la lista no es nula ni vacia. Sino true.
	 */
	@SuppressWarnings("rawtypes")
	public static boolean esNula(ArrayList lista) {
		
		if(lista==null){
			return true;
		}
		if(lista.isEmpty()){
			return true;
		}
			
		return false;
		
	}
	
	/**
	 * Comprueba si una cadena de es numero.
	 * @param cadena Es una cadena de tipo String
	 * @return false Si la cadena no es numero. Sino true.
	 */
    public static boolean esNumero(String cadena){
	    try {
	    	Integer.parseInt(cadena);
	    	return true;
	    } catch (NumberFormatException nfe){
	    	return false;
	    }
    }
	
	/**
	 * Comprueba si una cadena de es numero.
	 * @param cadena Es una cadena de tipo String
	 * @return false Si la cadena no es numero. Sino true.
	 */
    public static boolean esDoble(String cadena){
	    try {
	    	Double.parseDouble(cadena);
	    	return true;
	    } catch (NumberFormatException nfe){
	    	return false;
	    }
    }
    
	/**
	 * Comprueba si una cadena de es fecha.
	 * @param cadena Es una cadena de tipo String
	 * @return false Si la cadena no se puede parsear a una fecha. Sino true.
	 */
    @SuppressWarnings("unused")
	public static boolean esFechaAmericano(String cadena) {
    	
    	 try {
    		 SimpleDateFormat sdf = new SimpleDateFormat(Constantes.FORMATO_FECHA_ANIO_MES_DIA_AMERICANO);
             Date fecha = sdf.parse(cadena);
             return true;
         } catch (ParseException e) {
             return false;
         }
    }
    
	/**
	 * Comprueba si una cadena de es fecha.
	 * @param cadena Es una cadena de tipo String
	 * @return false Si la cadena no se puede parsear a una fecha. Sino true.
	 */
    @SuppressWarnings("unused")
	public static boolean esFechaCastellano(String cadena) {
    	
    	 try {
    		 SimpleDateFormat sdf = new SimpleDateFormat(Constantes.FORMATO_FECHA_DIA_MES_ANIO_CASTELLANO);
             Date fecha = sdf.parse(cadena);
             return true;
         } catch (ParseException e) {
             return false;
         }
    }
    
	/**
	 * Comprueba si una cadena de es fecha.
	 * @param fecha Es la fecha a comprobar entre las dos fechas.
	 * @param fechaInicio Es la fecha de inicio.
	 * @param fechaFin Es la fecha de fin.
	 * @return false Si la cadena no esta entre las dos fechas.
	 */
    public static boolean betweenFechas(Date fecha, Date fechaInicio, Date fechaFin) {
    	
    	if (fecha != null && fechaInicio != null && fechaFin != null) {
            if (fecha.after(fechaInicio) && fecha.before(fechaFin))
                return true;
            else
                return false;         
        }
    	return false;
    }

}
