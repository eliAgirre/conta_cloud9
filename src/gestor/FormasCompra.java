package gestor; 

import java.util.ArrayList; 

import modelo.FormaCompra; 

public class FormasCompra { 
        
    // Atributos de la clase 
    private ArrayList<FormaCompra> gestorFormasCompra; 
    
    public FormasCompra(){ 
            
        gestorFormasCompra=new ArrayList<FormaCompra>();         
            
    } // Cierre constructor 
    
    public void anadir(FormaCompra formaCompra){ 
            
        gestorFormasCompra.add(formaCompra);                 
            
    } // Cierre agregar 

    public int length() { 
            
        return gestorFormasCompra.size(); 
            
    } 
    
    public FormaCompra getFormaCompra(int posicion) { 
            
        return gestorFormasCompra.get(posicion); 
    } 
    
    public int getUltimoID(int posicion) { 
            
        int ultimoID =-1; 
        
        if (!gestorFormasCompra.isEmpty()){ 
                ultimoID = gestorFormasCompra.get(posicion).getIdFormaCompra(); 
        } 
        
        return ultimoID; 
    } 

} // Cierre de la clase 