package gestor; 

import java.util.ArrayList; 

import modelo.FormaPago; 

public class FormasPago { 
        
    // Atributos de la clase 
    private ArrayList<FormaPago> gestorFormasPago; 
    
    public FormasPago(){ 
            
        gestorFormasPago=new ArrayList<FormaPago>();         
            
    } // Cierre constructor 
    
    public void anadir(FormaPago formaPago){ 
            
        gestorFormasPago.add(formaPago);                 
            
    } // Cierre agregar 

    public int length() { 
            
        return gestorFormasPago.size(); 
    } 
    
    public FormaPago getFormaPago(int posicion) { 
            
        return gestorFormasPago.get(posicion); 
    } 
    
    public int getUltimoID(int posicion) { 
            
        int ultimoID =-1; 
        
        if (!gestorFormasPago.isEmpty()){ 
                ultimoID = gestorFormasPago.get(posicion).getIdFormaPago(); 
        } 
        
        return ultimoID; 
    } 

} // Cierre de la clase