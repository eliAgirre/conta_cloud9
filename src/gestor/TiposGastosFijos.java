package gestor;

import java.util.ArrayList;

import modelo.TipoGastoFijo;

public class TiposGastosFijos {
	
	// Atributos de la clase
	private ArrayList<TipoGastoFijo> gestorGastoFijo;
	
	public TiposGastosFijos(){
		
		gestorGastoFijo=new ArrayList<TipoGastoFijo>();	
		
	} // Cierre constructor
	
	public void anadir(TipoGastoFijo gastoFijo){
		
		gestorGastoFijo.add(gastoFijo);		
		
	} // Cierre agregar

	public int length() {
		
		return gestorGastoFijo.size();
		
	}
	
	public TipoGastoFijo getGastoFijo(int posicion) {
		
		return gestorGastoFijo.get(posicion);
	}
	
	public int getUltimoID(int posicion) {
		
		int ultimoID =-1;
		
		if (!gestorGastoFijo.isEmpty()){
			ultimoID = gestorGastoFijo.get(posicion).getIdGastoFijo();
		}
		
		return ultimoID;
	}

} // Cierre de la clase