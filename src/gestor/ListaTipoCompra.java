package gestor;

import modelo.TipoCompra;
import utilidades.Constantes;

public class ListaTipoCompra {
	
	private TipoCompra[] listaTipoCompra = null;
	
	public ListaTipoCompra() {

		this.listaTipoCompra = new TipoCompra[Constantes.MAX_TIPO_COMPRA];
	}

	public TipoCompra[] altaCompra(int idTipoCompra, String tipoCompra) {
		
		for(int i=0;i<Constantes.MAX_TIPO_COMPRA;i++) {
			listaTipoCompra[i].setIdTipoCompra(idTipoCompra);
			listaTipoCompra[i].setTipoCompra(tipoCompra);
		}
		
		return listaTipoCompra;
	}

}
