package gestor;

import java.util.ArrayList;

import modelo.Compra;

public class Compras {
	
	// Atributos de la clase
	private ArrayList<Compra> gestorCompra;
	
	public Compras(){
		
		gestorCompra=new ArrayList<Compra>();	
		
	} // Cierre constructor
	
	public void anadir(Compra compra){
		
		gestorCompra.add(compra);		
		
	} // Cierre agregar

	public int length() {
		
		return gestorCompra.size();
		
	}
	
	public Compra getCompra(int posicion) {
		
		return gestorCompra.get(posicion);
	}
	
	public int getUltimoID(int posicion) {
		
		int ultimoID =-1;
		
		if (!gestorCompra.isEmpty()){
			ultimoID = gestorCompra.get(posicion).getIdCompra();
		}
		
		return ultimoID;
	}

} // Cierre de la clase