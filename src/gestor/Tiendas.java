package gestor;

import java.util.ArrayList;

import modelo.Tienda;

public class Tiendas {
	
	// Atributos de la clase
	private ArrayList<Tienda> gestorTienda;
	
	public Tiendas(){
		
		gestorTienda=new ArrayList<Tienda>();	
		
	} // Cierre constructor
	
	public void anadir(Tienda tienda){
		
		gestorTienda.add(tienda);		
		
	} // Cierre agregar

	public int length() {
		
		return gestorTienda.size();
		
	}
	
	public Tienda getTienda(int posicion) {
		
		return gestorTienda.get(posicion);
	}
	
	public int getUltimoID(int posicion) {
		
		int ultimoID =-1;
		
		if (!gestorTienda.isEmpty()){
			ultimoID = gestorTienda.get(posicion).getIdTienda();
		}
		
		return ultimoID;
	}

} // Cierre de la clase